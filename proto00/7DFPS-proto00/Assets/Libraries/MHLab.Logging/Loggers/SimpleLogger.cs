﻿namespace MHLab.Logging.Loggers
{
	public class SimpleLogger : ILogger
	{
		public void Log(LogLevel level, string message, string memberName, string fileName, int lineNumber)
		{
			UnityEngine.Debug.Log("[" + level + "][" + fileName + "::" + memberName + "@" + lineNumber + "] " + message);
		}
	}
}
