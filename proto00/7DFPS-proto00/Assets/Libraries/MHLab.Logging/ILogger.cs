﻿namespace MHLab.Logging
{
	public interface ILogger
    {
        void Log(LogLevel level, string message, string memberName, string fileName, int lineNumber);
    }
}
