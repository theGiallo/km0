﻿namespace MHLab.Logging
{
    public enum LogLevel
    {
        Simple,
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
        OnFile
    }
}
