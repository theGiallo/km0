﻿using MHLab.Logging.Loggers;
using System.Runtime.CompilerServices;

namespace MHLab.Logging
{
	public static class LoggerManager
    {
	    private static readonly ILogger _logger = new SimpleLogger();

        public static void Log(LogLevel level, string message, [CallerMemberName] string memberName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNumber = 0)
        {
            _logger.Log(level, message, memberName, fileName, lineNumber);
        }
    }
}
