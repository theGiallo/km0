﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace fps
{
	public class
	UI_Manager : MonoBehaviour
	{
		public static UI_Manager instance = null;

		public ServerSearcher        server_searcher;

		[Range(float.Epsilon,6)]
		public float                 adv_validity_secs = 2;

		public Color                 button_selected_color;
		public Color                 button_unselected_color;
		public Color                 discovered_server_list_button_hover_color;

		public GameObject            server_list_button_prefab;
		public GameObject            connected_client_button_prefab;
		public GameObject            other_client_button_prefab;

		public Camera                ui_camera;

		public GameObject            ui_title_screen;
		public GameObject            ui_server_discovery;
		public GameObject            ui_server_lobby;
		public GameObject            ui_client_lobby;
		public GameObject            ui_client_in_game;

		public GameObject            map_container;

		public GameObject            ui_servers_list;

		public Button                ui_servers_list_button_back;
		public UI_Popup_Message      ui_servers_list_popup_message;

		public GameObject            ui_selected_server_data;
		public Text                  ui_selected_server_name_title;
		public Text                  ui_selected_server_mode;
		public Text                  ui_selected_server_netendpoint_text;
		public Text                  ui_selected_server_connected_players_count;
		public Text                  ui_selected_server_connected_players_list_text;
		public Button                ui_button_connect;
		public Button                ui_button_deselect;
		public string                connection_key;

		public GameObject            ui_select_or_create;
		public Button                ui_button_create;
		public InputField            ui_create_server_name_input;

		public GameObject            ui_server_lobby_clients_list;
		public Button                ui_server_lobby_button_back;
		public UI_Popup_Confirmation ui_server_lobby_confirmation_popup;
		public Button                ui_server_lobby_button_start_map;

		public GameObject            ui_client_lobby_clients_list;
		public Button                ui_client_lobby_button_back;
		public UI_Popup_Confirmation ui_client_lobby_confirmation_popup;
		public Text                  ui_client_latency_text;
		public InputField            ui_client_nickname_inputfield;

		public Server                server;
		public Client                client;

		public int                   min_server_name_length = 1;

		private Dictionary<LiteNetLib.NetEndPoint, UI_Server_Found> discovered_servers = new Dictionary<LiteNetLib.NetEndPoint, UI_Server_Found>();

		private UI_Server_Found        selected_server = null;
		private LiteNetLib.NetEndPoint server_netendpoint;
		private bool                   trying_to_connect = false;

		private Dictionary<LiteNetLib.NetEndPoint, UI_Connected_Client> connected_clients = new Dictionary<LiteNetLib.NetEndPoint, UI_Connected_Client>();

		private Dictionary<LiteNetLib.NetEndPoint, UI_Other_Client> other_clients = new Dictionary<LiteNetLib.NetEndPoint, UI_Other_Client>();

		private UI_Screen_ID current_screen_id = UI_Screen_ID._NONE;
		private GameObject   current_ui_screen;

		public class UI_Server_Found
		{
			public UI_Discovered_Server_Button button;
			public Advertised_Server_Data      advertised_server_data;
			public float                       last_seen_time;
		}
		public class UI_Connected_Client
		{
			public UI_Connected_Client_Button   button;
			public Server.Connected_Client_Data data;
		}
		public class UI_Other_Client
		{
			public UI_Connected_Client_Button   button;
			public Client.Other_Client          data;
		}

		public enum
		UI_Screen_ID
		{
			TITLE_SCREEN,
			SERVER_DISCOVERY,
			SERVER_LOBBY,
			CLIENT_LOBBY,
			CLIENT_IN_MAP,
			//---
			_COUNT,
			_NONE,
		}

		private
		void
		Awake()
		{
			instance = this;
		}

		void
		Start()
		{
			ui_server_discovery.SetActive( false );
			ui_title_screen    .SetActive( false );
			ui_server_lobby    .SetActive( false );
			ui_client_lobby    .SetActive( false );
			ui_client_in_game   .SetActive( false );

			map_container.gameObject.SetActive( false );
			ui_camera.gameObject.SetActive( true );

			server_searcher.on_new_server_discovered  .AddListener( on_new_server_discovered );
			ui_create_server_name_input.onValueChanged.AddListener( new_server_name_on_value_change );
			ui_create_server_name_input.onEndEdit     .AddListener( new_server_name_on_edit_complete );

			ui_button_create  .onClick.AddListener( on_new_server_click );
			ui_button_connect.onClick.AddListener( start_connection );
			ui_button_deselect.onClick.AddListener( deselect_server );

			ui_servers_list_button_back.onClick.AddListener( () => change_screen( UI_Screen_ID.TITLE_SCREEN     ) );
			ui_server_lobby_button_back.onClick.AddListener(
			() =>
			{
				ui_server_lobby_confirmation_popup.message = "Do you really want to shut down your server?";
				ui_server_lobby_confirmation_popup.on_confirm.RemoveListener( server_lobby_do_leave );
				ui_server_lobby_confirmation_popup.on_confirm.AddListener( server_lobby_do_leave );
				ui_server_lobby_confirmation_popup.gameObject.SetActive( true );
			} );
			ui_server_lobby_button_start_map.onClick.AddListener( () => { server.do_start_map(); map_container.SetActive( true ); } );


			ui_client_lobby_button_back.onClick.AddListener(
			() =>
			{
				ui_client_lobby_confirmation_popup.message = "Do you really want to disconnect from the server?";
				ui_client_lobby_confirmation_popup.on_confirm.RemoveListener( client_lobby_do_leave );
				ui_client_lobby_confirmation_popup.on_confirm.AddListener( client_lobby_do_leave );
				ui_client_lobby_confirmation_popup.gameObject.SetActive( true );
			} );


			client.on_other_client_connection     .AddListener( on_other_client_connection      );
			client.on_other_client_disconnection  .AddListener( on_other_client_disconnection   );
			client.on_other_client_name_change    .AddListener( on_other_client_name_change     );
			client.on_other_client_latency_updated.AddListener( on_other_client_latency_updated );
			client.on_latency_updated             .AddListener( on_client_latency_updated       );
			client.on_disconnected                .AddListener( on_client_gets_disconnected     );
			client.on_start_map                   .AddListener( on_client_start_map             );
			client.on_stop_map                    .AddListener( on_client_stop_map             );

			client.gameObject.SetActive( false );

			server.on_client_connected     .AddListener( server_on_client_connected      );
			server.on_client_disconnected  .AddListener( server_on_client_disconnected   );
			server.on_client_latency_update.AddListener( on_client_latency_update );
			server.on_client_rename        .AddListener( on_client_rename );

			ui_client_nickname_inputfield.onEndEdit.AddListener( on_nickname_edit_finish );

			ui_server_discovery_reset();
			ui_server_lobby_reset();
			ui_client_lobby_reset();
			ui_client_in_map_reset();

			current_screen_id = UI_Screen_ID._NONE;
			change_screen( UI_Screen_ID.TITLE_SCREEN );
		}

		void
		Update()
		{
			switch ( current_screen_id )
			{
				case UI_Screen_ID.TITLE_SCREEN:
				{
					if ( Input.anyKey )
					{
						change_screen( UI_Screen_ID.SERVER_DISCOVERY );
					}
					break;
				}
				case UI_Screen_ID.SERVER_DISCOVERY:
				{
					List<UI_Server_Found> expired = new List<UI_Server_Found>();
					foreach ( UI_Server_Found s in discovered_servers.Values )
					{
						if ( s.last_seen_time + adv_validity_secs < Time.realtimeSinceStartup )
						{
							expired.Add( s );
							if ( selected_server == s )
							{
								deselect_server();
							}
						}
					}
					foreach ( UI_Server_Found s in expired )
					{
						Destroy( s.button.gameObject );
						discovered_servers.Remove( s.advertised_server_data.adv_net_end_point );
					}

					if ( trying_to_connect )
					{
						if ( client.connected )
						{
							change_screen( UI_Screen_ID.CLIENT_LOBBY );
							client.on_rename( ui_client_nickname_inputfield.text );
						} else
						if ( !client.connecting )
						{
							ui_button_connect.gameObject.SetActive( true );
							ui_servers_list_popup_message.deactivate();
							//ui_connecting_indicator   .gameObject.SetActive( false );
							//ui_connecting_abort_button.gameObject.SetActive( false );
							trying_to_connect = false;
						}
					}
					break;
				}
				case UI_Screen_ID.SERVER_LOBBY:
				{
					break;
				}
				default:
					break;
			}
		}

		void
		ui_activate_message_notice( UnityAction callback, string message, string button_string = null )
		{
			ui_servers_list_popup_message.activate( callback, message, button_string );
		}

		void
		deselect_server()
		{
			ui_selected_server_data.gameObject.SetActive( false );
			ui_select_or_create.gameObject.SetActive( true );
			if ( selected_server != null )
			{
				selected_server.button.on_deselect();
			}
			selected_server = null;
		}
		void
		select_server( UI_Server_Found server_found )
		{
			ui_selected_server_data.SetActive( true  );
			ui_select_or_create    .SetActive( false );
			ui_selected_server_name_title.text = server_found.advertised_server_data.name;
			ui_selected_server_mode.text = "TODO";
			ui_selected_server_connected_players_count.text = server_found.advertised_server_data.connected_players_count.ToString();
			ui_selected_server_netendpoint_text.text = server_found.advertised_server_data.server_net_end_point.ToString();
			server_found.button.on_select();
			selected_server = server_found;
		}
		void
		start_connection()
		{
			client.gameObject.SetActive( true );
			server_netendpoint = selected_server.advertised_server_data.server_net_end_point;
			client.connect( server_netendpoint );
			trying_to_connect = true;
			ui_activate_message_notice( abort_connection_trial, "Connecting...", "Abort connection" );
			ui_button_connect         .gameObject.SetActive( false );
		}
		void
		abort_connection_trial()
		{
			ui_servers_list_popup_message.deactivate();
			ui_button_connect         .gameObject.SetActive( true  );
			trying_to_connect = false;
			client.disconnect();
			client.gameObject.SetActive( false );
		}

		public
		void
		on_new_server_discovered( Advertised_Server_Data advertised_server_data )
		{
			//Debug.Log( "on_new_server_discovered name = " + advertised_server_data.name );
			LiteNetLib.NetEndPoint nep = advertised_server_data.adv_net_end_point;
			UI_Server_Found s = null;
			if ( !discovered_servers.TryGetValue( nep, out s ) )
			{
				GameObject b = GameObject.Instantiate( server_list_button_prefab );
				s = new UI_Server_Found();
				s.button = b.GetComponent<UI_Discovered_Server_Button>();
				s.button.ui_button.onClick.AddListener( () => on_discovered_server_click( s ) );

				set_button_style( s.button.ui_button );

				s.button.ui_ping_ms_text.text = "";
				b.transform.SetParent( ui_servers_list.transform, false );
			}
			s.advertised_server_data = advertised_server_data;
			s.button.ui_server_name_text.text = advertised_server_data.name;
			s.last_seen_time = Time.realtimeSinceStartup;
			discovered_servers[nep] = s;
		}

		public
		void
		set_button_style( Button button )
		{
			ColorBlock cb = button.colors;
			cb.normalColor      = button_unselected_color;
			cb.highlightedColor = discovered_server_list_button_hover_color;
			cb.pressedColor     = button_selected_color;
			button.colors = cb;
		}

		public
		void
		on_discovered_server_click( UI_Server_Found server_found )
		{
			deselect_server();
			select_server( server_found );
		}

		void
		on_client_gets_disconnected()
		{
			change_screen( UI_Screen_ID.SERVER_DISCOVERY );
		}
		void
		on_client_latency_updated()
		{
			ui_client_latency_text.text = client.latency + "ms";
		}
		void
		on_other_client_connection( Client.Other_Client other_client )
		{
			UI_Other_Client oc;
			bool b_res = other_clients.TryGetValue( other_client.net_end_point, out oc );
			if ( b_res )
			{
				Debug.LogWarning( "ui received other client connection event, but already had that client" );
			} else
			{
				GameObject b = Instantiate( other_client_button_prefab );

				oc = new UI_Other_Client();

				oc.data = other_client;

				oc.button = b.GetComponent<UI_Connected_Client_Button>();
				oc.button.ui_button.onClick.AddListener( () => client_lobby_on_other_client_click( oc ) );
				set_button_style( oc.button.ui_button );
				oc.button.ui_client_name_text.text = other_client.net_end_point.ToString();
				b.transform.SetParent( ui_client_lobby_clients_list.transform, false );

				oc.button.ui_ping_ms_text.text = other_client.ping + "ms";
			}
			other_clients.Add( other_client.net_end_point, oc );
		}
		void
		on_other_client_disconnection( Client.Other_Client other_client )
		{
			UI_Other_Client oc;
			bool b_res = other_clients.TryGetValue( other_client.net_end_point, out oc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received other client disconnection event, but doesn't have that client" );
				return;
			}

			Destroy( oc.button.gameObject );

			other_clients.Remove( other_client.net_end_point );
		}
		void
		on_other_client_name_change( Client.Other_Client other_client )
		{
			UI_Other_Client oc;
			bool b_res = other_clients.TryGetValue( other_client.net_end_point, out oc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received other client name change event, but doesn't have that client" );
				return;
			}

			oc.data = other_client;
			oc.button.ui_client_name_text.text = other_client.name;

			//other_clients[other_client.net_end_point] = oc;
		}
		void
		on_other_client_latency_updated( Client.Other_Client other_client )
		{
			UI_Other_Client oc;
			bool b_res = other_clients.TryGetValue( other_client.net_end_point, out oc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received other client latency updated event, but doesn't have that client" );
				return;
			}

			oc.data = other_client;
			oc.button.ui_ping_ms_text.text = other_client.ping + "ms";

			//other_clients[other_client.net_end_point] = oc;
		}
		void
		client_lobby_on_other_client_click( UI_Other_Client other_client )
		{
		}
		void
		on_client_start_map()
		{
			Debug.Log( "on_client_start_map()" );
			change_screen( UI_Screen_ID.CLIENT_IN_MAP );
		}
		void
		on_client_stop_map()
		{
			Debug.Log( "on_client_stop_map()" );
			change_screen( UI_Screen_ID.CLIENT_LOBBY );
		}

		public
		void
		new_server_name_on_value_change( string server_name )
		{
			bool ok = server_name.Trim().Length >= min_server_name_length;
			ui_button_create.interactable = ok;
			ui_create_server_name_input.text = server_name;
			//Debug.Log( "new_server_name_on_value_change( '" + server_name + "' ) ok = " + ok );
		}

		public
		void
		new_server_name_on_edit_complete( string server_name )
		{
			string untrimmed = server_name;

			server_name = server_name.Trim();
			bool ok = server_name.Length >= min_server_name_length;
			ui_button_create.interactable = ok;
			ui_create_server_name_input.text = server_name;
			//Debug.Log( "new_server_name_on_value_change( '" + untrimmed + "' ) -> '" + server_name + "' ok = " + ok );
		}

		public
		void
		on_new_server_click()
		{
			connected_clients.Clear();
			server.server_name = ui_create_server_name_input.text;
			server.gameObject.SetActive( true );
			server.create();
			change_screen( UI_Screen_ID.SERVER_LOBBY );
		}

		void
		server_on_client_connected( Server.Connected_Client_Data data )
		{
			UI_Connected_Client cc;
			bool b_res = connected_clients.TryGetValue( data.peer.EndPoint, out cc );
			if ( b_res )
			{
				Debug.LogWarning( "ui received client connection event, but already had that client" );
			} else
			{
				GameObject b = Instantiate( connected_client_button_prefab );

				cc = new UI_Connected_Client();

				cc.data = data;

				cc.button = b.GetComponent<UI_Connected_Client_Button>();
				cc.button.ui_button.onClick.AddListener( () => server_lobby_on_connected_client_click( cc ) );
				set_button_style( cc.button.ui_button );
				cc.button.ui_client_name_text.text = data.peer.EndPoint.ToString();
				b.transform.SetParent( ui_server_lobby_clients_list.transform, false );

				set_client_button_latency( cc );
			}
			connected_clients.Add( data.peer.EndPoint, cc );
		}
		void
		server_on_client_disconnected( Server.Connected_Client_Data data )
		{
			UI_Connected_Client cc;
			bool b_res = connected_clients.TryGetValue( data.peer.EndPoint, out cc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received client disconnection event, but doesn't have that client" );
				return;
			}

			Destroy( cc.button.gameObject );

			connected_clients.Remove( data.peer.EndPoint );
		}
		void
		server_lobby_on_connected_client_click( UI_Connected_Client cc )
		{
		}
		void
		on_client_latency_update( Server.Connected_Client_Data data )
		{
			UI_Connected_Client cc;
			bool b_res = connected_clients.TryGetValue( data.peer.EndPoint, out cc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received client latency update event, but does not have that client" );
				return;
			}
			cc.data = data;
			set_client_button_latency( cc );
		}
		void
		on_client_rename( Server.Connected_Client_Data data )
		{
			UI_Connected_Client cc;
			bool b_res = connected_clients.TryGetValue( data.peer.EndPoint, out cc );
			if ( !b_res )
			{
				Debug.LogWarning( "ui received client rename event, but does not have that client" );
				return;
			}
			cc.data = data;
			cc.button.ui_client_name_text.text = data.name;
		}
		void
		set_client_button_latency( UI_Connected_Client cc )
		{
			// TODO(theGiallo): probably latency and ping are the same
			cc.button.ui_ping_ms_text.text =
			            System.String.Format( "{0,7}", "latency" ) + " = " + System.String.Format( "{0,3}", cc.data.latency   ) + "ms"
			   + "\n" + System.String.Format( "{0,7}", "ping"    ) + " = " + System.String.Format( "{0,3}", cc.data.peer.Ping ) + "ms";
		}

		void
		on_nickname_edit_finish( string nickname )
		{
			nickname =
			   ui_client_nickname_inputfield.text =
			      nickname.Trim();
			client.on_rename( nickname );
		}


		void
		change_screen( UI_Screen_ID sid )
		{
			if ( current_screen_id == sid )
			{
				return;
			}
			GameObject new_screen = null;
			switch ( sid )
			{
				case UI_Screen_ID.TITLE_SCREEN:
					new_screen = ui_title_screen;
					break;
				case UI_Screen_ID.SERVER_DISCOVERY:
					new_screen = ui_server_discovery;
					ui_server_discovery_on_enter( current_screen_id );
					break;
				case UI_Screen_ID.SERVER_LOBBY:
					new_screen = ui_server_lobby;
					ui_server_lobby_on_enter( current_screen_id );
					break;
				case UI_Screen_ID.CLIENT_LOBBY:
					new_screen = ui_client_lobby;
					ui_client_lobby_on_enter( current_screen_id );
					break;
				case UI_Screen_ID.CLIENT_IN_MAP:
					new_screen = ui_client_in_game;
					ui_client_in_map_on_enter( current_screen_id );
					break;
				default:
					break;
			}
			if ( current_screen_id != UI_Screen_ID._NONE )
			{
				current_ui_screen.SetActive( false );

				switch ( current_screen_id )
				{
					case UI_Screen_ID.TITLE_SCREEN:
						break;
					case UI_Screen_ID.SERVER_DISCOVERY:
						server_discovery_on_leave( sid );
						break;
					case UI_Screen_ID.SERVER_LOBBY:
						server_lobby_on_leave( sid );
						break;
					case UI_Screen_ID.CLIENT_LOBBY:
						client_lobby_on_leave( sid );
						break;
					case UI_Screen_ID.CLIENT_IN_MAP:
						client_in_map_on_leave( sid );
						break;
					default:
						break;
				}
			}
			current_ui_screen = new_screen;
			current_ui_screen.SetActive( true );
			current_screen_id = sid;
		}

		void
		server_lobby_do_leave()
		{
			change_screen( UI_Screen_ID.SERVER_DISCOVERY );
		}
		void
		client_lobby_do_leave()
		{
			change_screen( UI_Screen_ID.SERVER_DISCOVERY );
		}

		void
		server_lobby_on_leave( UI_Screen_ID to )
		{
			if ( to == UI_Screen_ID.SERVER_DISCOVERY )
			{
				ui_server_lobby_reset();
				server.stop();
				map_container.SetActive( false );
			}
		}
		void
		client_lobby_on_leave( UI_Screen_ID to )
		{
			if ( to == UI_Screen_ID.SERVER_DISCOVERY )
			{
				ui_client_lobby_reset();
				client.disconnect();
			}
		}
		void
		client_in_map_on_leave( UI_Screen_ID to )
		{
			map_container.gameObject.SetActive( false );
			ui_camera.gameObject.SetActive( true );
		}
		void
		server_discovery_on_leave( UI_Screen_ID to )
		{
			server_searcher.gameObject.SetActive( false );
			server_searcher.stop();
		}

		void
		ui_server_discovery_on_enter( UI_Screen_ID from )
		{
			server.gameObject.SetActive( false );
			server.stop();

			client.gameObject.SetActive( false );
			client.disconnect();

			ui_server_discovery_reset();
			ui_server_lobby_reset();
			ui_client_lobby_reset();
			ui_client_in_map_reset();

			connected_clients.Clear();
			other_clients    .Clear();

			server_searcher.gameObject.SetActive( true );
			server_searcher.stop();
			server_searcher.setup();

			Physics.autoSimulation = false;
			Physics.autoSyncTransforms = false;
		}
		void
		ui_server_lobby_on_enter( UI_Screen_ID from )
		{
			if ( from == UI_Screen_ID.SERVER_DISCOVERY )
			{
				//ui_server_lobby_reset();
				//client.disconnect();
				Physics.autoSimulation = true;
				Physics.autoSyncTransforms = true;
			} else
			{
				ui_server_lobby_confirmation_popup.gameObject.SetActive( false );
			}
		}
		void
		ui_client_lobby_on_enter( UI_Screen_ID from )
		{
			if ( from == UI_Screen_ID.SERVER_DISCOVERY )
			{
				//ui_client_lobby_reset();
			} else
			{
				ui_client_lobby_confirmation_popup.gameObject.SetActive( false );
			}
		}
		void
		ui_client_in_map_on_enter( UI_Screen_ID from )
		{
			ui_client_in_map_reset();

			map_container.gameObject.SetActive( true );
			ui_camera.gameObject.SetActive( false );
		}

		void
		ui_server_lobby_reset()
		{
			for ( int i = 0, count = ui_server_lobby_clients_list.transform.childCount; i != count; ++i )
			{
				GameObject.DestroyImmediate( ui_server_lobby_clients_list.transform.GetChild( 0 ).gameObject );
			}
			connected_clients.Clear();
			ui_server_lobby_confirmation_popup.gameObject.SetActive( false );
		}
		void
		ui_server_discovery_reset()
		{
			new_server_name_on_value_change( "" );
			deselect_server();

			for ( int i = 0, count = ui_servers_list.transform.childCount; i != count; ++i )
			{
				GameObject.DestroyImmediate( ui_servers_list.transform.GetChild( 0 ).gameObject );
			}

			discovered_servers.Clear();

			ui_servers_list_popup_message.deactivate();
			ui_button_connect.gameObject.SetActive( true );
		}
		void ui_client_lobby_reset()
		{
			for ( int i = 0, count = ui_client_lobby_clients_list.transform.childCount; i != count; ++i )
			{
				GameObject.DestroyImmediate( ui_client_lobby_clients_list.transform.GetChild( 0 ).gameObject );
			}
			other_clients.Clear();
			ui_client_lobby_confirmation_popup.gameObject.SetActive( false );
		}
		void ui_client_in_map_reset()
		{
		}
	}
}