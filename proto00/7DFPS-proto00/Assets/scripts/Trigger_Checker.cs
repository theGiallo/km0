﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	Trigger_Checker : MonoBehaviour
	{
		public HashSet<Collider> colliding_colliders = new HashSet<Collider>();
		public Collider collider;
		private void OnTriggerEnter( Collider other )
		{
			colliding_colliders.Add( other );
		}
		private void OnTriggerExit( Collider other )
		{
			colliding_colliders.Remove( other );
		}
	}
}