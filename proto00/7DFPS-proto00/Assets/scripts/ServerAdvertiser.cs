﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace fps
{
	public class
	ServerAdvertiser : MonoBehaviour, LiteNetLib.INetEventListener
	{
		public int port;
		[Range(1,128)]
		public int    port_range_length;

		public string server_name;
		public byte   connected_players_count;
		public int    server_port;

		LiteNetLib.NetManager net_manager;
		LiteNetLib.Utils.NetDataWriter data_writer;

		void INetEventListener.OnNetworkError( NetEndPoint endPoint, int socketErrorCode )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkLatencyUpdate( NetPeer peer, int latency )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkReceiveUnconnected( NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType )
		{
			IPEndPoint remote_ipendpoint = remoteEndPoint.EndPoint;
			Debug.Log( "[server advertiser] received from " + remote_ipendpoint + " ( " + remoteEndPoint + " ) " + messageType + " " + reader.GetString() );
		}

		void INetEventListener.OnPeerConnected( NetPeer peer )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnPeerDisconnected( NetPeer peer, DisconnectInfo disconnectInfo )
		{
			throw new System.NotImplementedException();
		}

		void Awake()
		{
		}

		void Start()
		{
			net_manager = new LiteNetLib.NetManager( this, "" );
			//net_manager[i].DiscoveryEnabled = true;

			bool b_res;
			b_res = net_manager.Start( 0 );
			if ( ! b_res )
			{
				Debug.LogError( "failed to start on port 0" );
			} else
			{
			}

			data_writer = new NetDataWriter();
		}

		void Update()
		{
			data_writer.Reset();
			data_writer.Put( ServerSearcher.ADV_HEADER_KEY );
			data_writer.Put( server_name );
			data_writer.Put( server_port );
			data_writer.Put( connected_players_count );
			for ( int i = 0; i != port_range_length; ++i )
			{
				net_manager.SendDiscoveryRequest( data_writer, port + i );
			}
			data_writer.Reset();

			net_manager.PollEvents();
		}

		private void OnDestroy()
		{
			if ( net_manager != null )
			{
				net_manager.Stop();
			}
		}
	}
}