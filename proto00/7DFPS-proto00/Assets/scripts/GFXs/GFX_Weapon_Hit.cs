﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent( typeof ( GFX ) )]
	public class
	GFX_Weapon_Hit : MonoBehaviour
	{
		public float           timer_duration = 0.5f;
		public Utilities.Timer timer;
		public GameObject      hit_effect;
		public AnimationCurve  scale_curve;

		[HideInInspector]
		public GFX             gfx;

		public
		void Awake()
		{
			timer         = new Utilities.Timer( timer_duration );
			gfx           = GetComponent<GFX>();
		}

		void Update()
		{
			if ( timer.active )
			{
				float t = timer.passed_percentage();

				if ( timer.remaining_time() <= 0.0f )
				{
					timer.active = false;
					gfx.put_back_in_pool();
				}
			}

			float scale = scale_curve.Evaluate( timer.passed_percentage() );
			Vector3 scale3 = transform.localScale;
			scale3.y = scale;
			transform.localScale = scale3;

			Vector3 fx_fwd = transform.forward;
			Transform camera_transform = Player_Character.current_player_character.camera.transform;
			Vector3 eye = camera_transform.position - transform.position;
			Vector3 up = transform.up.normalized;
			Vector3 parallel = up * Vector3.Dot( up, eye );
			Vector3 m_norm = parallel - eye;
			transform.rotation = Quaternion.LookRotation( m_norm, up );

			#if NO
			Vector3 s = hit_effect.transform.localScale;
			if ( Vector3.Dot( camera_transform.forward, hit_effect.transform.forward ) < 0 )
			{
				s.y = -1 * Mathf.Abs( s.y );
			} else
			{
				s.y = Mathf.Abs( s.y );
			}
			hit_effect.transform.localScale = s;
			#endif
		}
		private void OnEnable()
		{
			if ( gfx.gameplay_event == null )
			{
				return;
			}
			timer.restart();
			Gameplay_Event.Weapon_Hit weapon_hit = gfx.gameplay_event.weapon_hit;
			transform.position = weapon_hit.position;
			transform.up       = weapon_hit.normal;
			Update();
		}
	}
}
