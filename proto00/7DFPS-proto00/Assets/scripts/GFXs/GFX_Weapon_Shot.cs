﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent( typeof ( GFX ), typeof ( LineRenderer ) )]
	public class
	GFX_Weapon_Shot : MonoBehaviour
	{
		public float           timer_duration = 0.5f;
		public Utilities.Timer timer;
		public Color           beginning_start_color  = Color.red;
		public Color           beginning_end_color    = Color.white;
		public Color           completion_start_color = Color.white;
		public Color           completion_end_color   = Color.red;
		public float           beginning_start_width  = 0.1f;
		public float           beginning_end_width    = 0.01f;
		public float           completion_start_width = 0.01f;
		public float           completion_end_width   = 0.005f;
		public AnimationCurve  curve;

		[HideInInspector]
		public GFX             gfx;
		[HideInInspector]
		public LineRenderer    line_renderer;

		public
		void Awake()
		{
			timer         = new Utilities.Timer( timer_duration );
			gfx           = GetComponent<GFX>();
			line_renderer = GetComponent<LineRenderer>();
		}

		void Update()
		{
			if ( timer.active )
			{
				float t = curve.Evaluate( timer.passed_percentage() );
				line_renderer.startWidth = Mathf.Lerp( beginning_start_width, completion_start_width, t );
				line_renderer.endWidth   = Mathf.Lerp( beginning_end_width,   completion_end_width,   t );
				line_renderer.startColor = Color.Lerp( beginning_start_color, completion_start_color, t );
				line_renderer.endColor   = Color.Lerp( beginning_end_color,   completion_end_color,   t );

				if ( timer.remaining_time() <= 0.0f )
				{
					timer.active = false;
					gfx.put_back_in_pool();
				}
			}
		}
		private void OnEnable()
		{
			if ( gfx.gameplay_event == null )
			{
				return;
			}
			timer.restart();
			Gameplay_Event.Weapon_Shot weapon_shot = gfx.gameplay_event.weapon_shot;
			line_renderer.SetPositions( new Vector3[2] { weapon_shot.start_position, weapon_shot.end_position } );
			Update();
		}
	}
}
