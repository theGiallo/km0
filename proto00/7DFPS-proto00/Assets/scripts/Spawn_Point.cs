﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	Spawn_Point : MonoBehaviour
	{
		private static List<Spawn_Point> instances = new List<Spawn_Point>();
		private static Spawn_Point last_used = null;
		private
		void
		Awake()
		{
			instances.Add( this );
		}
		private void OnDestroy()
		{
			instances.Remove( this );
		}
		public
		static
		Vector3
		get_spawn_point()
		{
			Vector3 ret = Vector3.zero;
			Spawn_Point sp = null;
			do
			{
				int index = Random.Range( 0, instances.Count );
				sp = instances[index];
			} while ( sp == last_used );
			ret = sp.transform.position;
			last_used = sp;
			return ret;
		}
	}
}