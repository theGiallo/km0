﻿using LiteNetLib.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	SP_Manager : MonoBehaviour
	{
		public static SP_Manager instance;
		public static bool is_singleplayer { get { bool ret = instance != null && instance.gameObject.activeInHierarchy; return ret; } }

		public Input_Wrapper    input_wrapper;
		public Player_Character player_character;

		private LiteNetLib.Utils.NetDataWriter w;
		private LiteNetLib.Utils.NetDataReader r;
		private bool first_person_focus = true;

		void
		Awake()
		{
			instance = this;
		}

		void
		Start()
		{
			w = new LiteNetLib.Utils.NetDataWriter();
			r = new LiteNetLib.Utils.NetDataReader();

			Client.setup_controls( input_wrapper );
		}

		void
		Update()
		{
			w.Reset();
			//r.Reset();

			if ( Input.GetKeyDown( KeyCode.Escape ) )
			{
				first_person_focus = !first_person_focus;
			}
			Cursor.lockState = first_person_focus ? CursorLockMode.Locked : CursorLockMode.None;
			if ( first_person_focus )
			{
				Mouse_Wrapper.Mouse_Delta md = new Mouse_Wrapper.Mouse_Delta();
				md.retrieve_values();
				//Debug.Log( "md =  " + md.dx + ", " + md.dy );
				//if ( md.dx != 0 || md.dy != 0 )
				{
					player_character.manage_mouse_delta( md );
				}

				input_wrapper.gather( w );
				if ( w.Length > 0 )
				{
					//Debug.Log( "w.Length = " + w.Length );
					r.SetSource( w );
					r.GetClientMessage_ID();
					List<Input_Wrapper.Action_With_Front> awfs = Input_Wrapper.decode( r );
					player_character.manage_input( awfs );
				}
			}
		}
	}
}