﻿using System.Collections;
using System.Collections.Generic;
using MHLab.Pooling;
using UnityEngine;

namespace fps
{
	using static fps.Utilities;

	[RequireComponent( typeof( Rigidbody ) )]
	public class
	Player_Character : MonoBehaviour
	{
		public static Player_Character current_player_character = null;
		public static bool             current_player_character_is_assigned = false;

		[Header("Properties")]
		public float      max_depenetration_velocity              = 0.2f;
		public float      movement_speed                          = 3.0f;
		public float      air_movement_speed                      = 1.0f;
		public float      current_sprint_speed                    = 0;
		public float      full_air_control_duration               = 0.2f;
		public float      sprint_speed_max                        = 14.0f;
		public float      sprint_start_speed                      = 6.0f;
		public float      sprint_secs_to_full_speed               = 3.0f;
		public float      sprinting_jump_stamina_cost             = 3.0f;
		[Tooltip("computed using sprint_max_duration_secs and sprint_stamina_full_value and... movement ones")]
		public float      sprint_stamina_spent_per_speed_per_secs_computed;
		public ushort     sprint_stamina_full_value               = 10;
		public float      sprint_stamina_recharge_speed           = 0.5f;
		public float      stamina_value                           = 0.0f;
		public float      sprint_max_duration_secs                = 3.0f;
		[Tooltip("computed using sprint_secs_to_full_speed")]
		public float      sprint_acceleration_computed;
		public Vector2    mouse_sensitivity                       = new Vector2( 1, 1 );
		public float      min_pitch_deg, max_pitch_deg;
		public float      max_velocity_y_on_collision             = 0.1f;
		public float      mass_perc_value_push                    = 0.25f;
		public float      wall_fall_velocity                      = 1.0f;
		public float      max_walkable_step_height                = 0.5f;
		public float      max_walkable_step_height_backwards      = 0.5f;
		public float      min_height_that_needs_stepping          = 0.1f;
		public float      min_y_vel_for_stepping                  = 2.0f;
		public float      stepping_capsule_rotation_deg           = 20.0f;
		public float      stepping_pitch_duration                 = 0.5f;
		public float      stepping_point_checker_delta_y          = 0.2f;
		public float      stepping_point_checker_y_offset         = 0.1f;
		public float      crippled_stepping_y_vel_percentage      = 0.25f;
		public float      crippled_walk_jump_force                = 30000.0f;
		public float      crippled_crippling_jump_force           = 50000.0f;
		public float      crippled_walk_force_jump_angle          = 45.0f;
		public float      crippled_jump_force_modifier            = 0.6f;
		public float      crippled_jump_force_modifier_single_leg = 0.4f;
		//public float      crippled_jump_ahead_modifier            = 2.0f;
		public float      jump_one_leg_velocity                   = 2.0f;
		public float      crippled_movement_modifier              = 0.5f;
		public float      crippling_duration                      = 2.0f;
		public float      crippling_duration_random_range         = 0.5f;
		public float      wall_normal_force                       = 1.0f;
		public float      wall_down_speed_percentage              = 0.1f;
		public float      respawn_time                            = 10.0f;

		public float      min_colliding_height_to_jump_single_leg = 0.5f;
		public float      min_colliding_height_to_jump            = 0.1f;
		public float      jump_force                              = 10.0f;
		public float      jump_movement_direction_multiplier      = 5.0f;
		//public float      stepping_point_checker_min_y = ;

		[Header("Components")]
		public Camera          camera;
		public AudioListener   audio_listener;
		public CapsuleCollider capsule;
		public GameObject      stepping_point;
		public GameObject      stepping_point_debug;
		public GameObject      stepping_point_checker_pivot;
		public Trigger_Checker stepping_point_checker;
		public GameObject      yaw_rotated_object;
		public GameObject      pitch_rotated_object;

		public Animator animator;
		public string   animation_name_idle     = "idle";
		public string   animation_name_run      = "run";
		public string   animation_name_jump     = "jump";
		public string   animation_name_velocity = "velocity";

		public GameObject                 hitboxes_container;
		public GameObject                 debug_hit_point;
		public GameObject                 debug_hit_point_pc;
		public GameObject[]               debug_true_hit_points;
		public GameObject[]               debug_true_hit_points_pc;
		public GameObject[]               weapons;
		public GameObject[]               weapons_shoot_points;
		public GameObject[]               weapons_shoot_points_back;
		public PC_Hitbox.Hitbox_Part_ID[] weapons_parts_attached_to;
		public byte[]                     weapons_damage;
		public byte[]                     parts_hps_max;
		public byte[]                     parts_hps_current;

		[Header("Globals")]
		public  string layer_name_pc_hitbox;
		public  string layer_name_environment;
		public  string layer_name_props;
		public  string layer_name_physical_objects;
		private int    ray_layer_mask;
		private int    layer_pc_hitbox;
		private int    layer_environment;
		private int    layer_props;
		private int    layer_physical_objects;

		[Header("Internals")]
		public  float                     yaw_deg;
		public  float                     pitch_deg;
		public  float                     animator_parameter_velocity;
		public  bool                      animator_parameter_run;
		public  bool                      animator_parameter_idle;
		public  bool                      animator_parameter_jump;

		private bool[]                    movement_directions_active = new bool[4];
		private Mouse_Wrapper.Mouse_Delta md;
		private Vector3[]                 dumb_movement_map = new Vector3[4];
		private Vector3                   total_movement = Vector3.zero;
		private Vector3                   total_movement_direction = Vector3.zero;
		private Quaternion                prev_rotation;
		private Quaternion                yaw_rotated_prev_rotation;
		private Quaternion                pitch_rotated_prev_rotation;
		private Rigidbody                 rigidbody;
		private Vector3                   last_position;
		private Collision                 last_collision;
		private Collision                 last_collision_exited;
		private Vector3                   last_v;
		private Dictionary<Collider,Collision> colliders_currently_colliding = new Dictionary<Collider, Collision>();
		private Timer                     stepping_pitch_timer;
		private Timer                     air_control_timer;
		private bool                      has_jumped_in_this_action = false;
		private bool                      jump_action_requested = false;
		private bool                      last_fixed_time_step_was_colliding = false;
		private bool                      sprint_action_requested = false;
		private bool                      is_sprinting = false;
		private bool                      arm_left_use_requested;
		private bool                      arm_right_use_requested;
		private bool                      dead;
		private Timer                     respawn_timer;
		private PC_Hitbox[]               hitboxes;
		private Timer                     crippling_fixed_timer;
		private int                       animation_hash_idle;
		private int                       animation_hash_run;
		private int                       animation_hash_jump;
		private int                       animation_hash_velocity;

		public
		void
		Awake()
		{
			// NOTE(theGiallo): in this way the server sees the first PC view
			if ( current_player_character_is_assigned )
			{
				setup_as_non_current();
			} else
			{
				set_current();
			}
			rigidbody = GetComponent<Rigidbody>();
			#if NO
			if ( !Server.has_authority && !SP_Manager.is_singleplayer )
			{
				rigidbody.detectCollisions = false;
			}
			#endif

			ray_layer_mask = LayerMask.GetMask( layer_name_pc_hitbox,
			                                    layer_name_environment,
			                                    layer_name_props,
			                                    layer_name_physical_objects );
			layer_pc_hitbox        = LayerMask.NameToLayer( layer_name_pc_hitbox );
			layer_environment      = LayerMask.NameToLayer( layer_name_environment );
			layer_props            = LayerMask.NameToLayer( layer_name_props );
			layer_physical_objects = LayerMask.NameToLayer( layer_name_physical_objects );

			last_position = transform.position;

			if ( !Server.has_authority && !SP_Manager.is_singleplayer )
			{
				animator.enabled = false;
			}
		}

		void
		Start()
		{
			dumb_movement_map[(int) Input_Wrapper.Action.MOVE_FORWARD  - (int) Input_Wrapper.Action.MOVE_FORWARD  ] = new Vector3(  0,  0,  1 );
			dumb_movement_map[(int) Input_Wrapper.Action.MOVE_BACKWORD - (int) Input_Wrapper.Action.MOVE_FORWARD  ] = new Vector3(  0,  0, -1 );
			dumb_movement_map[(int) Input_Wrapper.Action.STRAFE_LEFT   - (int) Input_Wrapper.Action.MOVE_FORWARD  ] = new Vector3( -1,  0,  0 );
			dumb_movement_map[(int) Input_Wrapper.Action.STRAFE_RIGHT  - (int) Input_Wrapper.Action.MOVE_FORWARD  ] = new Vector3(  1,  0,  0 );
			prev_rotation = transform.rotation;

			  yaw_rotated_prev_rotation =   yaw_rotated_object.transform.localRotation;
			pitch_rotated_prev_rotation = pitch_rotated_object.transform.localRotation;

			stepping_pitch_timer = new Timer( stepping_pitch_duration );
			air_control_timer    = new Timer( full_air_control_duration );
			respawn_timer        = new Timer( respawn_time );


			compute_sprint_acceleration();
			compute_stamina_consumption_speed();
			stamina_value = sprint_stamina_full_value;
			if ( current_player_character == this )
			{
				UI_In_Game.instance.stamina_bar._max_value = sprint_stamina_full_value;
			}

			rigidbody.maxDepenetrationVelocity = max_depenetration_velocity;

			hitboxes = hitboxes_container.GetComponentsInChildren<PC_Hitbox>();
			for ( int i = 0; i != hitboxes.Length; ++i )
			{
				hitboxes[i].pc = this;
			}

			crippling_fixed_timer = new Timer( crippling_duration, false );
			crippling_fixed_timer.use_fixed_time = true;


			animation_hash_idle = Animator.StringToHash( animation_name_idle );
			animation_hash_run  = Animator.StringToHash( animation_name_run  );
			animation_hash_jump = Animator.StringToHash( animation_name_jump );
			animation_hash_velocity = Animator.StringToHash( animation_name_velocity );

			respawn();
		}

		void
		Update()
		{
			if ( dead )
			{
				if ( respawn_timer.remaining_percentage() <= 0 )
				{
					respawn();
				}
			}

			if ( this == current_player_character )
			{
				UI_In_Game.instance.stamina_bar.value = stamina_value;
			}

			if ( dead || !has_to_be_updated() )
			{
				return;
			}

			// NOTE(theGiallo): compute movement direction
			{
				total_movement = Vector3.zero;
				total_movement_direction = Vector3.zero;

				bool some = false;
				for ( int i = 0; i != 4; ++i )
				{
					bool a = movement_directions_active[i];
					some = some || a;
					if ( a )
					{
						total_movement += dumb_movement_map[i];
					}
				}
				if ( some && total_movement != Vector3.zero )
				{
					total_movement_direction = total_movement.normalized;
				}
			}

			// NOTE(theGiallo): shoot
			{
				Ray ray = new Ray( origin: camera.transform.position, direction: camera.transform.forward );
				float max_ray_distance = 100;
				RaycastHit hit_info;
				hitboxes_set_enabled( false );
				bool did_hit_something = Physics.Raycast( ray, out hit_info, max_ray_distance, ray_layer_mask );
				hitboxes_set_enabled( true );
				if ( did_hit_something )
				{
					Vector3 hit_point = hit_info.point;
					Collider collider = hit_info.collider;
					if ( collider.gameObject.layer == layer_pc_hitbox )
					{
						debug_hit_point_pc.SetActive( true );
						debug_hit_point_pc.transform.position = hit_point;
						debug_hit_point   .SetActive( false );
						PC_Hitbox hb = collider.gameObject.GetComponent<PC_Hitbox>();
					} else
					{
						debug_hit_point_pc.SetActive( false );
						debug_hit_point   .SetActive( true );
						debug_hit_point   .transform.position = hit_point;
					}


					// NOTE(theGiallo): align weapons to target
					for ( int i = 0; i != weapons.Length; ++i )
					{
						GameObject weapon                    = weapons                  [i];
						GameObject weapon_shoot_point        = weapons_shoot_points     [i];
						GameObject weapon_shoot_point_back   = weapons_shoot_points_back[i];
						GameObject debug_true_hit_point_pc   = debug_true_hit_points_pc [i];
						GameObject debug_true_hit_point      = debug_true_hit_points    [i];
						PC_Hitbox.Hitbox_Part_ID attached_to = weapons_parts_attached_to[i];
						byte       weapon_damage             = weapons_damage           [i];
						byte       part_hps                  = parts_hps_current        [(int)attached_to];

						Vector3 O3 = weapon.transform.position;

						weapon.transform.LookAt( hit_point, Vector3.up );

						Vector3 weapon_world_eulers = weapon.transform.rotation.eulerAngles;
						{
							Vector3 P3 = weapon_shoot_point.transform.localPosition;
							Vector3 B3 = weapon_shoot_point_back.transform.localPosition;
							Vector2 P  = new Vector2( P3.x, P3.z );
							Vector2 B  = new Vector2( B3.x, B3.z );
							Vector3 T3 = weapon.transform.InverseTransformPoint( hit_point );
							Vector2 T  = new Vector2( T3.x, T3.z );
							float y_rotation = get_angle_deg_to_align_two_points_to_target( T, P, B );
							weapon_world_eulers.y = deg_to_0_360( weapon_world_eulers.y + y_rotation );
							weapon.transform.rotation = Quaternion.Euler( weapon_world_eulers );
						}

						{
							Vector3 P3 = weapon_shoot_point.transform.localPosition;
							Vector3 B3 = weapon_shoot_point_back.transform.localPosition;
							Vector3 T3 = weapon.transform.InverseTransformPoint( hit_point );
							Vector2 P  = new Vector2( P3.z, P3.y );
							Vector2 B  = new Vector2( B3.z, B3.y );
							Vector2 T  = new Vector2( T3.z, T3.y );
							float x_rotation = get_angle_deg_to_align_two_points_to_target( T, P, B );
							weapon_world_eulers = weapon.transform.rotation.eulerAngles;
							weapon_world_eulers.x = deg_to_0_360( weapon_world_eulers.x - x_rotation );
							weapon.transform.rotation = Quaternion.Euler( weapon_world_eulers );
						}
					}
				}

				// NOTE(theGiallo): check for weapons targets and for shooting
				for ( int i = 0; i != weapons.Length; ++i )
				{
					GameObject weapon                    = weapons                  [i];
					GameObject weapon_shoot_point        = weapons_shoot_points     [i];
					GameObject weapon_shoot_point_back   = weapons_shoot_points_back[i];
					GameObject debug_true_hit_point_pc   = debug_true_hit_points_pc [i];
					GameObject debug_true_hit_point      = debug_true_hit_points    [i];
					PC_Hitbox.Hitbox_Part_ID attached_to = weapons_parts_attached_to[i];
					byte       weapon_damage             = weapons_damage           [i];
					byte       part_hps                  = parts_hps_current        [(int)attached_to];



					PC_Hitbox hb = null;
					Vector3  true_hit_point;
					Collider true_collider;
					Ray true_ray = new Ray( origin: weapon_shoot_point.transform.position,
					                        direction: weapon_shoot_point     .transform.position
					                                 - weapon_shoot_point_back.transform.position );
					RaycastHit true_hit_info;
					hitboxes_set_enabled( false );
					bool true_did_hit_something = Physics.Raycast( true_ray, out true_hit_info, max_ray_distance, ray_layer_mask );
					hitboxes_set_enabled( true );
					if ( true_did_hit_something )
					{
						true_hit_point = true_hit_info.point;
						true_collider  = true_hit_info.collider;
						if ( true_collider.gameObject.layer == layer_pc_hitbox )
						{
							debug_true_hit_point_pc.SetActive( true );
							debug_true_hit_point_pc.transform.position = true_hit_point;
							debug_true_hit_point   .SetActive( false );
							hb = true_collider.gameObject.GetComponent<PC_Hitbox>();
						} else
						{
							debug_true_hit_point_pc.SetActive( false );
							debug_true_hit_point   .SetActive( true );
							debug_true_hit_point.transform.position = true_hit_point;
						}
					} else
					{
						float max_weapon_distance = 1000.0f;
						true_hit_point = true_ray.origin + true_ray.direction * max_weapon_distance;
						debug_true_hit_point_pc.SetActive( false );
						debug_true_hit_point   .SetActive( false );
					}

					bool using_arm =
					    ( arm_left_use_requested
					   && part_hps > 0
					   && ( attached_to == PC_Hitbox.Hitbox_Part_ID.ARM_1_L
					     || attached_to == PC_Hitbox.Hitbox_Part_ID.ARM_2_L ) )
					 || ( arm_right_use_requested
					   && part_hps > 0
					   && ( attached_to == PC_Hitbox.Hitbox_Part_ID.ARM_1_R
					     || attached_to == PC_Hitbox.Hitbox_Part_ID.ARM_2_R ) );
					if ( using_arm )
					{
						if ( hb != null )
						{
							Debug.Log( "shooted " + hb.type + " of " + hb.gameObject.name + " dealing " + weapon_damage + " damages" );
							if ( hb.pc != null )
							{
								Debug.Log( "part hps before = " + hb.pc.parts_hps_current[(int)hb.type] );
								hb.pc.parts_hps_current[(int)hb.type] =
								   (byte)Mathf.Max( 0, hb.pc.parts_hps_current[(int)hb.type] - weapon_damage );
								Debug.Log( "part hps after  = " + hb.pc.parts_hps_current[(int)hb.type] );
							}
						} else
						{
							Debug.Log( "hit no player character" );
						}
						// NOTE(theGiallo): shoot gfx will be played on the clients
						{
							Gameplay_Event gameplay_event = new Gameplay_Event();
							Gameplay_Event.Weapon_Shot weapon_shot = new Gameplay_Event.Weapon_Shot();
							weapon_shot.start_position = weapon_shoot_point.transform.position;
							weapon_shot.end_position = true_hit_point;
							gameplay_event.set( weapon_shot );
							Gameplay_Event.enqueue_event( gameplay_event );
						}
						if ( did_hit_something )
						{
							Gameplay_Event gameplay_event = new Gameplay_Event();
							Gameplay_Event.Weapon_Hit weapon_hit = new Gameplay_Event.Weapon_Hit();
							weapon_hit.position = true_hit_point;
							weapon_hit.normal = true_hit_info.normal;
							weapon_hit.hit_material_type = Sound_Material.get_sound_material( true_hit_info.collider );
							gameplay_event.set( weapon_hit );
							Gameplay_Event.enqueue_event( gameplay_event );
						}
					}
				}
				arm_left_use_requested = arm_right_use_requested = false;
			} // shoot
		}

		private void FixedUpdate()
		{
			if ( dead || !has_to_be_updated() )
			{
				return;
			}

			if ( stepping_pitch_timer.active && stepping_pitch_timer.remaining_percentage() <= 0.0f )
			{
				capsule.transform.localRotation = new Quaternion();
				stepping_pitch_timer.active     = false;
			}

			bool is_colliding_with_something = colliders_currently_colliding.Count != 0;
			if ( last_fixed_time_step_was_colliding && !is_colliding_with_something && !is_sprinting )
			{
				air_control_timer.restart( full_air_control_duration );
			} else
			if ( is_colliding_with_something )
			{
				air_control_timer.active = false;
			}

			//Debug.Log( "FixedUpdate " + Time.frameCount + " Time.fixedDeltaTime = " + Time.fixedDeltaTime );
			Vector3 right   = -Vector3.Cross( transform.forward, Vector3.up );
			Vector3 up      = Vector3.up;
			Vector3 forward = Vector3.Cross( right, up );
			//Debug.Log( "right = " + right + " up = " + up + " forward = " + forward + " tr right = " + transform.right + " tr fw = " + transform.forward + " tr up = " + transform.up );


			Vector2    angle_deg = new Vector2( md.dx, md.dy );
			//Debug.Log( "    md =  "  + md.dx + ", " + md.dy );
			//angle_deg.x *= mouse_sensitivity.x * 1024.0f * Time.fixedDeltaTime;
			//angle_deg.y *= mouse_sensitivity.y * 1024.0f * Time.fixedDeltaTime;
			#if NO
			Quaternion yaw       = Quaternion.AngleAxis(  angle_deg.x, up    );
			Quaternion pitch     = Quaternion.AngleAxis( -angle_deg.y, right );
			//prev_rotation = transform.rotation = pitch * yaw * prev_rotation;
			#else
			yaw_deg   +=  angle_deg.x;
			pitch_deg += -angle_deg.y;
			//yaw_deg = yaw_deg - 360.0f * Mathf.Floor( yaw_deg / 360.0f );
			//pitch_deg = pitch_deg - 360.0f * Mathf.Floor( pitch_deg / 360.0f );
			yaw_deg   = deg_to_center0( yaw_deg );
			pitch_deg = deg_to_center0( pitch_deg );
			//Debug.Log( "pitch_deg = " + pitch_deg + " min_pitch_deg = " + min_pitch_deg + " max_pitch_deg = " + max_pitch_deg );
			min_pitch_deg = deg_to_center0( min_pitch_deg );
			max_pitch_deg = deg_to_center0( max_pitch_deg );
			//Debug.Log( " min_pitch_deg = " + min_pitch_deg + " max_pitch_deg = " + max_pitch_deg );
			//Debug.Log( "pitch_deg = " + pitch_deg + " min_pitch_deg = " + min_pitch_deg + " max_pitch_deg = " + max_pitch_deg );
			pitch_deg = clamp_deg_center0( pitch_deg, min_pitch_deg, max_pitch_deg );
			//Debug.Log( "pitch_deg = " + pitch_deg );
			#if NO
			Quaternion yaw       = Quaternion.AngleAxis( yaw_deg,   up    );
			Quaternion pitch     = Quaternion.AngleAxis( pitch_deg, right );
			prev_rotation = transform.rotation = pitch * yaw;// * prev_rotation;
			#else
			//Quaternion yaw       = Quaternion.AngleAxis( yaw_deg,     yaw_rotated_object.transform.worldToLocalMatrix * up    );
			//Quaternion pitch     = Quaternion.AngleAxis( pitch_deg, pitch_rotated_object.transform.worldToLocalMatrix * right );

			Vector3 euler_yaw = yaw_rotated_object.transform.localRotation.eulerAngles;
			euler_yaw.y = yaw_deg;
			//Debug.Log( "pitch_rotated_object.transform.localRotation.eulerAngles = "
			//          + pitch_rotated_object.transform.localRotation.eulerAngles );
			Vector3 euler_pitch = pitch_rotated_object.transform.localRotation.eulerAngles;
			//Debug.Log( "pitch_rotated_object.transform.localRotation.eulerAngles = "
			//          + pitch_rotated_object.transform.localRotation.eulerAngles );
			euler_pitch.x = pitch_deg;
			euler_pitch.y = 0;
			euler_pitch.z = 0;
			Quaternion yaw   = Quaternion.Euler( euler_yaw );
			Quaternion pitch = Quaternion.Euler( euler_pitch );
			//  yaw_rotated_object.transform.localRotation = yaw;
			//pitch_rotated_object.transform.localRotation = pitch;

			  yaw_rotated_prev_rotation =   yaw_rotated_object.transform.localRotation = yaw;// * prev_rotation;
			pitch_rotated_prev_rotation = pitch_rotated_object.transform.localRotation = pitch;// * prev_rotation;
			#endif
			#endif


			// TODO(theGiallo): move this booleans computation in Update
			bool has_both_legs =
			   parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_L] > 0
			&& parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_L] > 0
			&& parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_R] > 0
			&& parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_R] > 0;
			bool has_at_least_one_healthy_leg =
			   (    parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_L] > 0
			     && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_L] > 0 )
			|| (    parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_R] > 0
			     && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_R] > 0 );
			bool has_a_destroyed_leg =
			   (    parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_L] == 0
			     && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_L] == 0 )
			|| (    parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_R] == 0
			     && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_R] == 0 );
			bool has_no_legs =
			      parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_L] == 0
			   && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_L] == 0
			   && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_1_R] == 0
			   && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.LEG_2_R] == 0;

			Vector3 walk_direction =
			   right   * total_movement_direction.x
			 + up      * total_movement_direction.y
			 + forward * total_movement_direction.z;

			Vector3 original_velocity = rigidbody.velocity;

			bool sprinting_ended = false;
			bool is_in_full_movement_control =
			    is_colliding_with_something
			 || ! air_control_timer.active
			 || air_control_timer.remaining_percentage() > 0.0f;
			//if ( !is_in_full_movement_control )
			//{
			//	Debug.Log( "! is_in_full_movement_control" );
			//}
			//float current_movement_speed = is_in_full_movement_control ? movement_speed : air_movement_speed;
			float current_movement_speed = movement_speed;
			if ( !is_in_full_movement_control )
			{
				current_movement_speed = air_movement_speed;
				sprinting_ended = true;
			} else
			if ( is_colliding_with_something
			  && sprint_action_requested
			  && has_both_legs
			  && stamina_value > 0
			  && total_movement_direction != Vector3.zero )
			{
				// TODO(theGiallo): integrate properly
				float new_stamina_value = stamina_value -
				   ( 0.5f * sprint_acceleration_computed * Time.fixedDeltaTime * Time.fixedDeltaTime + current_sprint_speed * Time.fixedDeltaTime )
				   * sprint_stamina_spent_per_speed_per_secs_computed;
				current_sprint_speed = Mathf.Max( current_sprint_speed, sprint_start_speed );
				current_sprint_speed = current_sprint_speed + Time.fixedDeltaTime * sprint_acceleration_computed;
				current_sprint_speed = Mathf.Min( current_sprint_speed, sprint_speed_max );
				current_movement_speed = current_sprint_speed;
				set_stamina_value( new_stamina_value );
				is_sprinting = true;
			} else
			{
				current_sprint_speed = Vector3.Dot( original_velocity, walk_direction );
				sprinting_ended = true;
			}

			if ( !sprint_action_requested && !is_sprinting && stamina_value < sprint_stamina_full_value )
			{
				set_stamina_value( stamina_value + sprint_stamina_recharge_speed * Time.fixedDeltaTime );
			}

			Vector3 delta_movement = walk_direction * current_movement_speed;// * Time.fixedDeltaTime;
			//transform.position += delta_movement;
			Vector3 v = original_velocity;
			//if ( ! has_a_destroyed_leg )
			//if ( has_both_legs )
			{
				if ( ! has_both_legs )
				{
					delta_movement *= crippled_movement_modifier;
				}
				//if ( has_a_destroyed_leg && is_colliding_with_something )
				//{
				//	delta_movement.x = 0;
				//	delta_movement.z = 0;
				//}
				if ( has_a_destroyed_leg && is_colliding_with_something )
				{
					v.x = v.z = 0;
				} else
				//if ( !has_a_destroyed_leg || !is_colliding_with_something )
				{
					#if NO
					v.x = delta_movement.x;
					v.z = delta_movement.z;
					#else
					v.x = compute_movement_velocity( v.x, delta_movement.x, is_in_full_movement_control );
					v.z = compute_movement_velocity( v.z, delta_movement.z, is_in_full_movement_control );
					#endif
				}
				if ( ! has_both_legs && is_colliding_with_something && total_movement_direction != Vector3.zero )
				{
					if ( crippling_fixed_timer.remaining_time() <= 0.0f )
					{
						if ( has_a_destroyed_leg )
						{
						//	rigidbody.AddForce( Quaternion.AngleAxis( -crippled_walk_force_jump_angle, right ) * walk_direction * crippled_walk_jump_force );
						} else
						{
							rigidbody.AddForce( Vector3.up * crippled_crippling_jump_force );
						}
						float dur = crippling_duration + Random.Range( - crippling_duration_random_range, crippling_duration_random_range );
						crippling_fixed_timer.restart( dur );
					}
				}
			}

			//if ( transform.position == last_position )
			//Debug.Log( "   position = " + transform.position + " same as last = " + ( transform.position == last_position ) );

			bool    all_collisions_are_walkable = true;
			bool    has_to_step                 = true;
			float   max_step                    = float.MinValue;
			float   min_colliding_height        = float.MaxValue;
			Vector3 pos                         = transform.position;
			Vector3 local_up                    = transform.localRotation * Vector3.up;
			local_up.Normalize();
			float   max_forward_angle_cos       = float.MinValue;
			Vector3 forwardest_point            = Vector3.zero;
			Vector3 forwardest_dir              = Vector3.zero;
			Vector3 forwardest_normal           = Vector3.zero;
			float   forwarders_point_height     = float.MinValue;


			Vector3 normal = Vector3.zero;
			foreach ( Collision collision in colliders_currently_colliding.Values )
			{
				Rigidbody rb = collision.collider.gameObject.GetComponent<Rigidbody>();
				if ( rb != null && rb.mass < rigidbody.mass * mass_perc_value_push )
				{
					continue;
				}
				float   collision_max_height               = float.MinValue;
				Vector3 collision_highest_point            = Vector3.zero;
				Vector3 collision_highest_dir              = Vector3.zero;
				Vector3 collision_highest_normal           = Vector3.zero;
				float   collision_highest_cos_angle        = -2;
				for ( int i = 0, count = collision.contacts.Length; i != count; ++i )
				{
					Vector3 candidate_normal = collision.contacts[i].normal;
					float dot = Vector3.Dot( v.normalized, candidate_normal.normalized );
					if ( dot < -0.1 )
					{
						//Debug.Log( "dot = " + dot + " v = " + v + " adding collider normal " + candidate_normal + " " + collision.collider.gameObject.name );
						normal += candidate_normal;
					}

					Vector3 p = collision.contacts[i].point;
					Vector3 rel_p = p - pos;
					Vector3 rel_p_n = rel_p.normalized;
					float cos_angle = Vector3.Dot( walk_direction, rel_p_n );
					float d = Vector3.Dot( local_up, rel_p );
					//Debug.Log( "cos_angle = " + cos_angle );

					//if ( cos_angle > Mathf.Cos( Mathf.PI * 0.45f ) )
					//{
						max_step = Mathf.Max( max_step, d );
						min_colliding_height = Mathf.Min( min_colliding_height, d );
					//	//all_collisions_are_walkable = all_collisions_are_walkable && d <= max_walkable_step_height;
					//	//Debug.Log( "d = " + d );
					//	Debug.Log( "OK cos_angle = " + cos_angle + " d = " + d );
					//} else
					//{
					//	Debug.Log( "NO cos_angle = " + cos_angle + " d = " + d );
					//}
					if ( collision_max_height < d || ( collision_max_height == d && cos_angle < collision_highest_cos_angle ) )
					{
						collision_max_height        = d;
						collision_highest_cos_angle = cos_angle;
						collision_highest_dir       = rel_p_n;
						collision_highest_normal    = candidate_normal;
						collision_highest_point     = p;
					}
				}
				if ( collision_highest_cos_angle > max_forward_angle_cos )
				{
					max_forward_angle_cos   = collision_highest_cos_angle;
					forwardest_point        = collision_highest_point;
					forwardest_dir          = collision_highest_dir;
					forwardest_normal       = collision_highest_normal.normalized;
					forwarders_point_height = collision_max_height;
				}
			}
			if ( ! v3_eps( delta_movement, Vector3.zero, 0.01f ) )
			{

				//all_collisions_are_walkable = max_step < ( total_movement.z >= 0 ? max_walkable_step_height : max_walkable_step_height_backwards );
				//Vector3 step_dir = new Vector3( -normal.x, 0, -normal.z );
				Vector3 step_dir                = new Vector3( forwardest_dir.x, 0, forwardest_dir.z );
				Vector3 local_step_dir          = transform.InverseTransformDirection( step_dir );
				Vector2 local_step_dir2         = new Vector2( local_step_dir.x, local_step_dir.z );
				float   local_step_angle        = Vector2.Angle( Vector2.up, local_step_dir2 );
				float   local_step_angle_signed = -Vector2.SignedAngle( Vector2.up, local_step_dir2 );
				float   steppable_height;
				{
					float x = local_step_angle / 180.0f;
					float omx = 1 - x;
					float omx2 = omx * omx;
					float x2 = x * x;
					steppable_height = Mathf.Lerp( a: max_walkable_step_height_backwards,
					                               b: max_walkable_step_height,
					                               t: 1 - x2 * x2 );
					                               //t: Mathf.Sqrt( Mathf.Sqrt( local_step_angle / 180.0f ) ) );
				}

				stepping_point_debug.transform.localPosition = local_step_dir.normalized * 0.5f;
				stepping_point.transform.position = forwardest_point;
				{
					Vector3 r = Vector3.zero;
					r.y = local_step_angle_signed;
					stepping_point_checker_pivot.transform.localRotation = Quaternion.Euler( r );
					Vector3 p = stepping_point_checker_pivot.transform.localPosition;
					p.y = stepping_point.transform.localPosition.y + stepping_point_checker_delta_y + stepping_point_checker_y_offset;
					float stepping_point_checker_min_y = max_walkable_step_height + stepping_point_checker_y_offset;
					p.y = Mathf.Max( stepping_point_checker_min_y, p.y );
					stepping_point_checker_pivot.transform.localPosition = p;

					p = stepping_point_checker.collider.transform.localPosition;
					p.z = 0.5f + stepping_point_checker_delta_y;
					stepping_point_checker.collider.transform.localPosition = p;
				}
				stepping_point_checker_pivot.transform.localScale = new Vector3( 1,1,1 ) * stepping_point_checker_delta_y * 2;
				bool is_step_location_free = stepping_point_checker.colliding_colliders.Count == 0;
				//Debug.Log( "stepping_point_checker.colliding_colliders.Count = " + stepping_point_checker.colliding_colliders.Count );
				//stepping_point_checker.transform.position = forwardest_point + Vector3.up * stepping_point_checker_delta_y;
				//Debug.Log( "stepping_point_checker.colliding_colliders.Count = " + stepping_point_checker.colliding_colliders.Count );


				//Debug.Log( "delta_movement.normalized = " + delta_movement.normalized );
				//Debug.Log( "step_dir        = " + step_dir        );
				//Debug.Log( "local_step_dir  = " + local_step_dir );
				//Debug.Log( "local_step_dir2 = " + local_step_dir2 );
				//Debug.Log( "total_movement_direction = " + total_movement_direction );
				//Debug.Log( "local_step_angle = " + local_step_angle );
				all_collisions_are_walkable = max_step < steppable_height && is_step_location_free;
				has_to_step = max_step > min_height_that_needs_stepping && is_step_location_free;
				//if ( all_collisions_are_walkable )
				//{
				//	Debug.Log( "all collisions are walkable: steppable height = " + steppable_height + " max_step = " + max_step );
				//} else
				//{
				//	Debug.Log( "not all collisions are walkable: steppable height = " + steppable_height + " max_step = " + max_step );
				//}
				//Debug.Log( "steppable height = " + steppable_height + " max_step = " + max_step + " all_collisions_are_walkable = " + all_collisions_are_walkable );

				// TODO(theGiallo): make one leg step big things, but only big ones
				if ( Vector3.Dot( v, normal ) < 0
				//  && last_v != Vector3.zero
				//  && Vector3.Dot( last_v, normal ) < 0
				)
				{
					/*
					 v = (0.0, 0.0, -3.0) new_v = (0.0, 1.1, -0.5) normal = (0.0, 0.4, 0.9)
					 v = (0.0, 0.0, -3.0) new_v = (0.0, 1.1, -0.5) normal = (0.0, 0.4, 0.9) dot = -2.765224
					 v = (0.4, 0.0, -3.0) new_v = (0.4, 1.1, -0.4) normal = (0.0, 0.4, 0.9) dot = -2.743009 dot_n = -0.9143358
					 */
					normal.Normalize();
					normal.Normalize();
					float dot = Vector3.Dot( v, normal );
					float dot_n = Vector3.Dot( v.normalized, normal );
					Vector3 new_v = v - normal * dot;
					//Debug.Log( "v = " + v + " new_v = " + new_v + " normal = " + normal + " dot = " + dot + " dot_n = " + dot_n );
					//Debug.Log( "|v| = " + v.magnitude + " |new_v| = " + new_v.magnitude );

					if ( all_collisions_are_walkable )
					{
						if ( has_to_step )
						{
							//Debug.Log( "steppable height = " + steppable_height + " max_step = " + max_step + " all_collisions_are_walkable = " + all_collisions_are_walkable );
							//Debug.Log( "stepping" );
							float a = max_step / max_walkable_step_height;
							float oma = 1.0f - a;
							float p = 1.0f - oma*oma*oma*oma;

							//new_v.y = Mathf.Max( new_v.y, min_y_vel_for_stepping * p );
							new_v.y = min_y_vel_for_stepping * p;
							if ( !has_both_legs )
							{
								new_v.y *= crippled_stepping_y_vel_percentage;
							}

							//Debug.Log( "a = " + a + " p = " +  p + " fw normal = " + forwardest_normal  );
							//Debug.Log( "    new_v = " + new_v );
							Vector3 local_v = total_movement_direction;
							Vector2 local_v2 = new Vector2( local_v.x, local_v.z );
							Vector3 cr = capsule.transform.localRotation.eulerAngles;
							cr.x = stepping_capsule_rotation_deg;
							cr.y = local_step_angle_signed;
							//Quaternion.LookRotation( new Vector3( total_movement_direction.x, 0, total_movement.z ) );
							//Debug.Log( "vel angle = " + cr.y );
							capsule.transform.localRotation = Quaternion.Euler( cr );
							stepping_pitch_timer.restart( stepping_pitch_duration );
							if ( is_sprinting )
							{
								Vector3 vv = capsule.transform.up * new_v.y;
								new_v.x += vv.x;
								new_v.z += vv.z;

								new_v.y  = vv.y;
							} else
							{
								new_v = capsule.transform.up * new_v.y;
							}
						} else
						{
							//Debug.Log( "didn't have to step | steppable height = " + steppable_height + " max_step = " + max_step + " all_collisions_are_walkable = " + all_collisions_are_walkable );
						}
					} else
					if ( new_v.y > 0 && v.y < new_v.y )
					{
						//Debug.Log( "debug print wtf c'mon" );
						{
							if ( v.y < 0.1 )
							{
								new_v.y = Mathf.Min( new_v.y, max_velocity_y_on_collision );
							} else
							{
								new_v.y = Mathf.Min( Mathf.Min( new_v.y, 2.0f * v.y ), 2.0f );
							}
							if ( new_v.sqrMagnitude < 0.05 )
							{
								new_v *= 0;
							}
						}
					}
					//Debug.Log( "normal.y = " + normal.y + " Mathf.Cos( Mathf.Deg2Rad * 30.0f ) = " + Mathf.Cos( Mathf.Deg2Rad * 30.0f ) );
					if ( ! all_collisions_are_walkable /*&& new_v.y > 0*/ && normal != Vector3.zero && normal.y < Mathf.Cos( Mathf.Deg2Rad * 30.0f ) )
					{
						new_v.y = Mathf.Min( 0, v.y );//- new_v.y;
						Vector3 slide_down_acc = Physics.gravity - normal * Vector3.Dot( Physics.gravity, normal );
						new_v += slide_down_acc * Time.fixedDeltaTime;
						//new_v += ( -Vector3.up + normal * Vector3.Dot( -Vector3.up, normal ) ) * wall_fall_velocity;
					}
					v = new_v;
				}

				//Debug.Log( "_______normal.y = " + normal.y + " Mathf.Cos( Mathf.Deg2Rad * 30.0f ) = " + Mathf.Cos( Mathf.Deg2Rad * 30.0f ) + " forwardest_normal = " + forwardest_normal );
				if ( ! all_collisions_are_walkable
				/*&& new_v.y > 0*/
				  && normal.y < Mathf.Cos( Mathf.Deg2Rad * 30.0f )
				  && normal.y > Mathf.Cos( Mathf.Deg2Rad * 85.0f )
				  && is_colliding_with_something )
				{
					//Debug.Log( "v = " + v );
					//Debug.Log( "norma.y = " + normal.y );
					v.y = Mathf.Min( 0, v.y );//- new_v.y;
					Vector3 slide_down_acc = Physics.gravity - normal * Vector3.Dot( Physics.gravity, normal );
					v += wall_down_speed_percentage * slide_down_acc * Time.fixedDeltaTime;
					rigidbody.AddForce( normal * wall_normal_force );
					//Debug.Log( "v = " + v );
					//new_v += ( -Vector3.up + normal * Vector3.Dot( -Vector3.up, normal ) ) * wall_fall_velocity;
				}

				animator_set_run( true );
				animator_set_idle( false );
			} else
			{
				animator_set_run( false );
				animator_set_idle( true );
			}

			animator_set_velocity( v.magnitude * Mathf.Sign( Vector3.Dot( v, forward ) ) );
			if ( v3_eps( v, Vector3.zero, 0.01f ) )
			{
				animator_set_idle( true );
				animator_set_run( false );
			}

			last_v = rigidbody.velocity = v;

			//if ( jump_action_requested && ! has_jumped_in_this_action )
			//{
			//	/Debug.Log( "min_colliding_height = " + min_colliding_height );
			//}
			if ( jump_action_requested
			  && ! has_no_legs
			  && ! has_jumped_in_this_action
			  && ( !is_sprinting || stamina_value >= sprinting_jump_stamina_cost )
			  && min_colliding_height < ( has_a_destroyed_leg ? min_colliding_height_to_jump_single_leg : min_colliding_height_to_jump )
			  && ( !stepping_pitch_timer.active || stepping_pitch_timer.remaining_percentage() <= 0 ) )
			{
				//Debug.Log( "jumping" );
				has_jumped_in_this_action = true;
				float force_cripple_modifier =
				   has_both_legs
				 ? 1.0f
				 : ( has_a_destroyed_leg
				   ? crippled_jump_force_modifier_single_leg
				   : crippled_jump_force_modifier );
				//float force_ahead_cripple_modifier = crippled_jump_ahead_modifier;
				Vector3 jump_force_v = force_cripple_modifier
				                     * jump_force
				                     * ( Vector3.up + walk_direction * jump_movement_direction_multiplier );
				rigidbody.AddForce( jump_force_v );
				if ( has_a_destroyed_leg )
				{
					v += jump_force_v.normalized * jump_one_leg_velocity;
				}
				if ( is_sprinting )
				{
					set_stamina_value( stamina_value - sprinting_jump_stamina_cost );
				}
				animator_trigger_jump();
			} else
			{
				jump_action_requested = false;
			}

			last_position = transform.position;
			last_fixed_time_step_was_colliding = is_colliding_with_something;
			if ( sprinting_ended )
			{
				is_sprinting = false;
			}
		}
		private void LateUpdate()
		{
			if ( !has_to_be_updated() )
			{
				return;
			}
			//Debug.Log( "LateUpdate" + Time.frameCount );
			//Debug.Log( "    md =  "  + md.dx + ", " + md.dy );
			if ( !dead && parts_hps_current[(int)PC_Hitbox.Hitbox_Part_ID.HEAD] == 0 )
			{
				Debug.Log( "player character " + gameObject.name + " died" );
				dead = true;
				//rigidbody.freezeRotation = false;
				rigidbody.constraints = RigidbodyConstraints.None;
				respawn_timer.restart( respawn_time );
				animator_set_velocity( 0 );
			}
			animator_parameter_jump = false;
		}
		private void OnCollisionEnter( Collision collision )
		{
			#if NO
			Debug.Log( "OnCollisionEnter( Collision collision ) frame " + Time.frameCount + " collider owner = " + collision.collider.gameObject.name );
			for ( int i = 0, count = collision.contacts.Length; i != count; ++i )
			{
				ContactPoint cc = collision.contacts[i];
				Debug.Log( "P: " + cc.point + " N: " + cc.normal );
			}
			#endif
			last_collision = collision;
			colliders_currently_colliding[collision.collider] = collision;
		}
		private void OnCollisionExit( Collision collision )
		{
			#if NO
			Debug.Log( "OnCollisionExit( Collision collision ) frame " + Time.frameCount + " collider owner = " + collision.collider.gameObject.name );
			for ( int i = 0, count = collision.contacts.Length; i != count; ++i )
			{
				ContactPoint cc = collision.contacts[i];
				Debug.Log( "P: " + cc.point + " N: " + cc.normal );
			}
			#endif
			last_collision_exited = collision;
			bool b_res = colliders_currently_colliding.Remove( collision.collider );
			Debug.Assert( b_res );
		}
		private void OnTriggerEnter( Collider other )
		{
			//Debug.Log( "OnTriggerEnter( Collider other ) " + other.name );
		}
		private void OnTriggerExit( Collider other )
		{
			//Debug.Log( "OnTriggerExit( Collider other ) " + other.name );
		}

		private
		void
		respawn()
		{
			dead = false;
			rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
			stamina_value = sprint_stamina_full_value;
			for ( int i = 0; i != parts_hps_current.Length; ++i )
			{
				parts_hps_current[i] = parts_hps_max[i];
			}

			Vector3 spawn_point = Server.has_authority ? Spawn_Point.get_spawn_point() : Vector3.zero;
			transform.position      = spawn_point;
			transform.localRotation = new Quaternion();

			animator_set_idle( true );
			animator_set_run( false );
		}


		private
		void
		setup_as_non_current()
		{
			camera.enabled = false;
			audio_listener.enabled = false;
		}
		private
		void
		setup_as_current()
		{
			camera.enabled = true;
			audio_listener.enabled = true;
		}

		public
		void
		set_current()
		{
			//Debug.Assert( ! current_player_character_is_assigned );
			if ( current_player_character_is_assigned )
			{
				current_player_character.setup_as_non_current();
			}
			current_player_character = this;
			current_player_character_is_assigned = true;
			current_player_character.setup_as_current();
		}

		public
		bool
		has_to_be_updated()
		{
			bool ret = Server.has_authority || SP_Manager.is_singleplayer;
			return ret;
		}

		public void
		manage_input( List<Input_Wrapper.Action_With_Front> awfs )
		{
			foreach ( Input_Wrapper.Action_With_Front awf in awfs )
			{
				//Debug.Log( "awf = " + awf.action + " " + awf.front );
				switch( awf.action )
				{
					case Input_Wrapper.Action.MOVE_FORWARD:
					case Input_Wrapper.Action.MOVE_BACKWORD:
					case Input_Wrapper.Action.STRAFE_LEFT:
					case Input_Wrapper.Action.STRAFE_RIGHT:
						movement_directions_active[(int)awf.action - (int) Input_Wrapper.Action.MOVE_FORWARD] =
						   awf.front == Input_Wrapper.Action_Front.START;
						break;
					case Input_Wrapper.Action.JUMP:
						//Debug.Log( "Action JUMP" );
						jump_action_requested = awf.front == Input_Wrapper.Action_Front.START;
						if ( jump_action_requested )
						{
							has_jumped_in_this_action = false;
						}
						break;
					case Input_Wrapper.Action.SPRINT:
						//Debug.Log( "Action SPRINT" );
						sprint_action_requested = awf.front == Input_Wrapper.Action_Front.START;
						//Debug.Log( "sprint_action_requested = " + sprint_action_requested );
						break;
					case Input_Wrapper.Action.USE_ARM_LEFT:
						arm_left_use_requested = awf.front == Input_Wrapper.Action_Front.START;
						Debug.Log( "Action USE_ARM_LEFT arm_left_use_requested = " + arm_left_use_requested );
						break;
					case Input_Wrapper.Action.USE_ARM_RIGHT:
						arm_right_use_requested = awf.front == Input_Wrapper.Action_Front.START;
						Debug.Log( "Action USE_ARM_RIGHT arm_right_use_requested = " + arm_right_use_requested );
						break;
					default:
						break;
				}
			}
		}
		public
		void
		manage_mouse_delta( Mouse_Wrapper.Mouse_Delta md )
		{
			this.md = md;
			//Debug.Log( "[client] md = " + md.dx + ", " + md.dy + " this.md = " + this.md.dx + ", " + this.md.dy );
		}

		private
		float
		compute_movement_velocity( float v, float delta_movement, bool in_control )
		{
			if ( same_sign( v, delta_movement ) || ( !in_control && delta_movement == 0 ) )
			{
				v  = ( Mathf.Abs( v ) > Mathf.Abs( delta_movement ) ) ? v : delta_movement;
			} else
			{
				v = ( v + delta_movement ) * 0.5f;
			}
			return v;
		}

		private
		void
		compute_sprint_acceleration()
		{
			sprint_acceleration_computed = ( sprint_speed_max - sprint_start_speed ) / sprint_secs_to_full_speed;
		}
		private
		void
		compute_stamina_consumption_speed()
		{
			sprint_stamina_spent_per_speed_per_secs_computed =
			   sprint_stamina_full_value
			 / ( 0.5f * sprint_acceleration_computed * sprint_max_duration_secs * sprint_max_duration_secs
			   + sprint_start_speed * sprint_max_duration_secs);
		}
		private
		void
		set_stamina_value( float value )
		{
			stamina_value = Mathf.Clamp( value, 0, sprint_stamina_full_value );
		}
		private
		void
		hitboxes_set_enabled( bool active )
		{
			for ( int i = 0; i != hitboxes.Length; ++i )
			{
				hitboxes[i].own_collider.enabled = active;
			}
		}

		public
		void
		animator_set_idle( bool value )
		{
			animator.SetBool( animation_hash_idle, value );
			animator_parameter_idle = value;
		}
		public
		void
		animator_set_run( bool value )
		{
			animator.SetBool( animation_hash_run, value );
			animator_parameter_run = value;
		}
		public
		void
		animator_trigger_jump()
		{
			animator.SetTrigger( animation_hash_jump );
			animator_parameter_jump = true;
		}
		public
		void
		animator_set_velocity( float value )
		{
			animator.SetFloat( animation_hash_velocity, value );
			animator_parameter_velocity = value;
		}
	}
}