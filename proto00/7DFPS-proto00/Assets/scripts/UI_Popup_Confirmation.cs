﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace fps
{
	public class
	UI_Popup_Confirmation : MonoBehaviour
	{
		public Button confirm_button;
		public Button cancel_button;
		public Text   message_text;

		public UnityEvent on_confirm = new UnityEvent();
		public UnityEvent on_cancel  = new UnityEvent();

		public string _message;
		public string message
		{
			get { return _message; }
			set { message_text.text = _message = value; }
		}

		void
		Start()
		{
			Reset();
		}
		private void Reset()
		{
			confirm_button.onClick.AddListener( () => { on_confirm.Invoke(); gameObject.SetActive( false ); } );
			cancel_button .onClick.AddListener( () =>
			{
				on_cancel .Invoke();
				gameObject.SetActive( false );
			} );
		}

		//void
		//Update()
		//{
		//}
	}
}