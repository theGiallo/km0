﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public
	class
	Input_Wrapper : MonoBehaviour
	{
		public readonly Client.Message_Protocol.Message_ID message_ID = Client.Message_Protocol.Message_ID.INPUT_ACTION_FRONT;

		public enum
		Action
		{
			// NOTE(theGiallo): put here your high level input actions. E.g. "INTERACT", "JUMP", "THROW"
			MOVE_FORWARD,
			MOVE_BACKWORD,
			STRAFE_LEFT,
			STRAFE_RIGHT,
			JUMP,
			SPRINT,
			USE_ARM_LEFT,
			USE_ARM_RIGHT,
			//---
			_COUNT,
			_NONE
		}
		public enum
		Action_Front
		{
			START,
			STOP,
			//---
			_COUNT
		}
		public struct
		Action_With_Front
		{
			public Action       action;
			public Action_Front front;
			public
			Action_With_Front( Action action, Action_Front front )
			{
				this.action = action;
				this.front  = front;
			}
		}
		public class
		Input_Source
		{
			public enum Type
			{
				BUTTON,
				MOUSE_WHEEL,
				//---
				_COUNT,
				_UNASSIGNED,
			}
			public enum
			Mouse_Wheel_Direction
			{
				UP,
				DOWN,
				RIGHT,
				LEFT,
				//---
				_COUNT
			}

			public Type                  type;
			// NOTE(theGiallo): this is < 512
			public KeyCode               key_code;
			public Mouse_Wheel_Direction mouse_wheel_direction;
			//int     mouse_button;

			public
			Input_Source()
			{
				type = Type._UNASSIGNED;
			}
		}
		private Dictionary<Action, Input_Source> input_source_from_action = new Dictionary<Action, Input_Source>();
		private Dictionary<Input_Source, Action> action_from_input_source = new Dictionary<Input_Source, Action>();
		private bool[] mouse_wheel_action_is_on = new bool[4];
		private LiteNetLib.Utils.NetDataWriter this_net_data_writer = new LiteNetLib.Utils.NetDataWriter();
		static private LiteNetLib.Utils.NetDataReader this_net_data_reader = new LiteNetLib.Utils.NetDataReader();

		public bool add_action( Action action )
		{
			bool ret = false;
			Input_Source input_source;
			bool b_res = input_source_from_action.TryGetValue( action, out input_source );
			if ( !b_res )
			{
				input_source = new Input_Source();
				input_source_from_action.Add( action, input_source );
				action_from_input_source.Add( input_source, action );
				ret = true;
			}
			return ret;
		}
		public
		bool
		remap_action( Action action, Input_Source input_source, out Input_Source old_input_source, out Action old_action )
		{
			bool ret = false;

			old_input_source = null;
			old_action       = Action._NONE;

			bool b_res = input_source_from_action.TryGetValue( action, out old_input_source );
			if ( b_res )
			{
				b_res = action_from_input_source.TryGetValue( old_input_source, out old_action );
				if ( ! b_res )
				{
					Debug.Log( "input was unused before assigning it to " + action );
				}
				input_source_from_action[action]       = input_source;
				action_from_input_source[input_source] = action;
				ret = true;
			}

			return ret;
		}
		public
		bool
		get_action_from_input_source( Input_Source input_source, out Action action )
		{
			bool ret = false;

			action = Action._NONE;
			ret = action_from_input_source.TryGetValue( input_source, out action );

			return ret;
		}
		public
		bool
		get_input_source_from_action( Action action, out Input_Source input_source )
		{
			bool ret = false;

			ret = input_source_from_action.TryGetValue( action, out input_source );
			if ( !ret )
			{
				input_source = null;
			}

			return ret;
		}

		public
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_net_data_writer.Reset();

			foreach ( KeyValuePair<Action,Input_Source> a_is in input_source_from_action )
			{
				Action       action       = a_is.Key;
				Input_Source input_source = a_is.Value;
				switch ( input_source.type )
				{
					case Input_Source.Type.BUTTON:
						if ( Input.GetKeyDown( input_source.key_code ) )
						{
							this_net_data_writer.PutInput_ActionFront( Action_Front.START, action );
						} else
						if ( Input.GetKeyUp( input_source.key_code ) )
						{
							this_net_data_writer.PutInput_ActionFront( Action_Front.STOP, action );
						}
						break;
					case Input_Source.Type.MOUSE_WHEEL:
						if ( (    input_source.mouse_wheel_direction == Input_Source.Mouse_Wheel_Direction.UP
						       && Input.mouseScrollDelta.y > 0 )
						  || (    input_source.mouse_wheel_direction == Input_Source.Mouse_Wheel_Direction.DOWN
						       && Input.mouseScrollDelta.y < 0 )
						  || (    input_source.mouse_wheel_direction == Input_Source.Mouse_Wheel_Direction.RIGHT
						       && Input.mouseScrollDelta.x > 0 )
						  || (    input_source.mouse_wheel_direction == Input_Source.Mouse_Wheel_Direction.LEFT
						       && Input.mouseScrollDelta.x < 0 )
						   )
						{
							this_net_data_writer.PutInput_ActionFront( Action_Front.START, action );
							mouse_wheel_action_is_on[(int)input_source.mouse_wheel_direction] = true;
						} else
						if ( mouse_wheel_action_is_on[(int)input_source.mouse_wheel_direction] )
						{
							this_net_data_writer.PutInput_ActionFront( Action_Front.STOP, action );
							mouse_wheel_action_is_on[(int)input_source.mouse_wheel_direction] = false;
						}
						break;
					default:
						break;
				}
			}

			if ( this_net_data_writer.Length > 0 )
			{
				net_data_writer.PutClientMessage_ID( message_ID );
				net_data_writer.PutBytesWithLength( this_net_data_writer.Data, 0, this_net_data_writer.Length );
			}
		}
		public
		static
		List<Action_With_Front>
		decode( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			List<Action_With_Front> ret;

			byte[] bytes = net_data_reader.GetBytesWithLength();
			ret = new List<Action_With_Front>( bytes.Length / 2 );
			//Debug.Log( "Input_Wrapper got " + bytes.Length + "Bytes" );
			this_net_data_reader.SetSource( bytes );
			while ( this_net_data_reader.AvailableBytes > 0 )
			{
				Action_Front front;
				Action action = this_net_data_reader.GetInput_ActionFront( out front );
				ret.Add( new Action_With_Front( action, front ) );
			}

			return ret;
		}
	}
	public static class
	Input_NetDataExtensions
	{
		public static void
		PutInput_Source( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                 Input_Wrapper.Input_Source input_source )
		{
			// 1 bit type

			// if button
			// 9 bit key_code

			// if mouse wheel
			// 2 bit wheel direction

			ushort u = 0;
			u = (ushort) ( u | ( ((ushort)input_source.type) << 15 ) );
			if ( input_source.type == Input_Wrapper.Input_Source.Type.BUTTON )
			{
				u = (ushort)( u | ((ushort)input_source.key_code) << 6 );
			} else
			//if ( input_source.type == Input_Wrapper.Input_Source.Type.MOUSE_WHEEL )
			{
				u = (ushort)( u | ((ushort)input_source.mouse_wheel_direction) << 11 );
			}
			net_data_writer.Put( u );
		}
		public static Input_Wrapper.Input_Source
		GetInput_Source( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Input_Wrapper.Input_Source ret = new Input_Wrapper.Input_Source();
			ushort u = net_data_reader.GetUShort();
			ret.type = (Input_Wrapper.Input_Source.Type)( ( u >> 15 ) & 0b1 );
			if ( ret.type == Input_Wrapper.Input_Source.Type.BUTTON )
			{
				ret.key_code = (KeyCode)( ( u >> 6 ) & 511 );
			} else
			//if ( ret.type == Input_Wrapper.Input_Source.Type.MOUSE_WHEEL )
			{
				ret.mouse_wheel_direction = (Input_Wrapper.Input_Source.Mouse_Wheel_Direction)( ( u >> 11 ) & 31 );
			}
			return ret;
		}


		public static void
		PutInput_ActionFront( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                      Input_Wrapper.Action_Front action_front,
		                      Input_Wrapper.Action action )
		{
			ushort u = (ushort) ( ( (ushort)action_front << 15 ) | ( (ushort)action & 0x7FFF ) );
			net_data_writer.Put( u );
		}
		public static Input_Wrapper.Action
		GetInput_ActionFront( this LiteNetLib.Utils.NetDataReader net_data_reader,
		                      out Input_Wrapper.Action_Front action_front )
		{
			Input_Wrapper.Action ret = Input_Wrapper.Action._NONE;
			ushort u = net_data_reader.GetUShort();
			ret = (Input_Wrapper.Action)( u & 0x7FFF );
			action_front = (Input_Wrapper.Action_Front)( ( u >> 15 ) & 1 );
			return ret;
		}
	}
}