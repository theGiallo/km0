﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	// NOTE(theGiallo): put this in your syncer
	// [RequireComponent( typeof( UUID ) )]
	public class
	World_Syncer// : MonoBehaviour
	{
		public enum
		Sync_ID
		{
			// NOTE(theGiallo): put here your syncer id
			TRANSFORM,
			PC_DATA,
			//---
			_COUNT,
		}
		public static readonly Server.Message_Protocol.Message_ID message_ID = Server.Message_Protocol.Message_ID.SYNC_WORLD;

		// NOTE(theGiallo): implement this in your syncer
		//public static List<DataSyncer> all_data_syncers_of_this_type;
		//public static readonly Sync_ID sync_ID;

		private static LiteNetLib.Utils.NetDataWriter this_data_writer = new LiteNetLib.Utils.NetDataWriter();
		private static LiteNetLib.Utils.NetDataReader this_reader      = new LiteNetLib.Utils.NetDataReader();
		private static LiteNetLib.Utils.NetDataReader specific_reader  = new LiteNetLib.Utils.NetDataReader();

		public
		static
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_data_writer.Reset();

			// NOTE(theGiallo): implement this in your syncer
			#if NO
			this_data_writer.Reset();

			foreach ( DataSyncer ds in all_data_syncers_of_this_type )
			{
				// ds.extract // NOTE(theGiallo): you can implement it directly here, so there's one less call level
			}

			if ( this_data_writer.Length > 0 )
			{
				// NOTE(theGiallo): header
				this_data_writer.Put( (byte)sync_ID );

				net_data_writer.PutBytesWithLength( this_data_writer.Data );
			}
			#endif

			// NOTE(theGiallo): here put:
			// Your_Data_Syncer      .gather( this_data_writer );
			// Your_Other_Data_Syncer.gather( this_data_writer );
			// ...
			Syncer_PC_Data  .gather( this_data_writer );
			Syncer_Transform.gather( this_data_writer );

			if ( this_data_writer.Length > 0 )
			{
				//Debug.Log( "World_Syncer gathered " + this_data_writer.Length + "Bytes" );
				// NOTE(theGiallo): header
				net_data_writer.PutServerMessage_ID( message_ID );

				net_data_writer.PutBytesWithLength( this_data_writer.Data, 0, this_data_writer.Length );
			}
		}
		// NOTE(theGiallo): this will be called by the client if a Message_ID of the SYNC_WORLD type was read immediately before
		public
		static
		void
		assign( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			byte[] bytes = net_data_reader.GetBytesWithLength();
			//Debug.Log( "World_Syncer got " + bytes.Length + "Bytes" );
			this_reader.SetSource( bytes );
			while ( this_reader.AvailableBytes > 0 )
			{
				Sync_ID sync_id        = (Sync_ID)this_reader.GetByte();
				byte[]  specific_bytes = this_reader.GetBytesWithLength();
				specific_reader.SetSource( specific_bytes );
				switch ( sync_id )
				{
					/*
					 * NOTE(theGiallo): here put:
					case Sync_ID.YOUR_DATA_SYNC:
						Your_Data_Sync.assign( specific_reader );
						break;
					*/
					case Sync_ID.TRANSFORM:
						Syncer_Transform.assign( specific_reader );
						break;
					case Sync_ID.PC_DATA:
						Syncer_PC_Data  .assign( specific_reader );
						break;
					default:
						Debug.Log( "unknown sync_id " + sync_id + " = " + (int)sync_id );
						break;
				}
			}
		}
	}
}