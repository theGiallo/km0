﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace fps
{
	[ExecuteInEditMode]
	public class
	Prefabs_Manager : MonoBehaviour
	{
		public static Prefabs_Manager instance;
		public string prefabs_path = "game_prefabs";

		private
		void
		Awake()
		{
			Debug.Assert( instance == null );
			instance = this;

			populate_prefabs_db();
		}

		private Dictionary<Prefab_Hash,GameObject> prefab_from_hash = new Dictionary<Prefab_Hash, GameObject>();
		private
		void
		populate_prefabs_db()
		{
			GameObject[] objects = Resources.LoadAll<GameObject>( prefabs_path );
			foreach ( GameObject go in objects )
			{
				Prefab_Hash ph = new Prefab_Hash( go );
				prefab_from_hash[ph] = go;
			}
		}
		public
		GameObject
		instantiate_prefab( Prefab_Hash ph, LiteNetLib.Utils.NetDataReader net_data_reader = null )
		{
			GameObject ret = null;
			GameObject p;
			bool b_res = prefab_from_hash.TryGetValue( ph, out p );
			if ( b_res )
			{
				ret = Instantiate( p );
				UUIDs_Prefab_Syncer ups = ret.GetComponent<UUIDs_Prefab_Syncer>();
				if ( ups != null )
				{
					if ( Server.has_authority )
					{
						Server.instance.on_prefab_spawn( ph, ups );
					} else
					{
						ups.assign_uuids( net_data_reader );
					}
				}
			}
			return ret;
		}

		#if UNITY_EDITOR
		public GameObject editor_get_prefab_object( Prefab_Hash ph )
		{
			GameObject ret = null;
			prefab_from_hash.TryGetValue( ph, out ret );
			return ret;
		}
		private void OnValidate()
		{
			prefab_from_hash.Clear();
			populate_prefabs_db();
		}
		#endif

		//void
		//Start()
		//{
		//}

		//void
		//Update()
		//{
		//}
	}

	[System.Serializable]
	public struct
	Prefab_Hash
	{
		[SerializeField]
		int hash;
		public
		Prefab_Hash( GameObject go )
		{
			hash = go == null ? 0 : Utilities.s32_FNV1a( go.name );
		}

		public Prefab_Hash( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			hash = net_data_reader.GetInt();
		}
		public void write( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			net_data_writer.Put( hash );
		}
		#if UNITY_EDITOR
		public
		static
		Prefab_Hash from_int( int hash )
		{
			Prefab_Hash ret = new Prefab_Hash();
			ret.hash = hash;
			return ret;
		}
		public int editor_get_int_hash() { return hash; }
		#endif

		public
		override
		string
		ToString()
		{
			string ret = hash.ToString().PadLeft( 4 );
			return ret;
		}
		public
		override
		int
		GetHashCode()
		{
			return hash.GetHashCode();
		}
		public
		override
		bool
		Equals( object o )
		{
			bool ret = false;

			if ( ( o == null ) || !this.GetType().Equals( o.GetType() ) )
			{
				return ret;
			}
			ret = hash == ((Prefab_Hash)o).hash;
			return ret;
		}
	}
	public static class Prefab_Hash_NetDataExtensions
	{
		public static Prefab_Hash GetPrefab_Hash( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Prefab_Hash ret = new Prefab_Hash( net_data_reader );
			return ret;
		}
		public static void PutPrefab_Hash(  this LiteNetLib.Utils.NetDataWriter net_data_writer, Prefab_Hash p )
		{
			p.write( net_data_writer );
		}
	}
	#if UNITY_EDITOR
	[CustomPropertyDrawer( typeof( Prefab_Hash ) )]
	public class Prefab_Hash_Drawer : PropertyDrawer
	{
		// Draw the property inside the given rect
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty( position, label, property );

			Prefab_Hash old_ph = Prefab_Hash.from_int( property.FindPropertyRelative( "hash" ).intValue );
			GameObject old_go = Prefabs_Manager.instance.editor_get_prefab_object( old_ph );
			GameObject go =
			(GameObject)EditorGUI.ObjectField( position, label,
			                                   old_go,
			                                   typeof ( GameObject ),
			                                   false );

			Prefab_Hash ph = go != null ? new Prefab_Hash( go ) : new Prefab_Hash();
			property.FindPropertyRelative( "hash" ).intValue = ph.editor_get_int_hash();

			EditorGUI.EndProperty();
		}
	}
	#endif
}