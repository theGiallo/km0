﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	// NOTE(theGiallo): put this in your syncer
	[RequireComponent( typeof( UUID_Assigner ) )]
	[RequireComponent( typeof( Player_Character ) )]
	public class
	Syncer_PC_Data : MonoBehaviour
	{
		// NOTE(theGiallo): implement this in your syncer
		public static List<Syncer_PC_Data> all_data_syncers_of_this_type = new List<Syncer_PC_Data>();
		public static readonly World_Syncer.Sync_ID sync_ID = World_Syncer.Sync_ID.PC_DATA;

		private static LiteNetLib.Utils.NetDataWriter this_data_writer = new LiteNetLib.Utils.NetDataWriter();
		//private static LiteNetLib.Utils.NetDataReader this_reader      = new LiteNetLib.Utils.NetDataReader();

		private Player_Character pc;

		public void Awake()
		{
			all_data_syncers_of_this_type.Add( this );
			pc = GetComponent<Player_Character>();
		}
		public void OnDestroy()
		{
			all_data_syncers_of_this_type.Remove( this );
		}

		public
		static
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_data_writer.Reset();

			foreach ( Syncer_PC_Data ds in all_data_syncers_of_this_type )
			{
				// ds.extract // NOTE(theGiallo): you can implement it directly here, so there's one less call level
				this_data_writer.PutUUID( ds.GetComponent<UUID_Assigner>().get_uuid() );
				this_data_writer.PutPC_Data( new PC_Data( ds.pc ) );
			}

			if ( this_data_writer.Length > 0 )
			{
				//Debug.Log( "Transform_Syncer gathered " + this_data_writer.Length + "Bytes" );
				// NOTE(theGiallo): header
				net_data_writer.Put( (byte)sync_ID );

				net_data_writer.PutBytesWithLength( this_data_writer.Data, 0, this_data_writer.Length );
			}
		}
		// NOTE(theGiallo): this will be called by the client if a Message_ID of the SYNC_WORLD type was read immediately before
		public
		static
		void
		assign( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			//Debug.Log( "Syncer_PC_Data got " + net_data_reader.AvailableBytes + "Bytes" );
			while ( net_data_reader.AvailableBytes > 0 )
			{
				UUID    uuid    = net_data_reader.GetUUID();
				PC_Data pc_data = net_data_reader.GetPC_Data();
				GameObject go;
				bool b_res = UUID_Assigner.try_get_go( uuid, out go );
				if ( !b_res )
				{
					Debug.LogError( "got pc_data update of unknown uuid = " + uuid + " pc data = " + pc_data );
				} else
				{
					Player_Character pc = go.GetComponent<Player_Character>();
					pc_data.assign_to( pc );
				}
			}
		}
	}
	public struct
	PC_Data
	{
		public ushort stamina_value;
		public ushort sprint_stamina_full_value;
		#if NO
		public float  animator_parameter_velocity;
		public bool   animator_parameter_run;
		public bool   animator_parameter_idle;
		public bool   animator_parameter_jump;
		#endif

		public
		PC_Data( Player_Character pc )
		{
			sprint_stamina_full_value = pc.sprint_stamina_full_value;
			stamina_value             = (ushort) ( ( pc.stamina_value * ushort.MaxValue ) / pc.sprint_stamina_full_value );
			#if NO
			animator_parameter_velocity = pc.animator_parameter_velocity;
			animator_parameter_run      = pc.animator_parameter_run;
			animator_parameter_idle     = pc.animator_parameter_idle;
			animator_parameter_jump     = pc.animator_parameter_run;
			#endif
		}

		public
		void
		assign_to( Player_Character pc )
		{
			pc.sprint_stamina_full_value = sprint_stamina_full_value;
			pc.stamina_value             = ( (float)stamina_value * (float)sprint_stamina_full_value ) / (float)ushort.MaxValue;
			#if NO
			pc.animator_set_velocity( animator_parameter_velocity );
			pc.animator_set_run     ( animator_parameter_run      );
			pc.animator_set_idle    ( animator_parameter_idle     );
			if ( animator_parameter_run )
			{
				pc.animator_trigger_jump();
			}
			#endif
		}

		override
		public
		string
		ToString()
		{
			string ret = "sprint_stamina_full_value: " + sprint_stamina_full_value
			           + " stamina_value: " + stamina_value
			#if NO
			           + " animator_parameter_velocity: " + animator_parameter_velocity
			           + " animator_parameter_run: " + animator_parameter_run
			           + " animator_parameter_idle: " + animator_parameter_idle
			           + " animator_parameter_jump: " + animator_parameter_jump
			#endif
			;
			return ret;
		}
	}
	public static class
	PC_NetDataRW_Extensions
	{
		private static readonly byte idle_mask = 1 << 0;
		private static readonly byte  run_mask = 1 << 1;
		private static readonly byte jump_mask = 1 << 2;
		public static void
		PutPC_Data( this LiteNetLib.Utils.NetDataWriter net_data_writer, PC_Data pc_data )
		{
			net_data_writer.Put( pc_data.sprint_stamina_full_value );
			net_data_writer.Put( pc_data.stamina_value );
			#if NO
			net_data_writer.Put( pc_data.animator_parameter_velocity );
			byte mask = 0;
			mask = (byte)( mask | ( pc_data.animator_parameter_idle ? idle_mask : 0 ) );
			mask = (byte)( mask | ( pc_data.animator_parameter_idle ?  run_mask : 0 ) );
			mask = (byte)( mask | ( pc_data.animator_parameter_idle ? jump_mask : 0 ) );
			net_data_writer.Put( mask );
			#endif
		}
		public static PC_Data
		GetPC_Data( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			PC_Data ret = new PC_Data();

			ret.sprint_stamina_full_value = net_data_reader.GetUShort();
			ret.stamina_value             = net_data_reader.GetUShort();
			#if NO
			ret.animator_parameter_velocity = net_data_reader.GetFloat();
			byte mask = net_data_reader.GetByte();
			ret.animator_parameter_idle = ( mask & idle_mask ) != 0;
			ret.animator_parameter_run  = ( mask &  run_mask ) != 0;
			ret.animator_parameter_jump = ( mask & jump_mask ) != 0;
			#endif

			return ret;
		}
	}
}