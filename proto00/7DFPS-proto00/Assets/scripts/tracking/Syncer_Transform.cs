﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	// NOTE(theGiallo): put this in your syncer
	[RequireComponent( typeof( UUID_Assigner ) )]
	public class
	Syncer_Transform : MonoBehaviour
	{
		// NOTE(theGiallo): implement this in your syncer
		public static List<Syncer_Transform> all_data_syncers_of_this_type = new List<Syncer_Transform>();
		public static readonly World_Syncer.Sync_ID sync_ID = World_Syncer.Sync_ID.TRANSFORM;

		private static LiteNetLib.Utils.NetDataWriter this_data_writer = new LiteNetLib.Utils.NetDataWriter();
		//private static LiteNetLib.Utils.NetDataReader this_reader      = new LiteNetLib.Utils.NetDataReader();

		[System.Serializable]
		public
		struct
		Transform_Sync_Mask
		{
			public bool sync_position_x;
			public bool sync_position_y;
			public bool sync_position_z;
			public bool sync_rotation_x;
			public bool sync_rotation_y;
			public bool sync_rotation_z;
			public Scaling_Mode sync_scale_mode;
			public enum Scaling_Mode
			{
				NONE,
				UNIFORM,
				FULL,
			}
			public
			byte
			get_byte()
			{
				byte ret = 0;
				if ( sync_position_x ) { ret = (byte)( ret | ( 1 << 7 ) ); }
				if ( sync_position_y ) { ret = (byte)( ret | ( 1 << 6 ) ); }
				if ( sync_position_z ) { ret = (byte)( ret | ( 1 << 5 ) ); }
				if ( sync_rotation_x ) { ret = (byte)( ret | ( 1 << 4 ) ); }
				if ( sync_rotation_y ) { ret = (byte)( ret | ( 1 << 3 ) ); }
				if ( sync_rotation_z ) { ret = (byte)( ret | ( 1 << 2 ) ); }
				                         ret = (byte)( ret | ( (byte)sync_scale_mode ) );
				return ret;
			}
			public
			Transform_Sync_Mask( byte b )
			{
				sync_position_x = 0 != (byte)( b & ( 1 << 7 ) );
				sync_position_y = 0 != (byte)( b & ( 1 << 6 ) );
				sync_position_z = 0 != (byte)( b & ( 1 << 5 ) );
				sync_rotation_x = 0 != (byte)( b & ( 1 << 4 ) );
				sync_rotation_y = 0 != (byte)( b & ( 1 << 3 ) );
				sync_rotation_z = 0 != (byte)( b & ( 1 << 2 ) );
				sync_scale_mode = (Scaling_Mode)( b & 3 );
			}
		}

		public Transform_Sync_Mask sync_mask = new Transform_Sync_Mask( 0xff );

		public void Awake()
		{
			all_data_syncers_of_this_type.Add( this );
		}
		public void OnDestroy()
		{
			all_data_syncers_of_this_type.Remove( this );
		}

		public
		static
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_data_writer.Reset();

			foreach ( Syncer_Transform ds in all_data_syncers_of_this_type )
			{
				// ds.extract // NOTE(theGiallo): you can implement it directly here, so there's one less call level
				this_data_writer.PutUUID( ds.GetComponent<UUID_Assigner>().get_uuid() );
				this_data_writer.PutTransform( ds.gameObject.transform, ds.gameObject.GetComponent<Syncer_Transform>().sync_mask );
			}

			if ( this_data_writer.Length > 0 )
			{
				//Debug.Log( "Transform_Syncer gathered " + this_data_writer.Length + "Bytes" );
				// NOTE(theGiallo): header
				net_data_writer.Put( (byte)sync_ID );

				net_data_writer.PutBytesWithLength( this_data_writer.Data, 0, this_data_writer.Length );
			}
		}
		// NOTE(theGiallo): this will be called by the client if a Message_ID of the SYNC_WORLD type was read immediately before
		public
		static
		void
		assign( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			//Debug.Log( "Transform_Syncer got " + net_data_reader.AvailableBytes + "Bytes" );
			while ( net_data_reader.AvailableBytes > 0 )
			{
				UUID            uuid            = net_data_reader.GetUUID();
				Local_Transform local_transform = net_data_reader.GetTransform();
				GameObject go;
				bool b_res = UUID_Assigner.try_get_go( uuid, out go );
				if ( !b_res )
				{
					Debug.LogError( "got transform update of unknown uuid = " + uuid + " local transform = " + local_transform );
				} else
				{
					//Debug.Log( "got transform update of uuid = " + uuid + " local transform = " + local_transform );
					local_transform.assign_to( go.transform );
				}
			}
		}
	}
	public class
	Local_Transform
	{
		public Syncer_Transform.Transform_Sync_Mask sync_mask;
		public Vector3 localPosition;
		public Vector3 localEulerAngles;
		public Vector3 localScale;

		public
		void
		assign_to( Transform transform )
		{
			if ( sync_mask.sync_position_x
			  && sync_mask.sync_position_y
			  && sync_mask.sync_position_z )
			{
				transform.localPosition    = localPosition;
			} else
			if ( sync_mask.sync_position_x
			  || sync_mask.sync_position_y
			  || sync_mask.sync_position_z )
			{
				Vector3 lp = transform.localPosition;
				if ( sync_mask.sync_position_x )
				{
					lp.x = localPosition.x;
				}
				if ( sync_mask.sync_position_y )
				{
					lp.y = localPosition.y;
				}
				if ( sync_mask.sync_position_z )
				{
					lp.z = localPosition.z;
				}
				transform.localPosition = lp;
			}

			if ( sync_mask.sync_rotation_x
			  && sync_mask.sync_rotation_y
			  && sync_mask.sync_rotation_z )
			{
				transform.localEulerAngles = localEulerAngles;
			} else
			if ( sync_mask.sync_rotation_x
			  || sync_mask.sync_rotation_y
			  || sync_mask.sync_rotation_z )
			{
				Vector3 lea = transform.localEulerAngles;
				if ( sync_mask.sync_rotation_x )
				{
					lea.x = localEulerAngles.x;
				}
				if ( sync_mask.sync_rotation_y )
				{
					lea.y = localEulerAngles.y;
				}
				if ( sync_mask.sync_rotation_z )
				{
					lea.z = localEulerAngles.z;
				}
				transform.localEulerAngles = lea;
			}
			switch ( sync_mask.sync_scale_mode )
			{
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.FULL:
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.UNIFORM:
					transform.localScale = localScale;
					break;
				default: break;
			}
		}

		override
		public
		string
		ToString()
		{
			string ret = "localPosition: " + localPosition + " localEulerangles: " + localEulerAngles + " localScale: " + localScale;
			return ret;
		}
	}
	public static class
	Transform_NetDataRW_Extensions
	{
		public static void
		PutTransform( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                   Transform transform,
		                   Syncer_Transform.Transform_Sync_Mask sync_mask )
		{
			byte mask = sync_mask.get_byte();
			net_data_writer.Put( mask );

			if ( sync_mask.sync_position_x
			  && sync_mask.sync_position_y
			  && sync_mask.sync_position_z )
			{
				net_data_writer.PutVector3( transform.localPosition );
			} else
			if ( sync_mask.sync_position_x
			  || sync_mask.sync_position_y
			  || sync_mask.sync_position_z )
			{
				Vector3 lp = transform.localPosition;
				if ( sync_mask.sync_position_x )
				{
					net_data_writer.Put( (float) lp.x );
				}
				if ( sync_mask.sync_position_y )
				{
					net_data_writer.Put( (float) lp.y );
				}
				if ( sync_mask.sync_position_z )
				{
					net_data_writer.Put( (float) lp.z );
				}
			}

			if ( sync_mask.sync_rotation_x
			  && sync_mask.sync_rotation_y
			  && sync_mask.sync_rotation_z )
			{
				#if NET_ANGLES_ARE_16BITS
				Vector3 lea = transform.localEulerAngles;
				net_data_writer.PutAngleDeg( (float) lea.x );
				net_data_writer.PutAngleDeg( (float) lea.y );
				net_data_writer.PutAngleDeg( (float) lea.z );
				#else
				net_data_writer.PutVector3( transform.localEulerAngles );
				#endif
			} else
			if ( sync_mask.sync_rotation_x
			  || sync_mask.sync_rotation_y
			  || sync_mask.sync_rotation_z )
			{
				Vector3 lea = transform.localEulerAngles;
				if ( sync_mask.sync_rotation_x )
				{
					net_data_writer.PutAngleDeg( (float) lea.x );
				}
				if ( sync_mask.sync_rotation_y )
				{
					net_data_writer.PutAngleDeg( (float) lea.y );
				}
				if ( sync_mask.sync_rotation_z )
				{
					net_data_writer.PutAngleDeg( (float) lea.z );
				}
			}
			switch ( sync_mask.sync_scale_mode )
			{
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.FULL:
					net_data_writer.PutVector3( transform.localScale );
					break;
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.UNIFORM:
					float s = transform.localScale.x;
					net_data_writer.Put( (float) s );
					break;
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.NONE:
				default: break;
			}
		}
		public static Local_Transform
		GetTransform( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Local_Transform ret = new Local_Transform();
			byte b = net_data_reader.GetByte();
			Syncer_Transform.Transform_Sync_Mask sync_mask = new Syncer_Transform.Transform_Sync_Mask( b );
			ret.sync_mask = sync_mask;

			if ( sync_mask.sync_position_x
			  && sync_mask.sync_position_y
			  && sync_mask.sync_position_z )
			{
				ret.localPosition = net_data_reader.GetVector3();
			} else
			if ( sync_mask.sync_position_x
			  || sync_mask.sync_position_y
			  || sync_mask.sync_position_z )
			{
				if ( sync_mask.sync_position_x )
				{
					ret.localPosition.x = net_data_reader.GetFloat();
				}
				if ( sync_mask.sync_position_y )
				{
					ret.localPosition.y = net_data_reader.GetFloat();
				}
				if ( sync_mask.sync_position_z )
				{
					ret.localPosition.z = net_data_reader.GetFloat();
				}
			}

			if ( sync_mask.sync_rotation_x
			  && sync_mask.sync_rotation_y
			  && sync_mask.sync_rotation_z )
			{
				#if NET_ANGLES_ARE_16BITS
				ret.localEulerAngles.x = net_data_reader.GetAngleDeg();
				ret.localEulerAngles.y = net_data_reader.GetAngleDeg();
				ret.localEulerAngles.z = net_data_reader.GetAngleDeg();
				#else
				ret.localEulerAngles = net_data_reader.GetVector3();
				#endif
			} else
			if ( sync_mask.sync_rotation_x
			  || sync_mask.sync_rotation_y
			  || sync_mask.sync_rotation_z )
			{
				if ( sync_mask.sync_rotation_x )
				{
					ret.localEulerAngles.x = net_data_reader.GetAngleDeg();
				}
				if ( sync_mask.sync_rotation_y )
				{
					ret.localEulerAngles.y = net_data_reader.GetAngleDeg();
				}
				if ( sync_mask.sync_rotation_z )
				{
					ret.localEulerAngles.z = net_data_reader.GetAngleDeg();
				}
			}
			switch ( sync_mask.sync_scale_mode )
			{
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.FULL:
					ret.localScale = net_data_reader.GetVector3();
					break;
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.UNIFORM:
					ret.localScale.x = ret.localScale.y = ret.localScale.z =
					   net_data_reader.GetFloat();
					break;
				case Syncer_Transform.Transform_Sync_Mask.Scaling_Mode.NONE:
				default: break;
			}

			return ret;
		}
		public static void
		PutAngleDeg( this LiteNetLib.Utils.NetDataWriter net_data_writer, float angle_deg )
		{
			#if NET_ANGLES_ARE_16BITS
			float angle_deg_0_360 = Utilities.deg_to_0_360( angle_deg );
			UInt16 v = (UInt16)( ( angle_deg_0_360 / 360.0f ) * UInt16.MaxValue );
			net_data_writer.Put( v );
			#else
			net_data_writer.Put( angle_deg );
			#endif
		}
		public static float
		GetAngleDeg( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			float ret;
			#if NET_ANGLES_ARE_16BITS
			UInt16 v = net_data_reader.GetUShort();
			ret = 360.0f * (float)v / (float)UInt16.MaxValue;
			#else
			ret = net_data_reader.GetFloat();
			#endif
			return ret;
		}
		public static void
		PutVector3( this LiteNetLib.Utils.NetDataWriter net_data_writer, Vector3 vector )
		{
			net_data_writer.Put( vector.x );
			net_data_writer.Put( vector.y );
			net_data_writer.Put( vector.z );
		}
		public static Vector3
		GetVector3( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Vector3 ret;
			ret.x = net_data_reader.GetFloat();
			ret.y = net_data_reader.GetFloat();
			ret.z = net_data_reader.GetFloat();
			return ret;
		}
	}
}