﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent(typeof(UUID_Assigner))]
	public class
	UUIDs_Prefab_Syncer : MonoBehaviour
	{
		// NOTE(theGiallo): manually fill these
		// Only put direct sub-prefabs in prefab_children_with_syncer.
		public List<UUID_Assigner>       tracked_children            = new List<UUID_Assigner>();
		public List<UUIDs_Prefab_Syncer> prefab_children_with_syncer = new List<UUIDs_Prefab_Syncer>();

		void
		Start()
		{

		}

		void
		Update()
		{

		}

		public
		void
		assign_uuids( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			GetComponent<UUID_Assigner>().assign_uuid( net_data_reader.GetUUID() );
			for ( int i = 0, c = tracked_children.Count; i != c; ++i )
			{
				UUID_Assigner child = tracked_children[i];
				UUID uuid = net_data_reader.GetUUID();
				child.assign_uuid( uuid );
			}
			for ( int i = 0, c = prefab_children_with_syncer.Count; i != c; ++i )
			{
				UUIDs_Prefab_Syncer ups = prefab_children_with_syncer[i];
				ups.assign_uuids( net_data_reader );
			}
		}
		public
		void
		gather_uuids( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			net_data_writer.PutUUID( GetComponent<UUID_Assigner>().get_uuid() );
			for ( int i = 0, c = tracked_children.Count; i != c; ++i )
			{
				UUID_Assigner child = tracked_children[i];
				child.get_uuid().write( net_data_writer );
			}
			for ( int i = 0, c = prefab_children_with_syncer.Count; i != c; ++i )
			{
				UUIDs_Prefab_Syncer ups = prefab_children_with_syncer[i];
				ups.gather_uuids( net_data_writer );
			}
		}
	}
}