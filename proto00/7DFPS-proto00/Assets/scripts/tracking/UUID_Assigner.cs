﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	UUID_Assigner : MonoBehaviour
	{
		private static Dictionary<GameObject,UUID> uuid_from_go = new Dictionary<GameObject,UUID>();
		private static Dictionary<UUID,GameObject> go_from_uuid = new Dictionary<UUID,GameObject>();
		private UUID uuid;
		// NOTE(theGiallo): if true UUID_Assigners will assign UUIDs on Start().
		// If false they will remain invalid, until assigned by assign_uuid( UUID ).

		// NOTE(theGiallo): Awake is called just after instantiation,
		// so we can assume objects have UUIDS just after a prefab is instantiated.
		void
		Awake()
		{
			if ( Server.has_authority )
			{
				uuid = register_game_object_or_get_UUID( gameObject );
			}
		}
		private void OnDestroy()
		{
			if ( Server.has_authority )
			{
				Server.instance.on_tracked_go_destroy( gameObject );
			}
		}
		private void OnEnable()
		{
			if ( Server.has_authority )
			{
				Server.instance.on_tracked_go_enable( gameObject );
			}
		}
		private void OnDisable()
		{
			if ( Server.has_authority )
			{
				Server.instance.on_tracked_go_disable( gameObject );
			}
		}

		public
		void
		assign_uuid( UUID uuid )
		{
			Debug.Log( "[client] UUID " + uuid + " -> " + gameObject.name );
			assign_uuid( gameObject, uuid );
			this.uuid = uuid;
			Debug.Log( "[client] this.uuid =  " + this.uuid );
		}
		public
		UUID
		get_uuid()
		{
			return uuid;
		}

		public
		static
		UUID
		register_game_object_or_get_UUID( GameObject go )
		{
			UUID ret;

			if ( ! uuid_from_go.TryGetValue( go, out ret ) )
			{
				UUID uuid = UUID.get_new();
				uuid_from_go.Add( go, uuid );
				go_from_uuid.Add( uuid, go );
				ret = uuid;

				Debug.Log( "[server] UUID " + uuid + " -> " + go.name );
				Debug.Log( "[server] ret  " + ret );
			}

			return ret;
		}

		private
		static
		void
		assign_uuid( GameObject go, UUID uuid )
		{
			UUID old_uuid;
			if ( ! uuid_from_go.TryGetValue( go, out old_uuid ) )
			{
				uuid_from_go.Add( go, uuid );
				go_from_uuid.Add( uuid, go );
			} else
			{
				Debug.Log( "assigning uuid " + uuid + " to object '" + go.name + "' that had uuid " + old_uuid );
				uuid_from_go[go]   = uuid;
				go_from_uuid[uuid] = go;
			}
		}
		public
		static
		bool
		try_get_uuid( GameObject go, out UUID uuid )
		{
			bool ret = uuid_from_go.TryGetValue( go, out uuid );
			return ret;
		}
		public
		static
		bool
		try_get_go( UUID uuid, out GameObject go )
		{
			bool ret = go_from_uuid.TryGetValue( uuid, out go );
			return ret;
		}
	}

	public struct
	UUID
	{
		public uint value { get; private set; }
		// NOTE(theGiallo): 0 is the special value indicating it's unassigned
		private static uint next_uuid = 1;
		public bool is_valid()
		{
			return value != 0;
		}

		public UUID( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			value = net_data_reader.GetUInt();
		}
		public void write( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			net_data_writer.Put( value );
		}

		public
		static
		void
		restart()
		{
			next_uuid = 1;
		}

		public
		static
		UUID
		get_new()
		{
			UUID ret = new UUID();
			ret.value = next_uuid;
			if ( next_uuid == uint.MaxValue )
			{
				Debug.Log( "restarting UUID generation. We really went to town with UUID generation here!" );
				next_uuid = 0;
			} else
			{
				++next_uuid;
			}
			return ret;
		}

		public
		override
		string
		ToString()
		{
			string ret = value.ToString().PadLeft( 4 );
			return ret;
		}
		public
		override
		int
		GetHashCode()
		{
			int ret = value.GetHashCode();
			return ret;
		}
		public
		override
		bool
		Equals( object o )
		{
			bool ret = false;

			if ( ( o == null ) || !this.GetType().Equals( o.GetType() ) )
			{
				return ret;
			}
			ret = value == ((UUID)o).value;
			return ret;
		}
	}

	public static class UUID_NetDataExtensions
	{
		public static UUID GetUUID( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			UUID ret = new UUID( net_data_reader );
			return ret;
		}
		public static void PutUUID(  this LiteNetLib.Utils.NetDataWriter net_data_writer, UUID uuid )
		{
			uuid.write( net_data_writer );
		}
	}
}