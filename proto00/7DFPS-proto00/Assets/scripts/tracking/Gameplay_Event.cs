﻿//#define NORMALS_AS_3B_LINEAR
#define NORMALS_AS_2B_LINEAR_NORMALIZED
//#define NORMALS_AS_3B_SPHERE_VOLUME
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	Gameplay_Event
	{
		public enum
		Type
		{
			WEAPON_SHOT,
			WEAPON_HIT,
			//---
			_COUNT,
			UNDEFINED
		}
		public class
		Weapon_Shot
		{
			public Vector3 start_position;
			public Vector3 end_position;
		}
		public class
		Weapon_Hit
		{
			public Vector3        position;
			public Vector3        normal;
			public Sound_Material.Type hit_material_type;
		}

		public Type type { get; private set; }
		public Weapon_Shot weapon_shot { get; private set; }
		public Weapon_Hit  weapon_hit { get; private set; }

		public
		void
		set( Weapon_Shot weapon_shot )
		{
			Type target_type = Type.WEAPON_SHOT;
			type = weapon_shot == null ? ( type == target_type ? Type.UNDEFINED : type ) : target_type;
			this.weapon_shot = weapon_shot;
		}
		public
		void
		set( Weapon_Hit weapon_hit )
		{
			Type target_type = Type.WEAPON_HIT;
			type = weapon_hit == null ? ( type == target_type ? Type.UNDEFINED : type ) : target_type;
			this.weapon_hit = weapon_hit;
		}


		private static List<Gameplay_Event> gameplay_events_queue = new List<Gameplay_Event>();
		private static LiteNetLib.Utils.NetDataWriter this_data_writer = new LiteNetLib.Utils.NetDataWriter();
		private static LiteNetLib.Utils.NetDataReader this_reader      = new LiteNetLib.Utils.NetDataReader();
		private static LiteNetLib.Utils.NetDataReader specific_reader  = new LiteNetLib.Utils.NetDataReader();
		public static readonly Server.Message_Protocol.Message_ID message_ID = Server.Message_Protocol.Message_ID.GAMEPLAY_EVENTS;

		public
		static
		void
		enqueue_event( Gameplay_Event gameplay_event )
		{
			gameplay_events_queue.Add( gameplay_event );
		}
		public
		static
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_data_writer.Reset();

			foreach ( Gameplay_Event gameplay_event in gameplay_events_queue )
			{
				this_data_writer.Put_Gameplay_Event( gameplay_event );
			}

			if ( this_data_writer.Length > 0 )
			{
				//Debug.Log( "World_Syncer gathered " + this_data_writer.Length + "Bytes" );
				// NOTE(theGiallo): header
				net_data_writer.PutServerMessage_ID( message_ID );

				net_data_writer.PutBytesWithLength( this_data_writer.Data, 0, this_data_writer.Length );
			}

			gameplay_events_queue.Clear();
		}
		private static GFX_Manager gfx_manager;
		private static SFX_Manager sfx_manager;
		public
		static
		void
		manage( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			if ( gfx_manager == null )
			{
				gfx_manager = GameObject.FindObjectOfType<GFX_Manager>();
			}
			if ( sfx_manager == null )
			{
				sfx_manager = GameObject.FindObjectOfType<SFX_Manager>();
			}
			byte[] bytes = net_data_reader.GetBytesWithLength();
			//Debug.Log( "World_Syncer got " + bytes.Length + "Bytes" );
			this_reader.SetSource( bytes );
			while ( this_reader.AvailableBytes > 0 )
			{
				Gameplay_Event gameplay_event = this_reader.Get_Gameplay_Event();

				// NOTE(theGiallo): manage gameplay_event
				gfx_manager.activate_gfx( gameplay_event );
				sfx_manager.activate_sfx( gameplay_event );
			}
		}
	}
	public static class
	Gameplay_Event_NetDataRW_Extensions
	{
		public static void
		Put_Gameplay_Event_Type( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                         Gameplay_Event.Type type )
		{
			net_data_writer.Put( (byte)type );
		}
		public static Gameplay_Event.Type
		Get_Gameplay_Event_Type( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Gameplay_Event.Type ret;
			byte b = net_data_reader.GetByte();
			ret = (Gameplay_Event.Type)b;
			return ret;
		}
		public static void
		Put_Gameplay_Event( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                    Gameplay_Event gameplay_event )
		{
			net_data_writer.Put_Gameplay_Event_Type( gameplay_event.type );

			switch ( gameplay_event.type )
			{
				case Gameplay_Event.Type.WEAPON_HIT:
					net_data_writer.PutVector3( gameplay_event.weapon_hit.position );
					net_data_writer.put_normal( gameplay_event.weapon_hit.normal );
					net_data_writer.put_sound_material_type( gameplay_event.weapon_hit.hit_material_type );
					break;
				case Gameplay_Event.Type.WEAPON_SHOT:
					net_data_writer.PutVector3( gameplay_event.weapon_shot.start_position );
					net_data_writer.PutVector3( gameplay_event.weapon_shot.end_position );
					break;
			}
		}
		public static Gameplay_Event
		Get_Gameplay_Event( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Gameplay_Event ret = null;
			Gameplay_Event.Type type = net_data_reader.Get_Gameplay_Event_Type();
			switch ( type )
			{
				case Gameplay_Event.Type.WEAPON_HIT:
					ret = new Gameplay_Event();
					Gameplay_Event.Weapon_Hit weapon_hit = new Gameplay_Event.Weapon_Hit();
					weapon_hit.position          = net_data_reader.GetVector3();
					weapon_hit.normal            = net_data_reader.get_normal();
					weapon_hit.hit_material_type = net_data_reader.get_sound_material_type();
					ret.set( weapon_hit );
					break;
				case Gameplay_Event.Type.WEAPON_SHOT:
					ret = new Gameplay_Event();
					Gameplay_Event.Weapon_Shot weapon_shot = new Gameplay_Event.Weapon_Shot();
					weapon_shot.start_position = net_data_reader.GetVector3();
					weapon_shot.end_position   = net_data_reader.GetVector3();
					ret.set( weapon_shot );
					break;
			}
			return ret;
		}
	}
	public static class
	LiteNetLib_Normals
	{
		public
		static
		void
		put_normal( this LiteNetLib.Utils.NetDataWriter writer, Vector3 normal )
		{
			#if NORMALS_AS_3B_LINEAR
			writer.put_normal_3B_linear( normal );
			#elif NORMALS_AS_2B_LINEAR_NORMALIZED
			writer.put_normal_2B_linear_normalized( normal );
			#elif NORMALS_AS_3B_SPHERE_VOLUME
			writer.put_normal_3B_sphere_volume( normal );
			#else
			writer.PutVector3( normal );
			#endif
		}
		public
		static
		void
		put_normal_3B_linear( this LiteNetLib.Utils.NetDataWriter writer, Vector3 normal )
		{
			float k = 0.5f * 255f;
			writer.Put( encode_m1p1_0_255( normal.x ) );
			writer.Put( encode_m1p1_0_255( normal.y ) );
			writer.Put( encode_m1p1_0_255( normal.z ) );
		}
		public static byte encode_m1p1_0_255( float v )
		{
			byte ret;
			if ( v >= 0 )
			{
				ret = (byte)Mathf.RoundToInt( 127.0f + v * 128.0f );
			} else
			{
				ret = (byte)Mathf.RoundToInt( ( 1f + v ) * 127.0f );
			}
			return ret;
		}
		public static float decode_0_255_m1p1( byte b )
		{
			float ret;
			float bf = (float)b;
			if ( b >= 127 )
			{
				ret = ( bf - 127f ) / 128f;
			} else
			{
				ret = bf / 127f - 1f;
			}
			return ret;
		}
		public static byte encode_m1p1_0_127( float v )
		{
			byte ret;
			if ( v >= 0 )
			{
				ret = (byte)Mathf.RoundToInt( 63.0f + v * 64.0f );
			} else
			{
				ret = (byte)Mathf.RoundToInt( ( 1f + v ) * 63.0f );
			}
			return ret;
		}
		public static float decode_0_127_m1p1( byte b )
		{
			float ret;
			float bf = (float)b;
			if ( b >= 63 )
			{
				ret = ( bf - 63f ) / 64f;
			} else
			{
				ret = bf / 63f - 1f;
			}
			return ret;
		}
		public
		static
		void
		put_normal_2B_linear_normalized( this LiteNetLib.Utils.NetDataWriter writer, Vector3 normal )
		{
			writer.Put( encode_m1p1_0_255( normal.x ) );
			writer.Put( (byte) ( ( normal.z > 0 ? 0 : 1 << 7 ) | encode_m1p1_0_127( normal.y ) ) );
		}
		public
		static
		void
		put_normal_3B_sphere_volume( this LiteNetLib.Utils.NetDataWriter writer, Vector3 normal )
		{
			int x1 = 0;
			int y1 = 0;
			int z1 = 0;
			float l = 192f;// 127 * sqrt(2)
			int x2 = Mathf.RoundToInt( normal.x * l );
			int y2 = Mathf.RoundToInt( normal.y * l );
			int z2 = Mathf.RoundToInt( normal.z * l );
			V3S32[] candidates = bresenham_3D( x1, y1, z1, x2, y2, z2 );

			V3S32 best_p = candidates[1];
			float candidate_error = float.MaxValue;
			for ( int i = 1; i != candidates.Length; ++i )
			{
				V3S32 c = candidates[i];
				if ( c.x >   128 || c.y >   128 || c.z >   128
				  || c.x <= -127 || c.y <= -127 || c.z <= -127 )
				{
					continue;
				}

				Vector3 p = get_normal_from_sphere_volume_3B( c );
				float error = ( p - normal ).magnitude;
				if ( error < candidate_error )
				{
					candidate_error = error;
					best_p = c;
				}
			}

			writer.Put( (byte)( best_p.x + 127 ) );
			writer.Put( (byte)( best_p.y + 127 ) );
			writer.Put( (byte)( best_p.z + 127 ) );
		}
		public
		static
		Vector3
		get_normal( this LiteNetLib.Utils.NetDataReader reader )
		{
			Vector3 ret = new Vector3();

			#if NORMALS_AS_3B_LINEAR
			ret = reader.get_normal_3B_linear();
			#elif NORMALS_AS_2B_LINEAR_NORMALIZED
			ret = reader.get_normal_2B_linear_normalized();
			#elif NORMALS_AS_3B_SPHERE_VOLUME
			ret = reader.get_normal_3B_sphere_volume();
			#else
			ret = reader.GetVector3();
			#endif

			return ret;
		}
		public
		static
		Vector3
		get_normal_3B_linear( this LiteNetLib.Utils.NetDataReader reader )
		{
			Vector3 ret = new Vector3();

			byte b;
			b = reader.GetByte();
			ret.x = decode_0_255_m1p1( b );
			b = reader.GetByte();
			ret.y = decode_0_255_m1p1( b );
			b = reader.GetByte();
			ret.x = decode_0_255_m1p1( b );

			ret.Normalize();

			return ret;
		}
		public
		static
		Vector3
		get_normal_2B_linear_normalized( this LiteNetLib.Utils.NetDataReader reader )
		{
			Vector3 ret = new Vector3();

			byte b;
			b = reader.GetByte();
			ret.x = decode_0_255_m1p1( b );
			b = reader.GetByte();
			ret.y = decode_0_127_m1p1( (byte) ( b & ~( 1 << 7 ) ) );
			ret.z = ( ( b & ( 1 << 7 ) ) == 0 ? 1f : -1f ) * Mathf.Sqrt( 1f - ( ret.x * ret.x + ret.y * ret.y ) );

			ret.Normalize();

			return ret;
		}
		public
		static
		Vector3
		get_normal_3B_sphere_volume( this LiteNetLib.Utils.NetDataReader reader )
		{
			Vector3 ret = new Vector3();

			int bx = reader.GetByte() - 127;
			int by = reader.GetByte() - 127;
			int bz = reader.GetByte() - 127;
			V3S32 b3 = new V3S32( bx, by, bz );
			ret = get_normal_from_sphere_volume_3B( b3 );

			return ret;
		}

		private static Vector3 get_normal_from_sphere_volume_3B( V3S32 v )
		{
			Vector3 ret = new Vector3();
			ret.x = (float)( v.x );
			ret.y = (float)( v.y );
			ret.z = (float)( v.z );
			ret.Normalize();
			return ret;
		}

		private static int abs( int x ) { return x >=0 ? x : -x; }
		private static int max( int x, int y ) { return x >= y ? x : y; }
		private static int max( params int[] xs ) { int ret = xs[0]; foreach ( int x in xs ) { ret = max( ret, x ); } return ret; }

		private struct V3S32
		{
			public int x, y, z;
			public
			V3S32( int x, int y, int z )
			{
				this.x = x;
				this.y = y;
				this.z = z;
			}
		}

		// https://gist.githubusercontent.com/yamamushi/5823518/raw/10a1065ddb1db4f2210d3ba9989e1e87998ca612/bresenham3d
		private static V3S32[]
		bresenham_3D( int x1, int y1, int z1, int x2, int y2, int z2 )
		{
			V3S32[] arr = new V3S32[ max( abs(x1-x2), abs(y1-y2), abs(z1-z2) ) + 1 ];

			int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2, iarr;

			int X1 = x1;
			int Y1 = y1;
			int Z1 = z1;
			dx = x2 - x1;
			dy = y2 - y1;
			dz = z2 - z1;
			x_inc = ( dx < 0 ) ? -1 : 1;
			l = abs( dx );
			y_inc = ( dy < 0 ) ? -1 : 1;
			m = abs( dy );
			z_inc = ( dz < 0 ) ? -1 : 1;
			n = abs( dz );
			dx2 = l << 1;
			dy2 = m << 1;
			dz2 = n << 1;
			iarr = 0;

			if ( ( l >= m ) && ( l >= n ) )
			{
				err_1 = dy2 - l;
				err_2 = dz2 - l;
				for ( i = 0; i < l; i++ )
				{
					arr[iarr++] = new V3S32( X1, Y1, Z1 );
					if ( err_1 > 0 )
					{
						Y1 += y_inc;
						err_1 -= dx2;
					}
					if ( err_2 > 0 )
					{
						Z1 += z_inc;
						err_2 -= dx2;
					}
					err_1 += dy2;
					err_2 += dz2;
					X1 += x_inc;
				}
			} else
			if ( ( m >= l ) && ( m >= n ) )
			{
				err_1 = dx2 - m;
				err_2 = dz2 - m;
				for ( i = 0; i < m; i++ )
				{
					arr[iarr++] = new V3S32( X1, Y1, Z1 );
					if ( err_1 > 0 )
					{
						X1 += x_inc;
						err_1 -= dy2;
					}
					if ( err_2 > 0 )
					{
						Z1 += z_inc;
						err_2 -= dy2;
					}
					err_1 += dx2;
					err_2 += dz2;
					Y1 += y_inc;
				}
			} else
			{
				err_1 = dy2 - n;
				err_2 = dx2 - n;
				for ( i = 0; i < n; i++ )
				{
					arr[iarr++] = new V3S32( X1, Y1, Z1 );
					if ( err_1 > 0 )
					{
						Y1 += y_inc;
						err_1 -= dz2;
					}
					if ( err_2 > 0 )
					{
						X1 += x_inc;
						err_2 -= dz2;
					}
					err_1 += dy2;
					err_2 += dx2;
					Z1 += z_inc;
				}
			}
			arr[iarr++] = new V3S32( X1, Y1, Z1 );

			return arr;
		}


	#if NO
		private
		static
		void
		brasenham_int_low( int x0, int y0, int x1, int y1, int[] arr )
		{
			int dx = x1 - x0;
			int dy = y1 - y0;
			int yi = 1;
			if ( dy < 0 )
			{
				yi = -1;
				dy = -dy;
			}
			int D = 2 * dy - dx;
			int y = y0;
			int iarr = 0;
			for ( int x = x0, xs = ( x0 >= x1 ? 1 : -1 ); x != x1 + 1; x += xs )
			{
				arr[iarr++] = x;
				arr[iarr++] = y;

				if ( D > 0 )
				{
					y = y + yi;
					D = D - 2 * dx;
				}
				D = D + 2 * dy;
			}
		}
		private
		static
		void
		brasenham_int_high( int x0, int y0, int x1, int y1, int[] arr )
		{
			int dx = x1 - x0;
			int dy = y1 - y0;
			int xi = 1;
			if ( dx < 0 )
			{
				xi = -1;
				dx = -dx;
			}
			int D = 2 * dx - dy;
			int x = x0;

			int iarr = 0;
			for ( int y = y0, ys = ( y0 >= y1 ? 1 : -1 ); y != y1 + 1; y += ys )
			{
				arr[iarr++] = x;
				arr[iarr++] = y;

				if ( D > 0 )
				{
					x = x + xi;
					D = D - 2 * dy;
				}
				D = D + 2 * dx;
			}
		}

		private
		static
		int[]
		brasenham_int( int x0, int y0, int x1, int y1 )
		{
			int l = Mathf.Max( Mathf.Abs( x0 - x1 ), Mathf.Abs( y0 - y1 ) ) + 1;
			int[] ret = new int[l];
			if ( Mathf.Abs( y1 - y0 ) < Mathf.Abs( x1 - x0 ) )
			{
				if ( x0 > x1 )
				{
					brasenham_int_low( x1, y1, x0, y0, ret );
				} else
				{
					brasenham_int_low( x0, y0, x1, y1, ret );
				}
			} else
			{
				if ( y0 > y1 )
				{
					brasenham_int_high( x1, y1, x0, y0, ret );
				} else
				{
					brasenham_int_high( x0, y0, x1, y1, ret );
				}
			}
			return ret;
		}
	#endif
}
}