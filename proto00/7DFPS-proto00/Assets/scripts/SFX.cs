﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	SFX : MonoBehaviour, IPooled<SFX>
	{
		public int         _pool_index;
		public SFX_Manager manager;
		public AudioSource audio_source;

		public int get_pool_index()
		{
			return _pool_index;
		}

		public void set_pool_index( int new_pool_index )
		{
			_pool_index = new_pool_index;
		}

		public void put_back_in_pool()
		{
			gameObject.SetActive( false );
			manager.sound_sources.put_back( this );
		}
		public
		void
		Update()
		{
			if ( !audio_source.isPlaying )
			{
				put_back_in_pool();
			}
		}

		public
		SFX
		new_one()
		{
			SFX ret = Instantiate( this );
			ret.gameObject.SetActive( false );
			ret.transform.SetParent( manager.transform );
			Debug.Assert( ret.manager == manager );
			return ret;
		}

		public
		void
		play( AudioClip audio_clip, float pitch = 1.0f, bool spatialize = true, float spatial_blend = 1.0f )
		{
			audio_source.spatialBlend = spatial_blend;
			audio_source.spatialize   = spatialize;
			audio_source.pitch        = pitch;
			audio_source.clip         = audio_clip;
			audio_source.loop         = false;
			audio_source.playOnAwake  = true;
		}
	}
}
