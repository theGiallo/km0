﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent( typeof ( Collider ) )]
	public class
	Sound_Material : MonoBehaviour
	{
		public enum
		Type
		{
			UNDEFINED,
			ROBOT_METAL,
			CONCRETE,
			COBBLESTONE_ROAD,
			METAL_BOX,
			LASER_BEAM,
			//---
			_COUNT,
		}
		public Type type;

		public
		static
		Type
		get_sound_material( Collider collider )
		{
			Type ret = Type.UNDEFINED;
			Sound_Material sm = collider.gameObject.GetComponent<Sound_Material>();
			if ( sm != null )
			{
				ret = sm.type;
			}
			return ret;
		}
	}
	public static class
	LiteNetLib_Sound_Material
	{
		public
		static
		void
		put_sound_material_type( this LiteNetLib.Utils.NetDataWriter writer, Sound_Material.Type sound_material_type )
		{
			writer.Put( (byte) sound_material_type );
		}
		public
		static
		Sound_Material.Type
		get_sound_material_type( this LiteNetLib.Utils.NetDataReader reader )
		{
			Sound_Material.Type ret = (Sound_Material.Type)reader.GetByte();
			return ret;
		}
	}
}