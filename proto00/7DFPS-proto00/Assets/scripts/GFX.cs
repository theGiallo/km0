﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	GFX : MonoBehaviour, IPooled<GFX>
	{
		public int                 _pool_index;
		public Gameplay_Event.Type _type;
		public GFX_Manager         manager;
		public Gameplay_Event      gameplay_event;

		public int get_pool_index()
		{
			return _pool_index;
		}

		public void set_pool_index( int new_pool_index )
		{
			_pool_index = new_pool_index;
		}

		public GFX new_one()
		{
			GFX ret = Instantiate( this );
			ret.gameObject.SetActive( false );
			ret.transform.SetParent( manager.transform );
			Debug.Assert( ret.manager == manager );
			return ret;
		}

		public void put_back_in_pool()
		{
			gameObject.SetActive( false );
			manager.pools[(int)_type].put_back( this );
		}

		void Start()
		{
		}

		void Update()
		{
		}

		public
		void
		activate_gfx( Gameplay_Event gameplay_event )
		{
			this.gameplay_event = gameplay_event;

			switch ( _type )
			{
				case Gameplay_Event.Type.WEAPON_HIT:
					gameObject.SetActive( true );
					break;
				case Gameplay_Event.Type.WEAPON_SHOT:
					gameObject.SetActive( true );
					break;
				default: break;
			}
		}
	}
}
