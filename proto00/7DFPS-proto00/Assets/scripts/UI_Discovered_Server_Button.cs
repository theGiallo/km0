﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace fps
{
	public class
	UI_Discovered_Server_Button : MonoBehaviour
	{
		public Text                   ui_server_name_text;
		// TODO(theGiallo): it seems that LiteNetLib offers pings only if connected, automatically
		public Text                   ui_ping_ms_text;
		public Button                 ui_button;
		public Image                  bg;
		public LiteNetLib.NetEndPoint net_end_point;

		public void on_select()
		{
			bg.color = UI_Manager.instance.button_selected_color;
		}
		public void on_deselect()
		{
			bg.color = UI_Manager.instance.button_unselected_color;
		}

		//void
		//Start()
		//{
		//}

		//void
		//Update()
		//{
		//}
	}
}