﻿using LiteNetLib.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	Player_Character_Factory : MonoBehaviour
	{
		public static Player_Character_Factory instance;

		public Prefab_Hash pc_prefab;

		public
		Player_Character
		create_pc()
		{
			GameObject go = Prefabs_Manager.instance.instantiate_prefab( pc_prefab );
			//go.SetActive( false );
			Player_Character ret = go.GetComponent<Player_Character>();
			return ret;
		}

		private void Awake()
		{
			instance = this;
		}
	}
}