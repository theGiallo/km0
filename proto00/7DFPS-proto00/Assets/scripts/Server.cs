﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace fps
{
	public class
	Server : MonoBehaviour, LiteNetLib.INetEventListener
	{
		public static Server instance { get; private set; }
		public static bool has_authority { get { bool ret = instance != null && instance.gameObject.activeInHierarchy; return ret; } }

		public string    connect_key;
		public string    server_name;
		public int       max_connections = 64;
		public int       port            = 0;
		public On_Client on_client_connected      = new On_Client();
		public On_Client on_client_disconnected   = new On_Client();
		public On_Client on_client_latency_update = new On_Client();
		public On_Client on_client_rename         = new On_Client();

		public ServerAdvertiser advertiser;

		private LiteNetLib.NetManager          net_manager;
		private LiteNetLib.Utils.NetDataWriter data_writer;
		private bool got_error = false;
		private bool started   = false;
		private bool on_map    = false;

		public class On_Client : UnityEvent<Connected_Client_Data> { }

		public class
		Connected_Client_Data
		{
			public NetPeer peer;
			public string  name;
			public ushort  latency;

			public Player_Character player_character;
		}
		private Dictionary<NetEndPoint,Connected_Client_Data> connected_clients = new Dictionary<NetEndPoint, Connected_Client_Data>();

		private Dictionary<UUID,Transform>  transforms_tracked_by_uuid = new Dictionary<UUID, Transform>();

		public
		void
		on_prefab_spawn( Prefab_Hash ph, UUIDs_Prefab_Syncer ups )
		{
			NetDataWriter w = mp.encode_prefab_spawn( ph, ups );
			net_manager.SendToAll( w, SendOptions.ReliableOrdered );
		}
		public
		void
		on_tracked_go_destroy( GameObject go )
		{
			Debug.Log( "TODO IMPLEMENT - on_tracked_go_destroy( " + go.name + ")" );
		}
		public
		void
		on_tracked_go_enable( GameObject go )
		{
			Debug.Log( "TODO IMPLEMENT - on_tracked_go_enable( " + go.name + ")" );
		}
		public
		void
		on_tracked_go_disable( GameObject go )
		{
			Debug.Log( "TODO IMPLEMENT - on_tracked_go_disable( " + go.name + ")" );
		}

		public class
		Message_Protocol
		{
			public enum
			Message_ID
			{
				CLIENT_CONNECTED,
				CLIENT_DISCONNECTED,
				CLIENT_RENAME,
				CLIENT_LATENCY_UPDATE,
				START_MAP,
				STOP_MAP,
				PREFAB_SPAWN,
				SYNC_WORLD,
				ASSIGN_PLAYER_CHARACTER,
				GAMEPLAY_EVENTS,
				//---
				_COUNT
			}

			NetDataWriter net_data_writer = new NetDataWriter();

			public
			NetDataWriter
			encode_client_connected( Connected_Client_Data data )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.CLIENT_CONNECTED );

				w.Put( data.peer.EndPoint );
				//w.Put( data.name );
				//w.Put( data.latency );

				return w;
			}
			public static
			Client.Other_Client
			decode_client_connected( NetDataReader r )
			{
				Client.Other_Client c = new Client.Other_Client();

				c.net_end_point = r.GetNetEndPoint();
				//c.name          = r.GetString();
				//c.ping          = r.GetUShort();

				return c;
			}

			public
			NetDataWriter
			encode_client_disconnected( Connected_Client_Data data )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.CLIENT_DISCONNECTED );

				w.Put( data.peer.EndPoint );

				return w;
			}
			public static
			NetEndPoint
			decode_client_disconnected( NetDataReader r )
			{
				NetEndPoint ret = r.GetNetEndPoint();
				return ret;
			}

			public
			NetDataWriter
			encode_client_rename( Connected_Client_Data data )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.CLIENT_RENAME );

				w.Put( data.peer.EndPoint );
				w.Put( data.name );

				return w;
			}
			public static
			void
			decode_client_rename( NetDataReader r, out NetEndPoint net_end_point, out string new_name )
			{
				net_end_point = r.GetNetEndPoint();
				new_name      = r.GetString();
			}

			public
			NetDataWriter
			encode_client_latency_update( Connected_Client_Data data )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.CLIENT_LATENCY_UPDATE );

				w.Put( data.peer.EndPoint );
				w.Put( data.latency );

				return w;
			}
			public static
			void
			decode_client_latency_update( NetDataReader r, out NetEndPoint net_end_point, out ushort new_latency )
			{
				net_end_point = r.GetNetEndPoint();
				new_latency   = r.GetUShort();
			}

			public
			NetDataWriter
			encode_start_map()
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.START_MAP );

				return w;
			}
			//public static
			//void
			//decode_start_map( NetDataReader r )
			//{
			//}

			public
			NetDataWriter
			encode_stop_map()
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.STOP_MAP );

				return w;
			}
			//public static
			//void
			//decode_stop_map( NetDataReader r )
			//{
			//}

			public
			NetDataWriter
			encode_prefab_spawn( Prefab_Hash ph, UUIDs_Prefab_Syncer ups )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.PREFAB_SPAWN );
				w.PutPrefab_Hash( ph );
				ups.gather_uuids( w );

				return w;
			}
			public static
			Prefab_Hash
			decode_prefab_spawn( NetDataReader r )
			{
				Prefab_Hash ret = r.GetPrefab_Hash();
				return ret;
			}

			public
			NetDataWriter
			encode_world_sync()
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				// NOTE(theGiallo): this is put by gather, because gather could put nothing at all
				// w.PutServerMessage_ID( Message_ID.SYNC_WORLD );
				World_Syncer.gather( w );

				return w;
			}
			public static
			void
			decode_world_sync( NetDataReader r )
			{
				World_Syncer.assign( r );
			}

			public
			NetDataWriter
			encode_assign_player_character( Player_Character pc )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutServerMessage_ID( Message_ID.ASSIGN_PLAYER_CHARACTER );
				w.PutUUID( pc.GetComponent<UUID_Assigner>().get_uuid() );

				return w;
			}
			public
			static
			UUID
			decode_assign_player_character( NetDataReader reader )
			{
				UUID ret = reader.GetUUID();
				return ret;
			}

			public
			NetDataWriter
			encode_gameplay_events()
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				Gameplay_Event.gather( w );

				return w;
			}
			public
			static
			void
			decode_gameplay_events( NetDataReader reader )
			{
				Gameplay_Event.manage( reader );
			}
		}

		Message_Protocol mp = new Message_Protocol();

		void INetEventListener.OnNetworkError( NetEndPoint endPoint, int socketErrorCode )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkLatencyUpdate( NetPeer peer, int latency )
		{
			Connected_Client_Data ccd;
			bool b_res = connected_clients.TryGetValue( peer.EndPoint, out ccd );
			if ( !b_res )
			{
				Debug.LogError( "server received network latency update from unknown client" );
			}
			ccd.latency = (ushort)latency;
			on_client_latency_update.Invoke( ccd );

			NetDataWriter w = mp.encode_client_latency_update( ccd );
			net_manager.SendToAll( w, SendOptions.ReliableOrdered, peer );
		}

		void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )
		{
			//Debug.Log( "void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )" );

			Connected_Client_Data ccd = null;
			if ( ! connected_clients.TryGetValue( peer.EndPoint, out ccd ) )
			{
				Debug.LogError( "server received from a peer that is not int the server" );
				return;
			}

			Client.Message_Protocol.Message_ID m_id = reader.GetClientMessage_ID();
			switch ( m_id )
			{
				case Client.Message_Protocol.Message_ID.CLIENT_RENAME:
				{
					Debug.Log( "received " + m_id );
					string new_name = Client.Message_Protocol.decode_client_rename( reader );
					ccd.name = new_name;
					on_client_rename.Invoke( ccd );
					NetDataWriter w = mp.encode_client_rename( ccd );
					net_manager.SendToAll( w, SendOptions.ReliableOrdered, peer );
					break;
				}
				case Client.Message_Protocol.Message_ID.INPUT_ACTION_FRONT:
				{
					List<Input_Wrapper.Action_With_Front> awfs =
					   Client.Message_Protocol.decode_input_action_front( reader );
					ccd.player_character.manage_input( awfs );
					break;
				}
				case Client.Message_Protocol.Message_ID.MOUSE_DELTA:
				{
					Mouse_Wrapper.Mouse_Delta md =
					   Client.Message_Protocol.decode_mouse_delta( reader );
					ccd.player_character.manage_mouse_delta( md );
					break;
				}
				default:
					Debug.LogError( "received message of unknown type " + m_id + " " + (int)m_id );
					break;
			}
		}

		void INetEventListener.OnNetworkReceiveUnconnected( NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType )
		{
			IPEndPoint remote_ipendpoint = remoteEndPoint.EndPoint;
			Debug.Log( "[server] received from " + remote_ipendpoint + " ( " + remoteEndPoint + " ) " + messageType + " " + reader.GetString() );
		}

		void INetEventListener.OnPeerConnected( NetPeer peer )
		{
			Debug.Log( "server void INetEventListener.OnPeerConnected( NetPeer peer " + peer + " )" );
			Connected_Client_Data ccd = new Connected_Client_Data();
			ccd.peer = peer;
			connected_clients.Add( peer.EndPoint, ccd );
			on_client_connected.Invoke( ccd );

			NetDataWriter w = mp.encode_client_connected( ccd );
			net_manager.SendToAll( w, SendOptions.ReliableOrdered, peer );

			foreach ( Connected_Client_Data c in connected_clients.Values )
			{
				if ( c != ccd )
				{
					w = mp.encode_client_connected( c );
					peer.Send( w, SendOptions.ReliableOrdered );

					w = mp.encode_client_rename( c );
					peer.Send( w, SendOptions.ReliableOrdered );

					//w = mp.encode_client_latency_update( c );
					//peer.Send( w, SendOptions.ReliableOrdered );
				}
			}
		}

		void INetEventListener.OnPeerDisconnected( NetPeer peer, DisconnectInfo disconnectInfo )
		{
			Connected_Client_Data ccd = connected_clients[peer.EndPoint];
			on_client_disconnected.Invoke( ccd );
			connected_clients.Remove( peer.EndPoint );

			NetDataWriter w = mp.encode_client_disconnected( ccd );
			net_manager.SendToAll( w, SendOptions.ReliableOrdered, peer );
		}

		void Awake()
		{
			instance = this;
			           gameObject.SetActive( false );
			advertiser.gameObject.SetActive( false );
		}

		void Start()
		{
			data_writer = new NetDataWriter();
		}

		void Update()
		{
			if ( got_error )
			{
				setup();
				if ( got_error )
				{
					return;
				}
			}

			net_manager.PollEvents();

			// TODO(theGiallo): move into setters
			advertiser.server_name = server_name;
			advertiser.connected_players_count = (byte)net_manager.PeersCount;
			advertiser.server_port = net_manager.LocalPort;

			if ( on_map )
			{
				NetDataWriter net_data_writer = mp.encode_world_sync();
				if ( net_data_writer.Length > 0 )
				{
					net_manager.SendToAll( net_data_writer, SendOptions.ReliableOrdered );
				}
				net_data_writer = mp.encode_gameplay_events();
				if ( net_data_writer.Length > 0 )
				{
					net_manager.SendToAll( net_data_writer, SendOptions.ReliableOrdered );
				}
			}
		}

		private void OnDestroy()
		{
			stop();
		}

		public
		void
		create()
		{
			net_manager = new LiteNetLib.NetManager( this, max_connections, connect_key );
			connected_clients.Clear();
			//net_manager.DiscoveryEnabled = true;

			setup();
		}
		public
		void
		stop()
		{
			if ( net_manager != null )
			{
				net_manager.Stop();
			}
			advertiser.gameObject.SetActive( false );
			connected_clients.Clear();
		}

		private
		void
		setup()
		{
			bool b_res;
			b_res = net_manager.Start( port = 0 );
			if ( ! b_res )
			{
				Debug.LogError( "failed to start on port = " + port );
				got_error = true;
			} else
			{
				got_error = false;
				port = net_manager.LocalPort;
				advertiser.gameObject.SetActive( true );
				advertiser.server_name = server_name;
				Debug.Log( "server '" + server_name + "' listening on port " + port );
			}
		}

		public
		void
		do_start_map()
		{
			NetDataWriter w = mp.encode_start_map();
			net_manager.SendToAll( w, SendOptions.ReliableOrdered );
			on_map = true;

			foreach ( Connected_Client_Data ccd in connected_clients.Values )
			{
				ccd.player_character = Player_Character_Factory.instance.create_pc();
				ccd.peer.Send( mp.encode_assign_player_character( ccd.player_character ), SendOptions.ReliableOrdered );
			}
		}
		public void
		do_stop_map()
		{
			NetDataWriter w = mp.encode_stop_map();
			net_manager.SendToAll( w, SendOptions.ReliableOrdered );
			on_map = false;
			foreach ( Connected_Client_Data ccd in connected_clients.Values )
			{
				Destroy( ccd.player_character.gameObject );
				ccd.player_character = null;
			}
		}
	}
	public static class Server_Message_Protocol_NetDataExtensions
	{
		public
		static
		Server.Message_Protocol.Message_ID
		GetServerMessage_ID( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Server.Message_Protocol.Message_ID ret = (Server.Message_Protocol.Message_ID)net_data_reader.GetByte();
			return ret;
		}
		public
		static
		void
		PutServerMessage_ID( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                     Server.Message_Protocol.Message_ID message_id )
		{
			net_data_writer.Put( (byte)message_id );
		}
	}
}