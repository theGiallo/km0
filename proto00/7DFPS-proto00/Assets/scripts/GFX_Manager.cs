﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	GFX_Manager : MonoBehaviour
	{
		public Pool<GFX>[] pools;
		public GFX[]       spawners = new GFX[(int)Gameplay_Event.Type._COUNT];

		private
		void
		Awake()
		{
			pools    = new Pool<GFX>[(int)Gameplay_Event.Type._COUNT];
			for ( int i = 0; i != pools.Length; ++i )
			{
				Debug.Assert( spawners[i]._type == (Gameplay_Event.Type)i );
				spawners[i].manager = this;
				pools[i] = new Pool<GFX>();
				pools[i].init( spawners[i] );
			}
		}
		public
		void
		activate_gfx( Gameplay_Event gameplay_event )
		{
			GFX gfx = pools[(int)gameplay_event.type].take_away();
			gfx.activate_gfx( gameplay_event );
		}
	}
}