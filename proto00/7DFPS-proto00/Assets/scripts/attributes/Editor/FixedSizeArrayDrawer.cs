﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( FixedSizeArrayAttribute ) )]
public
class
FixedSizeArrayDrawer : PropertyDrawer
{
	public
	override
	float
	GetPropertyHeight( SerializedProperty property, GUIContent label )
	{
		//return EditorGUI.GetPropertyHeight( property, label, true );
		float ret = 0;

		string[] variableName = property.propertyPath.Split('.');
		SerializedProperty p = property.serializedObject.FindProperty(variableName[0]);
		Debug.Assert( p.isArray );

		string tyep_str = p.arrayElementType;
		int size = p.arraySize;

		int old_indent = EditorGUI.indentLevel;
		++EditorGUI.indentLevel;

		for ( int i = 0; i != size; ++i )
		{
			SerializedProperty pe = p.GetArrayElementAtIndex( i );
			GUIContent l = new GUIContent( "Element " + i );
			float h = EditorGUI.GetPropertyHeight( pe, l );
			ret += h;
		}
		return ret;
	}

	public
	override
	void
	OnGUI( Rect position, SerializedProperty property, GUIContent label )
	{
		string[] variableName = property.propertyPath.Split('.');
		Debug.Log( "property.propertyPath = '" + property.propertyPath + "'" );
		SerializedProperty p = property.serializedObject.FindProperty(variableName[0]);
		Debug.Log( "p.displayName = " + p.displayName );
		Debug.Assert( p.isArray );

		string tyep_str = p.arrayElementType;
		int size = p.arraySize;

		EditorGUI.BeginProperty( position, label, property );

		// NOTE(theGiallo): drawing label
		position = EditorGUI.PrefixLabel( position, GUIUtility.GetControlID( FocusType.Passive ), label );

		int old_indent = EditorGUI.indentLevel;
		++EditorGUI.indentLevel;

		Rect rect = EditorGUI.IndentedRect( position );

		for ( int i = 0; i != size; ++i )
		{
			SerializedProperty pe = p.GetArrayElementAtIndex( i );
			GUIContent l = new GUIContent( property.displayName + "[" + i + "] " + pe.displayName);
			EditorGUI.PropertyField( rect, pe, l, includeChildren: true );
			Vector2 pos = rect.position;
			float h = EditorGUI.GetPropertyHeight( pe, l );
			pos.y += h;
			rect.position = pos;
			rect.height -= h;
		}

		EditorGUI.indentLevel = old_indent;

		EditorGUI.EndProperty();
	}
}