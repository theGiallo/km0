﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public static class
	Utilities
	{
		public
		static
		uint
		u32_FNV1a( byte[] bytes )
		{
			uint ret;

			const uint FNV_prime    = 16777619;
			const uint offset_basis = 2166136261;

			ret = offset_basis;
			for ( uint i = 0; i < bytes.Length; ++i )
			{
				ret = ret ^ (uint)bytes[i];
				ret = ret * FNV_prime;
			}

			return ret;
		}
		public
		static
		uint
		u32_FNV1a( string s )
		{
			uint ret;

			byte[] bytes = System.Text.Encoding.UTF8.GetBytes( s );

			ret = u32_FNV1a( bytes );

			return ret;
		}
		public
		static
		int
		s32_FNV1a( byte[] bytes )
		{
			int ret;
			uint u = u32_FNV1a( bytes );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}
		public
		static
		int
		s32_FNV1a( string s )
		{
			int ret;
			uint u = u32_FNV1a( s );
			ret = System.BitConverter.ToInt32( System.BitConverter.GetBytes( u ), 0 );
			return ret;
		}

		public
		static
		float
		deg_to_0_360( float deg )
		{
			float ret = deg - Mathf.Floor( deg / 360 ) * 360;
			ret = ret < 0 ? ret + 360 : ret;
			return ret;
		}

		public static float clamp_deg_0_360( float deg_0360, float min_deg_0360, float max_deg_0360 )
		{
			float ret;
			if ( min_deg_0360 > max_deg_0360 )
			{
				ret = deg_0360 < max_deg_0360
				    ? deg_0360
				    : ( deg_0360 > min_deg_0360
				      ? deg_0360
				      : ( deg_0360 <= 180
				        ? max_deg_0360
				        : min_deg_0360 ) );
			} else
			{
				ret = Mathf.Clamp( deg_0360, min_deg_0360, max_deg_0360 );
			}
			return ret;
		}

		public
		static
		float
		deg_to_center0( float deg )
		{
			float ret = deg_to_0_360( deg + 180 ) - 180;
			return ret;
		}

		public
		static
		float
		clamp_deg_center0( float deg_center0, float min_deg_center0, float max_deg_center0 )
		{
			float ret = Mathf.Clamp( deg_center0, min_deg_center0, max_deg_center0 );
			return ret;
		}

		public
		static
		float
		clamp_deg_to_center0( float deg )
		{
			float ret = Mathf.Clamp( deg, -180, 180 );
			return ret;
		}

		public
		static
		bool
		f_eps( float f0, float f1, float eps )
		{
			bool ret = Mathf.Abs( f0 - f1 ) < eps;
			return ret;
		}

		public static
		bool
		v3_eps( Vector3 v0, Vector3 v1, float eps )
		{
			bool ret = f_eps( v0.x, v0.x, eps ) && f_eps( v0.y, v1.y, eps ) && f_eps( v0.z, v1.z, eps );
			return ret;
		}

		public
		static
		bool
		same_sign( float a, float b )
		{
			bool ret = ( a > 0 && b > 0 ) || ( a < 0 && b < 0 ) || ( a == 0 && b == 0 );
			return ret;
		}
		public
		static
		bool
		a_greater_in_module_than_b_and_same_sign( float a, float b )
		{
			bool ret = same_sign( a, b ) && Mathf.Abs( a ) > Mathf.Abs( b );
			return ret;
		}

		public
		static
		float
		get_angle_deg_to_align_two_points_to_target( Vector2 target, Vector2 forward_point, Vector2 backward_point )
		{
			Vector2 T = target;
			Vector2 P = forward_point;
			Vector2 B = backward_point;

			Vector2 BOdir2   = -B.normalized;
			Vector2 BPdir2   = ( P - B ).normalized;
			//Vector3 BOdir3   = new Vector3( BOdir2.x, 0, BOdir2.y );
			//Vector3 BPdir3   = new Vector3( BPdir2.x, 0, BPdir2.y );
			//float sin_beta   = Vector3.Cross( BOdir3, BPdir3 ).y;
			float deg_beta   = Vector2.Angle( BOdir2, BPdir2 );
			float sin_beta   = Mathf.Sin( Mathf.Deg2Rad * deg_beta );
			//float sin_alpha  = Vector3.Cross( P.normalized, B.normalized ).magnitude;
			float sin_delta  = sin_beta * B.magnitude / T.magnitude;
			float deg_delta  = Mathf.Rad2Deg * Mathf.Asin( sin_delta );
			float deg_alpha  = 180 - deg_beta - deg_delta;
			float deg_alpha_ = Vector2.Angle( T, B );

			float y_rotation  = deg_alpha - deg_alpha_;
			return y_rotation;
		}

		public
		class
		Timer
		{
			public  bool  active = false;
			private float target_time;
			private float duration;
			public  bool use_fixed_time = false;

			public Timer( float duration, bool autostart = false )
			{
				this.duration = duration;
				if ( autostart )
				{
					restart();
				}
			}
			public
			void
			restart( float duration )
			{
				this.duration = duration;
				target_time = time() + duration;
				active = true;
			}
			public
			void
			restart()
			{
				target_time = time() + duration;
				active = true;
			}
			public
			float
			remaining_time()
			{
				float ret = target_time - time();
				return ret;
			}
			public
			float
			passed_time()
			{
				float ret = time() - target_time + duration;
				return ret;
			}
			public
			float
			remaining_percentage()
			{
				float ret = remaining_time() / duration;
				return ret;
			}
			public
			float
			passed_percentage()
			{
				float ret = passed_time() / duration;
				return ret;
			}

			private
			float
			time()
			{
				float ret = use_fixed_time ? Time.fixedTime : Time.time;
				return ret;
			}
		}
	}
}