﻿[System.Serializable]
public
class
ArrayWrapper<T>
{
	public T[] array;

	public
	ArrayWrapper( int length )
	{
		array = new T[length];
	}

	public int Length
	{
		get
		{
			return array.Length;
		}
	}

	public T this[int i]
	{
		get
		{
			return array[i];
		}
		set
		{
			array[i] = value;
		}
	}
}
//[System.Serializable]
//public class Array_Wrapper_Base {}