﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace fps
{
	[ExecuteInEditMode]
	public class
	UI_Bar : MonoBehaviour
	{
		public Image borders;
		public Image bg;
		public Image fg;
		public Text  text;

		public float _current_value;
		public float _max_value;
		public float _min_value;
		public bool  clamp = true;
		public bool  show_over_full_text = true;

		public float value
		{
			get
			{
				return _current_value;
			}
			set
			{
				if ( clamp )
				{
					_current_value = Mathf.Clamp( _current_value, _min_value, _max_value );
				}
				_current_value = value;
				float size = ( _current_value - _min_value ) / ( _max_value - _min_value ) * bg.rectTransform.rect.width;
				fg.rectTransform.SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, size );
				text.text = "" + _current_value + ( show_over_full_text ? "/" + _max_value : "" );
			}
		}
		#if UNITY_EDITOR
		private
		void
		Update()
		{
			if ( _max_value == 0 )
			{
				_max_value = 1;
			}
			value = _current_value;
		}
		#endif
	}
}