﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[Serializable]
	public class Sounds : ArrayWrapper<SFX_Manager.Sound>
	{
		public
		Sounds(int length) : base(length) { }
	}

	public class
	SFX_Manager : MonoBehaviour
	{

		[System.Serializable]
		public struct
		SFX_Materials_Couple
		{
			public Sound_Material.Type type_a, type_b;
			public Sound_Type sound_type;
		}
		public enum
		Sound_Type
		{
			CONCRETE_SMALL_HIT,
			ROBOT_METAL_HIT_BY_LASER,
			LASER_WEAPON_SHOT,
			//---
			_COUNT,
			UNDEFINED
		}

		[Serializable]
		public
		struct
		Pitch_Range
		{
			public float min, max;
			public
			Pitch_Range( float min = 1, float max = 1)
			{
				this.min = min;
				this.max = max;
			}
		}

		[Serializable]
		public
		struct
		Sound
		{
			[ReadOnly]
			public Sound_Type type;
			public AudioClip  audio_clip;
			public Pitch_Range pitch_range;
		}

		public List<SFX_Materials_Couple> sfx_materials_couples = new List<SFX_Materials_Couple>();
		//public AudioClip[]                sfx_clip_by_type      = new AudioClip[(int)Sound_Type._COUNT];
		public Sounds                     sfx_by_type           = new Sounds( (int)Sound_Type._COUNT );

		public SFX sfx_prefab;

		private Sound_Type[][] sound_types_triangular_matrix = new Sound_Type[(int)Sound_Material.Type._COUNT][];
		public Pool<SFX>       sound_sources;

		#if UNITY_EDITOR
		private void OnValidate()
		{
			for ( int i = 0; i != sfx_by_type.Length; ++i )
			{
				sfx_by_type.array[i].type = (Sound_Type)i;
			}
		}
		#endif

		private
		void
		Awake()
		{
			for ( int i = 0; i != sound_types_triangular_matrix.Length; ++i )
			{
				sound_types_triangular_matrix[i] = new Sound_Type[ i + 1 ];
				for ( int j = 0; j != sound_types_triangular_matrix[i].Length; ++j )
				{
					sound_types_triangular_matrix[i][j] = Sound_Type.UNDEFINED;
				}
			}
			for ( int i = 0; i != sfx_materials_couples.Count; ++i )
			{
				SFX_Materials_Couple mc = sfx_materials_couples[i];
				Sound_Type old_type = set_sound_type( mc );
				if ( old_type != Sound_Type.UNDEFINED )
				{
					Debug.LogWarning( "sound type of material couple " + mc.type_a + " - " + mc.type_b + " is doubled. Found type " + old_type + " new type " + mc.sound_type );
				}
			}

			sfx_prefab.manager = this;
			sound_sources = new Pool<SFX>();
			sound_sources.init( sfx_prefab );
		}

		public
		void
		play_hit_sound( Sound_Material.Type m0, Sound_Material.Type m1, Vector3 world_position )
		{
			Sound_Type sound_type = get_sound_type( m0, m1 );
			if ( sound_type == Sound_Type.UNDEFINED )
			{
				return;
			}
			play_sound( sound_type, world_position );
		}

		public
		void
		play_sound( Sound_Type sound_type, Vector3 world_position )
		{
			#if NO
			AudioSource.PlayClipAtPoint( clip( sound_type ), world_position );
			#else
			SFX sfx = sound_sources.take_away();
			sfx.transform.SetParent( null );
			sfx.transform.localPosition = world_position;
			sfx.play( clip( sound_type ), pitch( sound_type ) );
			sfx.gameObject.SetActive( true );
			#endif
		}

		public
		void
		play_sound( Sound_Type sound_type, Transform dynamic_position, Vector3 relative_local_position )
		{
			// TODO(theGiallo): finish to IMPLEMENT
			SFX sfx = sound_sources.take_away();
			sfx.transform.SetParent( dynamic_position, false );
			sfx.transform.localPosition = relative_local_position;
			sfx.play( clip( sound_type ), pitch( sound_type ) );
			sfx.gameObject.SetActive( true );
		}

		public
		AudioClip
		clip( Sound_Type sound_type )
		{
			AudioClip ret;
			Sound sound = sfx_by_type[(int)sound_type];
			ret = sound.audio_clip;
			return ret;
		}

		public
		float
		pitch( Sound_Type sound_type )
		{
			float ret;
			Sound sound = sfx_by_type[(int)sound_type];
			ret = UnityEngine.Random.Range( sound.pitch_range.min, sound.pitch_range.max );
			ret = ret <= 0 ? 1.0f : ret;
			return ret;
		}

		private
		Sound_Type
		get_sound_type( Sound_Material.Type m0, Sound_Material.Type m1 )
		{
			Sound_Type ret = Sound_Type.UNDEFINED;
			int i0 = (int)m0;
			int i1 = (int)m1;
			if ( i1 > i0 )
			{
				int tmp = i0;
				     i0 = i1;
				     i1 = tmp;
			}
			ret = sound_types_triangular_matrix[i0][i1];
			return ret;
		}
		private
		Sound_Type
		get_sound_type( Sound_Material.Type m0, Sound_Material.Type m1, out int idx0, out int idx1 )
		{
			Sound_Type ret = Sound_Type.UNDEFINED;
			int i0 = (int)m0;
			int i1 = (int)m1;
			if ( i1 > i0 )
			{
				int tmp = i0;
				     i0 = i1;
				     i1 = tmp;
			}
			ret = sound_types_triangular_matrix[i0][i1];
			idx0 = i0;
			idx1 = i1;

			return ret;
		}
		private
		Sound_Type
		set_sound_type( Sound_Material.Type m0, Sound_Material.Type m1, Sound_Type sound_type )
		{
			Sound_Type ret;
			int i0 = (int)m0;
			int i1 = (int)m1;
			if ( i1 > i0 )
			{
				int tmp = i0;
				     i0 = i1;
				     i1 = tmp;
			}
			ret = sound_types_triangular_matrix[i0][i1];
			sound_types_triangular_matrix[i0][i1] = sound_type;
			return ret;
		}
		private
		Sound_Type
		set_sound_type( SFX_Materials_Couple mc )
		{
			Sound_Type ret = set_sound_type( mc.type_a, mc.type_b, mc.sound_type );
			return ret;
		}
		public
		void
		activate_sfx( Gameplay_Event gameplay_event )
		{
			switch ( gameplay_event.type )
			{
				case Gameplay_Event.Type.WEAPON_HIT:
					play_hit_sound( Sound_Material.Type.LASER_BEAM,
					                gameplay_event.weapon_hit.hit_material_type,
					                gameplay_event.weapon_hit.position );
					break;
				case Gameplay_Event.Type.WEAPON_SHOT:
					play_sound( Sound_Type.LASER_WEAPON_SHOT, gameplay_event.weapon_shot.start_position );
					break;
			}
		}
	}
}
