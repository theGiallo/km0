﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	Mouse_Wrapper : MonoBehaviour
	{
		public static Mouse_Wrapper instance;
		public readonly Client.Message_Protocol.Message_ID message_ID = Client.Message_Protocol.Message_ID.MOUSE_DELTA;
		public bool send_always = false;

		public struct
		Mouse_Delta
		{
			public float dx, dy;

			public void
			retrieve_values()
			{
				dx = Input.GetAxisRaw( "Mouse X" );
				dy = Input.GetAxisRaw( "Mouse Y" );
			}

			public
			void
			clear()
			{
				dx = dy = 0;
			}
		}

		private LiteNetLib.Utils.NetDataWriter this_net_data_writer = new LiteNetLib.Utils.NetDataWriter();

		public
		void
		Awake()
		{
			instance = this;
		}

#if NO
		public
		void
		Start()
		{

		}

		void
		Update()
		{
		}
#endif

		public
		void
		gather( LiteNetLib.Utils.NetDataWriter net_data_writer )
		{
			this_net_data_writer.Reset();
			Mouse_Delta md = new Mouse_Delta();
			md.retrieve_values();

			if ( send_always || ( md.dx != 0 && md.dy != 0 ) )
			{
				this_net_data_writer.PutMouse_Delta( md );
			}

			if ( this_net_data_writer.Length > 0 )
			{
				net_data_writer.PutClientMessage_ID( message_ID );
				net_data_writer.Put( this_net_data_writer.Data, 0, this_net_data_writer.Length );
			}
		}
		public
		static
		Mouse_Delta
		decode( LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Mouse_Delta ret = net_data_reader.GetMouse_Delta();

			return ret;
		}
	}
	public static class
	Mouse_Delta_NetDataExtensions
	{
		public static void
		PutMouse_Delta( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                Mouse_Wrapper.Mouse_Delta mouse_delta )
		{
			net_data_writer.Put( mouse_delta.dx );
			net_data_writer.Put( mouse_delta.dy );
		}
		public static Mouse_Wrapper.Mouse_Delta
		GetMouse_Delta( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Mouse_Wrapper.Mouse_Delta ret = new Mouse_Wrapper.Mouse_Delta();
			ret.dx = net_data_reader.GetFloat();
			ret.dy = net_data_reader.GetFloat();
			return ret;
		}
	}
}