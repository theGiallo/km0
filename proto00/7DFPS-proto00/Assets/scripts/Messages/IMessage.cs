﻿using LiteNetLib.Utils;

namespace fps.Messages
{
	public interface IMessage
	{
		byte Code { get; }
		byte Subcode { get; }

		void Serialize(ref NetDataWriter writer);
		void Deserialize(NetDataReader reader);
	}
}
