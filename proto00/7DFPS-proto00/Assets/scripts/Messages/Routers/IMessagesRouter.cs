﻿using fps.Messages.Handlers.Loaders;
using LiteNetLib.Utils;

namespace fps.Messages.Routers
{
	public interface IMessagesRouter
	{
		IMessageHandlersLoader Loader { get; }

		void Route(NetDataReader reader);
	}
}
