﻿using System;

namespace fps.Messages.Routers.Exceptions
{
	public class HandlerNotFoundException : Exception
	{
		public HandlerNotFoundException(byte code, byte subcode) : base("Handler [" + code + ", " + subcode + "] not found.")
		{

		}
	}
}
