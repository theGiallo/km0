﻿using fps.Messages.Handlers;
using fps.Messages.Handlers.Loaders;
using fps.Messages.Routers.Exceptions;
using LiteNetLib.Utils;
using System.Collections.Generic;

namespace fps.Messages.Routers
{
	public class MessagesRouter : IMessagesRouter
	{
		public IMessageHandlersLoader Loader { get; private set; }

		private readonly Dictionary<byte, Dictionary<byte, IMessageHandler>> _handlers;

		public MessagesRouter(IMessageHandlersLoader loader)
		{
			Loader = loader;
			var handlers = Loader.Load();

			_handlers = new Dictionary<byte, Dictionary<byte, IMessageHandler>>();

			for (var i = 0; i < handlers.Count; i++)
			{
				var handler = handlers[i];
				var code = handler.Code;
				var subcode = handler.Subcode;

				if (_handlers.ContainsKey(code))
				{
					if(_handlers[code] == null)
						_handlers[code] = new Dictionary<byte, IMessageHandler>();
					_handlers[code].Add(subcode, handler);
				}
				else
				{
					_handlers.Add(code, new Dictionary<byte, IMessageHandler>() { {subcode, handler} });
				}
			}
		}

		public void Route(NetDataReader reader)
		{
			var code = reader.GetByte();
			var subcode = reader.GetByte();

			if (_handlers.ContainsKey(code))
			{
				var handlers = _handlers[code];
				if (handlers.ContainsKey(subcode))
				{
					var handler = handlers[subcode];
					handler.Handle(handler.Create(reader));
				}
			}

			throw new HandlerNotFoundException(code, subcode);
		}
	}
}
