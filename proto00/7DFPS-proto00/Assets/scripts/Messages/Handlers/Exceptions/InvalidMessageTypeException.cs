﻿using System;

namespace fps.Messages.Handlers.Exceptions
{
	public class InvalidMessageTypeException : Exception
	{
		public InvalidMessageTypeException(byte code, byte subcode, Type type) : base("IMessage [" + type + "] cannot be created in handler [" + code + ", " + subcode + "].")
		{

		}
	}
}
