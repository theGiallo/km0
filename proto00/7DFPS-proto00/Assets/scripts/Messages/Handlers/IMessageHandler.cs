﻿using LiteNetLib.Utils;

namespace fps.Messages.Handlers
{
	public interface IMessageHandler
	{
		byte Code { get; }
		byte Subcode { get; }

		IMessage Create(NetDataReader reader);
		void Handle(IMessage message);
	}
}
