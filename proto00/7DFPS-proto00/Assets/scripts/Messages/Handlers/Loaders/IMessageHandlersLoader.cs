﻿using System.Collections.Generic;

namespace fps.Messages.Handlers.Loaders
{
	public interface IMessageHandlersLoader
	{
		List<IMessageHandler> Load();
	}
}
