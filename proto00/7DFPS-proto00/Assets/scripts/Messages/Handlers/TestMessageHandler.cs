﻿namespace fps.Messages.Handlers
{
	public class TestMessageHandler : MessageHandler<SimpleMessage>
	{
		public override byte Code { get; } = 0;
		public override byte Subcode { get; } = 0;

		protected override void Handle(SimpleMessage message)
		{
			// Do stuff with that SimpleMessage
		}
	}
}
