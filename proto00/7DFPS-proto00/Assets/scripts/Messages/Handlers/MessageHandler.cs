﻿using LiteNetLib.Utils;
using System;

namespace fps.Messages.Handlers
{
	public abstract class MessageHandler<T> : IMessageHandler where T : IMessage
	{
		public abstract byte Code { get; }
		public abstract byte Subcode { get; }

		public IMessage Create(NetDataReader reader)
		{
			IMessage message = Activator.CreateInstance<T>();
			message.Deserialize(reader);
			return message;
		}

		public void Handle(IMessage message)
		{
			Handle((T)message);
		}

		protected abstract void Handle(T message);
	}
}
