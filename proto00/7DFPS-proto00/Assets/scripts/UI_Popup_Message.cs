﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace fps
{
	public class
	UI_Popup_Message : MonoBehaviour
	{
		public Button acknowledge_button;
		public Text   message_text;
		public Text   button_text;

		public UnityEvent on_acknowledge = new UnityEvent();

		private string _message;
		public string message
		{
			get { return _message; }
			set { message_text.text = _message = value; }
		}

		private string _button_string;
		public string default_button_string;
		public string button_string
		{
			get { return _button_string; }
			set { button_text.text = _button_string = value; }
		}

		private void Awake()
		{
			default_button_string = _button_string = button_text.text;
		}
		void
		Start()
		{
			Reset();
		}

		private
		void
		Reset()
		{
			button_string = default_button_string;
			on_acknowledge.RemoveAllListeners();
			acknowledge_button.onClick.RemoveAllListeners();
			acknowledge_button.onClick.AddListener( () => { on_acknowledge.Invoke(); gameObject.SetActive( false ); } );
		}

		public
		void
		deactivate()
		{
			gameObject.SetActive( false );
		}

		public
		void
		activate( UnityAction callback, string message, string button_string = "" )
		{
			Reset();
			this.message = message;
			if ( button_string != null )
			{
				this.button_string = button_string;
			}
			on_acknowledge.AddListener( callback );
			gameObject.SetActive( true );
		}

		//void
		//Update()
		//{
		//}
	}
}