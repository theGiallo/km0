﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent(typeof(SphereCollider))]
	public class
	DeactivateColliderOnSideCollision : MonoBehaviour
	{
		public float r_perc = 0.5f;

		private Dictionary<Collider,Collision> colliders_currently_colliding = new Dictionary<Collider, Collision>();

		SphereCollider sphere_collider;
		private void Awake()
		{
			sphere_collider = GetComponent<SphereCollider>();
		}
		private void OnCollisionEnter( Collision collision )
		{
			colliders_currently_colliding[collision.collider] = collision;
			check_collisions();
		}
		private void OnCollisionExit( Collision collision )
		{
			bool b_res = colliders_currently_colliding.Remove( collision.collider );
			Debug.Assert( b_res );
			check_collisions();
		}

		public
		void
		check_collisions()
		{
			Vector3 local_up = transform.localRotation * Vector3.up;
			Vector3 pos = transform.position;
			local_up.Normalize();
			float r = sphere_collider.radius;
			Vector3 normal = Vector3.zero;
			bool all_up = true;
			bool all_down = true;
			foreach ( Collision collision in colliders_currently_colliding.Values )
			{
				for ( int i = 0, count = collision.contacts.Length; i != count; ++i )
				{
					Vector3 n = collision.contacts[i].normal;
					Vector3 p = collision.contacts[i].point;
					float d = Vector3.Dot( local_up, p - pos );
					all_up   = all_up   && ( d >= - r_perc * r );
					all_down = all_down && ( d <= - r_perc * r );
				}
			}
			if ( !all_down )
			{
				gameObject.SetActive( false );
			} else
			{
				gameObject.SetActive( true );
			}
		}
	}
}