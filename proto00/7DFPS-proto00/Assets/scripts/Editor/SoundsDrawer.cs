﻿using UnityEditor;
using UnityEngine;

namespace fps
{
	[UnityEditor.CustomPropertyDrawer( typeof( Sounds ) )]
	public class SoundsDrawer : ArrayWrapperDrawer//<SFX_Manager.Sound>
	{
        #if NO
        public
		override
		float
		GetPropertyHeight( SerializedProperty property, GUIContent label )
		{
			return base.GetPropertyHeight( property, label );
		}

		public
		override
		void
		OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			base.OnGUI( position, property, label );
		}
        #endif
	}
}
