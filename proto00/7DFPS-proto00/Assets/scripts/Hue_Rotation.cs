﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hue_Rotation : MonoBehaviour
{
	public Color   color_base;
	public Color   color_current;
	public Graphic graphics;
	[Range(0.8f,120)]
	public float   period_secs = 5.0f;

	private float h,s,v;

	void
	Start()
	{
		Color.RGBToHSV( color_base, out h, out s, out v );
	}

	void
	Update()
	{
		graphics.color = color_current = Color.HSVToRGB( Mathf.Cos( Mathf.PI * 2.0f * Time.realtimeSinceStartup / period_secs ) * 0.5f + 0.5f, s, v );
	}

#if UNITY_EDITOR
	private void OnValidate()
	{
		Update();
	}
#endif
}