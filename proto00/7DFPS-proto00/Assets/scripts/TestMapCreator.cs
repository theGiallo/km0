﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	public class
	TestMapCreator : MonoBehaviour
	{
		public Prefab_Hash prefab_to_instantiate;
		public GameObject  instance_obj;

		public void
		spawn_map()
		{
			if ( ! Server.has_authority )
			{
				Debug.Log( "server has not authority" );
				return;
			}
			Prefabs_Manager.instance.instantiate_prefab( prefab_to_instantiate );
		}

		void
		Start()
		{
		}

		public void OnEnable()
		{
			if ( ! Server.has_authority )
			{
				return;
			}
			Debug.Log( "test map spawner OnEnable" );
			if ( instance_obj != null )
			{
				Destroy( instance_obj );
				instance_obj = null;
			}
			spawn_map();
		}

		void
		Update()
		{

		}
	}
}