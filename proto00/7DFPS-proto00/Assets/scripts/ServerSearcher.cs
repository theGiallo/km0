﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace fps
{
	public class
	ServerSearcher : MonoBehaviour, LiteNetLib.INetEventListener
	{
		public class On_New_Server_Discovered : UnityEvent<Advertised_Server_Data> { }

		public int                      port;
		[Range(1,128)]
		public int                      port_range_length;
		public On_New_Server_Discovered on_new_server_discovered = new On_New_Server_Discovered();

		public static readonly string ADV_HEADER_KEY = "FPS_ADV";

		LiteNetLib.NetManager net_manager;
		bool running = false;

		void INetEventListener.OnNetworkError( NetEndPoint endPoint, int socketErrorCode )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkLatencyUpdate( NetPeer peer, int latency )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkReceiveUnconnected( NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType )
		{
			IPEndPoint remote_ipendpoint = remoteEndPoint.EndPoint;
			//Debug.Log( "[server searcher] received from " + remote_ipendpoint + " ( " + remoteEndPoint + " ) " + messageType );
			switch ( messageType )
			{
				case UnconnectedMessageType.DiscoveryRequest:
				{
					string head_key = reader.GetString();
					if ( head_key != ADV_HEADER_KEY )
					{
						return;
					}
					Advertised_Server_Data asd = new Advertised_Server_Data();
					asd.name                    = reader.GetString();
					asd.port                    = reader.GetInt();
					asd.connected_players_count = reader.GetSByte();
					asd.adv_net_end_point           = remoteEndPoint;

					//Debug.Log( "port = " + asd.port + " adv port = " + remote_ipendpoint.Port );

					System.Net.IPEndPoint ipep = remoteEndPoint.EndPoint;
					ipep.Port = asd.port;
					asd.server_net_end_point = new LiteNetLib.NetEndPoint( ipep );
					on_new_server_discovered.Invoke( asd );
					break;
				}
				case UnconnectedMessageType.Default:
				{
					// TODO(theGiallo): here manage message
					break;
				}
				case UnconnectedMessageType.DiscoveryResponse:
					break;
			}
		}

		void INetEventListener.OnPeerConnected( NetPeer peer )
		{
			throw new System.NotImplementedException();
		}

		void INetEventListener.OnPeerDisconnected( NetPeer peer, DisconnectInfo disconnectInfo )
		{
			throw new System.NotImplementedException();
		}

		void
		Start()
		{
			net_manager = new NetManager( this, "" );
			net_manager.DiscoveryEnabled = true;
			net_manager.ReuseAddress     = true;
		}
		public
		void
		setup()
		{
			running = true;
			for ( int i = 0; i != port_range_length; ++i )
			{
				int p = port + i;
				bool b_res;
				b_res = net_manager.Start( p );
				if ( ! b_res )
				{
					Debug.LogError( "failed to start on port " + p );
				} else
				{
					break;
				}
			}
		}

		void
		Update()
		{
			if ( ! running )
			{
				return;
			}

			if ( net_manager.IsRunning )
			{
				net_manager.PollEvents();
			} else
			{
				setup();
			}
		}

		public
		void
		stop()
		{
			running = false;
			if ( net_manager != null )
			{
				net_manager.Stop();
			}
		}
		private void OnDestroy()
		{
			stop();
		}
	}

	public class
	Advertised_Server_Data
	{
		public string      name;
		public int         port;
		public NetEndPoint adv_net_end_point;
		public NetEndPoint server_net_end_point;
		public int         connected_players_count;
	}
}