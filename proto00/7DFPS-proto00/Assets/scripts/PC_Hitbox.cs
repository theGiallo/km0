﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fps
{
	[RequireComponent(typeof(Collider))]
	public class
	PC_Hitbox : MonoBehaviour
	{
		public Player_Character pc;
		public Hitbox_Part_ID   type;

		[Header("Auto Assigned")]
		public Collider own_collider;

		public enum
		Hitbox_Part_ID
		{
			HEAD,
			TORSO,
			ARM_1_L,
			ARM_1_R,
			ARM_2_L,
			ARM_2_R,
			LEG_1_L,
			LEG_1_R,
			LEG_2_L,
			LEG_2_R,
			//--
			_COUNT
		}
		private void Awake()
		{
			own_collider = GetComponent<Collider>();
		}
	}
}