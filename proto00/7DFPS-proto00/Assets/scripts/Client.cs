﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace fps
{
	public class
	Client : MonoBehaviour, LiteNetLib.INetEventListener
	{
		public static Client instance { get; private set; }

		public class On_Other_Client : UnityEvent<Other_Client> { }

		public string connect_key { get; private set; }
		public bool connected  { get { return _connected;  } }
		public bool connecting { get { return _connecting; } }

		public On_Other_Client on_other_client_connection      = new On_Other_Client();
		public On_Other_Client on_other_client_disconnection   = new On_Other_Client();
		public On_Other_Client on_other_client_name_change     = new On_Other_Client();
		public On_Other_Client on_other_client_latency_updated = new On_Other_Client();
		public UnityEvent      on_disconnected;
		public UnityEvent      on_latency_updated;
		public UnityEvent      on_start_map;
		public UnityEvent      on_stop_map;

		public Input_Wrapper   input_wrapper;
		public bool            in_game;
		public bool            first_person_focus;

		public int latency
		{
			get { return net_manager.GetFirstPeer().Ping; }
		}

		private LiteNetLib.NetManager net_manager;
		private bool got_error = false;
		//bool started = false;
		private LiteNetLib.Utils.NetDataWriter data_writer;
		private int port = 0;

		private NetEndPoint server_netendpoint;
		private NetPeer     server_peer;
		private bool        _connected  = false;
		private bool        _connecting = false;

		private string      nickname;

		public class
		Other_Client
		{
			public string      name;
			public NetEndPoint net_end_point;
			public ushort      ping;
		}
		Dictionary<NetEndPoint,Other_Client> other_clients = new Dictionary<NetEndPoint, Other_Client>();

		public class
		Message_Protocol
		{
			public enum
			Message_ID
			{
				CLIENT_RENAME,
				INPUT_ACTION_FRONT,
				MOUSE_DELTA,
				//---
				_COUNT,
			}

			NetDataWriter net_data_writer = new NetDataWriter();

			public
			NetDataWriter
			encode_client_rename( string new_name )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				w.PutClientMessage_ID( Message_ID.CLIENT_RENAME );

				w.Put( new_name );

				return w;
			}
			public static
			string
			decode_client_rename( NetDataReader r )
			{
				string ret = r.GetString();
				return ret;
			}

			public
			NetDataWriter
			encode_input_action_front( Input_Wrapper input_wrapper )
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				input_wrapper.gather( net_data_writer );

				return w;
			}
			public
			static
			List<Input_Wrapper.Action_With_Front>
			decode_input_action_front( NetDataReader r )
			{
				List<Input_Wrapper.Action_With_Front> ret = Input_Wrapper.decode( r );
				return ret;
			}

			public
			NetDataWriter
			encode_mouse_delta()
			{
				NetDataWriter w = net_data_writer;
				w.Reset();

				Mouse_Wrapper.instance.gather( w );

				return w;
			}
			public
			static
			Mouse_Wrapper.Mouse_Delta
			decode_mouse_delta( NetDataReader r)
			{
				Mouse_Wrapper.Mouse_Delta ret = Mouse_Wrapper.decode( r );
				return ret;
			}
		}
		Message_Protocol mp = new Message_Protocol();

		void INetEventListener.OnNetworkError( NetEndPoint endPoint, int socketErrorCode )
		{
			Debug.Log( "client void INetEventListener.OnNetworkError( NetEndPoint endPoint = " + endPoint + ", int socketErrorCode = " + socketErrorCode + " )" );
			_connecting = false;
			_connected  = false;
			//throw new System.NotImplementedException();
		}

		void INetEventListener.OnNetworkLatencyUpdate( NetPeer peer, int latency )
		{
			on_latency_updated.Invoke();
		}

		void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )
		{
			//Debug.Log( "void INetEventListener.OnNetworkReceive( NetPeer peer, NetDataReader reader )" );

			if ( server_peer != peer )
			{
				Debug.LogError( "client received from a peer that is not the server" );
				return;
			}

			Server.Message_Protocol.Message_ID m_id = reader.GetServerMessage_ID();
			switch ( m_id )
			{
				case Server.Message_Protocol.Message_ID.CLIENT_CONNECTED:
				{
					Other_Client other_client = Server.Message_Protocol.decode_client_connected( reader );
					other_clients.Add( other_client.net_end_point, other_client );
					on_other_client_connection.Invoke( other_client );
					break;
				}
				case Server.Message_Protocol.Message_ID.CLIENT_DISCONNECTED:
				{
					NetEndPoint other_client_net_end_point = Server.Message_Protocol.decode_client_disconnected( reader );
					Other_Client other_client;
					if ( other_clients.TryGetValue( other_client_net_end_point, out other_client ) )
					{
						on_other_client_disconnection.Invoke( other_client );
						other_clients.Remove( other_client_net_end_point );
					} else
					{
						Debug.LogError( "client got an other client disconnection event about an unknown client" );
					}
					break;
				}
				case Server.Message_Protocol.Message_ID.CLIENT_RENAME:
				{
					NetEndPoint other_client_net_end_point;
					string      new_name;
					Server.Message_Protocol.decode_client_rename( reader, out other_client_net_end_point, out new_name );
					Other_Client other_client;
					if ( other_clients.TryGetValue( other_client_net_end_point, out other_client ) )
					{
						other_client.name = new_name;
						on_other_client_name_change.Invoke( other_client );
					}
					break;
				}
				case Server.Message_Protocol.Message_ID.CLIENT_LATENCY_UPDATE:
				{
					NetEndPoint other_client_net_end_point;
					ushort      new_latency;
					Server.Message_Protocol.decode_client_latency_update( reader, out other_client_net_end_point, out new_latency );
					Other_Client other_client;
					if ( other_clients.TryGetValue( other_client_net_end_point, out other_client ) )
					{
						other_client.ping = new_latency;
						on_other_client_latency_updated.Invoke( other_client );
					}
					break;
				}
				case Server.Message_Protocol.Message_ID.START_MAP:
				{
					on_start_map.Invoke();
					in_game = true;
					first_person_focus = true;
					break;
				}
				case Server.Message_Protocol.Message_ID.STOP_MAP:
				{
					on_stop_map.Invoke();
					in_game = false;
					first_person_focus = false;
					break;
				}
				case Server.Message_Protocol.Message_ID.PREFAB_SPAWN:
				{
					Prefab_Hash ph = Server.Message_Protocol.decode_prefab_spawn( reader );
					GameObject  go = Prefabs_Manager.instance.instantiate_prefab( ph, reader );
					break;
				}
				case Server.Message_Protocol.Message_ID.SYNC_WORLD:
				{
					Server.Message_Protocol.decode_world_sync( reader );
					break;
				}
				case Server.Message_Protocol.Message_ID.ASSIGN_PLAYER_CHARACTER:
				{
					UUID uuid = Server.Message_Protocol.decode_assign_player_character( reader );
					GameObject go;
					bool b_res = UUID_Assigner.try_get_go( uuid, out go );
					if ( !b_res )
					{
						Debug.LogError( "[client] got uuid for player character, but couldn't find it ( uuid = " + uuid + " )" );
						break;
					}
					Player_Character pc = go.GetComponent<Player_Character>();
					pc.set_current();
					break;
				}
				case Server.Message_Protocol.Message_ID.GAMEPLAY_EVENTS:
					Server.Message_Protocol.decode_gameplay_events( reader );
					break;
				default:
					Debug.LogError( "received a message of unknown id " + m_id + " = " + (ushort)m_id );
					break;
			}
		}

		void INetEventListener.OnNetworkReceiveUnconnected( NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType )
		{
			IPEndPoint remote_ipendpoint = remoteEndPoint.EndPoint;
			Debug.Log( "[client] received from " + remote_ipendpoint + " ( " + remoteEndPoint + " ) " + messageType + " " + reader.GetString() );
		}

		void INetEventListener.OnPeerConnected( NetPeer peer )
		{
			Debug.Log( "void INetEventListener.OnPeerConnected( NetPeer peer = " + peer + " )" );
			_connected = true;
			//throw new System.NotImplementedException();
		}

		void INetEventListener.OnPeerDisconnected( NetPeer peer, DisconnectInfo disconnectInfo )
		{
			Debug.Log( "void INetEventListener.OnPeerDisconnected( NetPeer peer = " + peer.EndPoint.ToString() + ", DisconnectInfo disconnectInfo = " + disconnectInfo.Reason + " " + disconnectInfo.SocketErrorCode + " )" );
			_connected  = false;
			_connecting = false;
			on_disconnected.Invoke();
			//throw new System.NotImplementedException();
		}

		public
		static
		void
		setup_controls( Input_Wrapper input_wrapper )
		{
			input_wrapper.add_action( Input_Wrapper.Action.MOVE_FORWARD  );
			input_wrapper.add_action( Input_Wrapper.Action.MOVE_BACKWORD );
			input_wrapper.add_action( Input_Wrapper.Action.STRAFE_LEFT   );
			input_wrapper.add_action( Input_Wrapper.Action.STRAFE_RIGHT  );
			input_wrapper.add_action( Input_Wrapper.Action.JUMP          );
			input_wrapper.add_action( Input_Wrapper.Action.SPRINT        );
			input_wrapper.add_action( Input_Wrapper.Action.USE_ARM_LEFT  );
			input_wrapper.add_action( Input_Wrapper.Action.USE_ARM_RIGHT );

			Input_Wrapper.Input_Source input_source_forward       = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_backward      = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_strafe_left   = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_strafe_right  = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_jump          = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_sprint        = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_use_arm_left  = new Input_Wrapper.Input_Source();
			Input_Wrapper.Input_Source input_source_use_arm_right = new Input_Wrapper.Input_Source();

			input_source_forward      .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_backward     .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_strafe_left  .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_strafe_right .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_jump         .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_sprint       .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_use_arm_left .type = Input_Wrapper.Input_Source.Type.BUTTON;
			input_source_use_arm_right.type = Input_Wrapper.Input_Source.Type.BUTTON;

			KeyCode LMB = KeyCode.Mouse0;
			KeyCode RMB = KeyCode.Mouse1;
			input_source_forward      .key_code = KeyCode.W;
			input_source_backward     .key_code = KeyCode.S;
			input_source_strafe_left  .key_code = KeyCode.A;
			input_source_strafe_right .key_code = KeyCode.D;
			input_source_jump         .key_code = KeyCode.Space;
			input_source_sprint       .key_code = KeyCode.LeftShift;
			input_source_use_arm_left .key_code = LMB;
			input_source_use_arm_right.key_code = RMB;


			Input_Wrapper.Input_Source old_input_source;
			Input_Wrapper.Action old_action;
			input_wrapper.remap_action( Input_Wrapper.Action.MOVE_FORWARD,  input_source_forward,       out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.MOVE_BACKWORD, input_source_backward,      out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.STRAFE_LEFT,   input_source_strafe_left,   out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.STRAFE_RIGHT,  input_source_strafe_right,  out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.JUMP,          input_source_jump,          out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.SPRINT,        input_source_sprint,        out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.USE_ARM_LEFT,  input_source_use_arm_left,  out old_input_source, out old_action );
			input_wrapper.remap_action( Input_Wrapper.Action.USE_ARM_RIGHT, input_source_use_arm_right, out old_input_source, out old_action );
		}

		private
		void
		Awake()
		{
			instance = this;
		}

		void Start()
		{
			data_writer = new NetDataWriter();
			setup_controls( input_wrapper );
			//create();
		}

		void Update()
		{
			if ( got_error )
			{
				setup();
				if ( got_error )
				{
					return;
				}
			}

			net_manager.PollEvents();

			if ( in_game )
			{
				if ( Input.GetKeyDown( KeyCode.Escape ) )
				{
					first_person_focus = ! first_person_focus;
				}
			}
			Cursor.lockState = first_person_focus ? CursorLockMode.Locked : CursorLockMode.None;
			if ( in_game && first_person_focus )
			{
				NetDataWriter net_data_writer = mp.encode_input_action_front( input_wrapper );
				if ( net_data_writer.Length > 0 )
				{
					server_peer.Send( net_data_writer, SendOptions.ReliableOrdered );
				}
				net_data_writer = mp.encode_mouse_delta();
				if ( net_data_writer.Length > 0 )
				{
					server_peer.Send( net_data_writer, SendOptions.ReliableOrdered );
				}
			}
		}

		private void OnDestroy()
		{
			disconnect();
			if ( net_manager != null )
			{
				net_manager.Stop();
			}
		}

		public
		void
		set_key( string connect_key )
		{
			if ( this.connect_key.Equals( connect_key ) )
			{
				return;
			}
			this.connect_key = connect_key;
			disconnect();
			net_manager.Stop();
			create();
		}

		public
		void
		create()
		{
			_connected   = false;
			if ( connect_key == null )
			{
				connect_key = "";
			}
			net_manager = new LiteNetLib.NetManager( this, connect_key );
			//net_manager.DiscoveryEnabled = true;

			setup();
		}
		public
		void
		connect( NetEndPoint netendpoint )
		{
			if ( net_manager == null )
			{
				create();
				if ( got_error )
				{
					return;
				}
			} else
			{
				disconnect();
			}
			server_netendpoint = netendpoint;
			server_peer = net_manager.Connect( server_netendpoint );
			_connecting = true;
			_connected  = false;
		}

		void setup()
		{
			bool b_res;
			b_res = net_manager.Start( port = 0 );
			if ( ! b_res )
			{
				Debug.LogError( "failed to start" );
				got_error = true;
				return;
			} else
			{
				got_error = false;
				port = net_manager.LocalPort;
			}
		}
		public
		void
		disconnect()
		{
			if ( net_manager != null )
			{
				if ( _connected )
				{
					net_manager.DisconnectPeer( server_peer );
				}
			}
			other_clients.Clear();
			_connected = false;
			_connecting = false;
		}

		public
		void
		on_rename( string new_name )
		{
			nickname = new_name;
			NetDataWriter w = mp.encode_client_rename( new_name );
			server_peer.Send( w, SendOptions.ReliableOrdered );
		}
	}
	public static class Cilent_Message_Protocol_NetDataExtensions
	{
		public
		static
		Client.Message_Protocol.Message_ID
		GetClientMessage_ID( this LiteNetLib.Utils.NetDataReader net_data_reader )
		{
			Client.Message_Protocol.Message_ID ret = (Client.Message_Protocol.Message_ID)net_data_reader.GetByte();
			return ret;
		}
		public
		static
		void
		PutClientMessage_ID( this LiteNetLib.Utils.NetDataWriter net_data_writer,
		                     Client.Message_Protocol.Message_ID message_id )
		{
			net_data_writer.Put( (byte)message_id );
		}
	}
}